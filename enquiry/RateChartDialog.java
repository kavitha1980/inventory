package enquiry;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.awt.image.*;

import guiutil.*;
import util.*;
import jdbc.*;
import java.net.*;

public class RateChartDialog extends JInternalFrame
{

     JPanel  TopPanel,MiddlePanel,BottomPanel;


     JTable  theTable;

     public RateChartModel theModel;
     public JButton        BSave,BExit;

     int              iAppr,ItemCode,ItemName,SupName;
     JLayeredPane     theLayer;
     Common           common  = new Common();
     TabReport      tabreport;
     public RateChartDialog(JLayeredPane theLayer,int iAppr,TabReport tabreport)
     {
          this.theLayer    = theLayer;
          this.tabreport   = tabreport;
          this.iAppr       = iAppr;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setRows();
     }

     private void createComponents()
     {

          TopPanel       =    new JPanel();
          MiddlePanel    =    new JPanel();
          BottomPanel    =    new JPanel();


          theModel       = new RateChartModel();
          theTable       = new JTable(theModel);
     
          BSave          = new JButton(" Save ");
          BExit          = new JButton(" Exit ");

          theModel.fireTableDataChanged();
          theTable.setShowGrid(true);
          theTable.setFillsViewportHeight(true);
     }
     private void setLayouts()
     {

          setClosable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,950,500);
          setVisible(true);
          setTitle(" Rate Chart ");

          MiddlePanel.setLayout(new BorderLayout());

     }
     private void addComponents()
     {

          MiddlePanel.add(new JScrollPane(theTable));
          BottomPanel.add(BSave);
          BottomPanel.add(BExit);

          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BSave.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
          theTable.addKeyListener(new KeyList());

     }

     private void setRows()
     {
          String STempItemCode="";


          for(int i=0;i<tabreport.RowData.length;i++)
          {
               Boolean Bselected = (Boolean)tabreport.RowData[i][9];   
               if(Bselected.booleanValue())
               {
                    String SItemCode    = (String)tabreport.RowData[i][2];
                    String SItemName    = (String)tabreport.RowData[i][3];

                    if(SItemCode.equals(STempItemCode))
                    {
                         STempItemCode = SItemCode;
                         continue;
                    }
                    HashMap theMap = new HashMap();
     
     
                    theMap.put("ItemCode",SItemCode);
                    theMap.put("ItemName",SItemName);

                    theModel.appendRow(theMap);
               }
          }
     }
     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int iRow = theTable.getSelectedRow();
                    try
                    {

                         
                         SupplierSearchList suplist = new SupplierSearchList(theLayer,theModel,iRow);
                         theLayer.add(suplist);
                         theLayer.repaint();
                         suplist.setSelected(true);
                         theLayer.updateUI();
                         suplist.show();
                    }catch(Exception ex){}
               }
          }
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExit)
               {
                    removeFrame();
               }
          }

     }
     public void removeFrame()
     {
          try
          {
               theLayer.remove(this);
               theLayer.updateUI();
          }
          catch(Exception ex)
          {

          }

     }

}
