package enquiry;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class SuppliersSearch implements ActionListener
{
     JLayeredPane   Layer;
     Vector         VSelectedSupCode,VSupName,VSupCode;
     JTextArea      ASuppliers;
     String         SSupTable;
     Vector         VPrevSupCode;

     JTextField     TIndicator;
     JButton        BOk;
     JList          BrowList,SelectedList;
     JScrollPane    BrowScroll,SelectedScroll;
     JPanel         LeftPanel,RightPanel;
     JInternalFrame SupplierFrame;
     JPanel         SFMPanel,SFBPanel;

     Vector VSelectedName,VSelectedCode;
     String str="";
     int iSFSig=0;
     ActionEvent ae;
     Common common = new Common();

     SuppliersSearch(JLayeredPane Layer,Vector VSelectedSupCode,JTextArea ASuppliers,Vector VSupCode,Vector VSupName,String SSupTable)
     {
          this.Layer            = Layer;
          this.VSelectedSupCode = VSelectedSupCode;
          this.ASuppliers       = ASuppliers;
          this.VSupCode         = VSupCode;
          this.VSupName         = VSupName;
          this.SSupTable        = SSupTable;

          VPrevSupCode = new Vector();
          createComponents();
     }
     SuppliersSearch(JLayeredPane Layer,Vector VSelectedSupCode,JTextArea ASuppliers,Vector VSupCode,Vector VSupName,String SSupTable,Vector VPrevSupCode)
     {
          this.Layer            = Layer;
          this.VSelectedSupCode = VSelectedSupCode;
          this.ASuppliers       = ASuppliers;
          this.VSupCode         = VSupCode;
          this.VSupName         = VSupName;
          this.SSupTable        = SSupTable;
          this.VPrevSupCode     = VPrevSupCode;

          createComponents();
     }

     public void createComponents()
     {
          VSelectedName = new Vector();
          VSelectedCode = new Vector();
          BrowList      = new JList(VSupName);
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          SFMPanel      = new JPanel(true);
          SFBPanel      = new JPanel(true);
          SupplierFrame = new JInternalFrame("List of Suppliers");
          SupplierFrame.show();
          SupplierFrame.setBounds(80,100,550,350);
          SupplierFrame.setClosable(true);
          SupplierFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedCode.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Supplier is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setSuppliers();
               removeHelpFrame();
               str="";
               ((JButton)ae.getSource()).setEnabled(false);
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          this.ae = ae;
          VSelectedName = new Vector();
          VSelectedCode = new Vector();
          TIndicator.setText(str);
          BOk.setEnabled(true);
          if(iSFSig==0)
          {
               SFMPanel.setLayout(new GridLayout(1,2));
               SFBPanel.setLayout(new GridLayout(1,2));
               SFMPanel.add(BrowScroll);
               SFMPanel.add(SelectedScroll);
               SFBPanel.add(TIndicator);
               SFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               SupplierFrame.getContentPane().add("Center",SFMPanel);
               SupplierFrame.getContentPane().add("South",SFBPanel);
               iSFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(SupplierFrame);
               SupplierFrame.moveToFront();
               SupplierFrame.setSelected(true);
               SupplierFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }  

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               lastchar=Character.toUpperCase(lastchar);
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==116)    // F5 is pressed
               {
                    setDataIntoVector();
                    BrowList.setListData(VSupName);
               }

               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    int index = BrowList.getSelectedIndex();
                    String SSupName = (String)VSupName.elementAt(index);
                    String SSupCode = (String)VSupCode.elementAt(index);
                    addSupDet(SSupName,SSupCode);
                    str="";
                    TIndicator.setText(str);
               }

               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    if(VSelectedCode.size()==0)
                    {
                         JOptionPane.showMessageDialog(null,"No Supplier is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                         BrowList.requestFocus();
                         return;
                    }

                    BOk.setEnabled(false);
                    setSuppliers();
                    removeHelpFrame();
                    str="";
                    ((JButton)ae.getSource()).setEnabled(false);
               }
          }
     }

     public void setCursor()
     {
          int index=0;
          TIndicator.setText(str);
          for(index=0;index<VSupName.size();index++)
          {
               String str1 = ((String)VSupName.elementAt(index)).toUpperCase();
               if(str1.startsWith(str))
               {
                    BrowList.setSelectedValue(str1,true);
                    BrowList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(SupplierFrame);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }

     public boolean addSupDet(String SSupName,String SSupCode)
     {
          if(VPrevSupCode.size()>0)
          {
               int iPIndex=VPrevSupCode.indexOf(SSupCode);
               if (iPIndex>=0)
               {
                    JOptionPane.showMessageDialog(null,"Enquiry Already Made for this Supplier","Information",JOptionPane.INFORMATION_MESSAGE);
                    return true;
               }
          }

          int iIndex=VSelectedCode.indexOf(SSupCode);
          if (iIndex==-1)
          {
               VSelectedName.addElement(SSupName);
               VSelectedCode.addElement(SSupCode);
          }
          else
          {
               VSelectedName.removeElementAt(iIndex);
               VSelectedCode.removeElementAt(iIndex);
          }
          SelectedList.setListData(VSelectedName);
          return true;
     }

     public void setSuppliers()
     {
          VSelectedSupCode.removeAllElements();
          ASuppliers.setText("");
          for(int i=0;i<VSelectedCode.size();i++)
          {
               VSelectedSupCode.addElement((String)VSelectedCode.elementAt(i));
               ASuppliers.append((String)VSelectedName.elementAt(i)+"\n");
          }
     }

     public void setDataIntoVector()
     {
          VSupName.removeAllElements();
          VSupCode.removeAllElements();
          String QString = "Select Name,Ac_Code From "+SSupTable+" Order By Name";
          try
          {
               ORAConnection  oraConnection  =  ORAConnection.getORAConnection();
               Connection     theConnection  =  oraConnection.getConnection();               
               Statement      stat           =  theConnection.createStatement();
          
               ResultSet res = stat.executeQuery(QString);
               while(res.next())
               {
                    VSupName.addElement(res.getString(1));
                    VSupCode.addElement(res.getString(2));
               }
               res  . close();
               stat . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
}
