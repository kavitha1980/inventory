package guiutil;

public class MyArrayList<E> extends java.util.ArrayList<E>{
	
	@Override
	public void add(int index, E element){
	    if(index >= 0 && index <= size()){
	        super.add(index, element);
	        return;
	    }
	    int insertNulls = index - size();
	    for(int i = 0; i < insertNulls; i++){
	        super.add(null);
	    }
	    super.add(element);
	}
	
}     

