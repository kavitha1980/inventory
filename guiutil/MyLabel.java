package guiutil;

import javax.swing.*;
import java.awt.*;

import util.*;
import jdbc.*;

public class MyLabel extends JLabel 
{
     public MyLabel()
     {
          super();
          setFont(new Font("WST_Span",Font.BOLD,11));
//          setForeground(FOREGROUND);
     }

     public MyLabel(String SName)
     {
          setText(SName);
          setFont(new Font("WST_Span",Font.BOLD,11));
//          setForeground(FOREGROUND);
     }

     public MyLabel(String SName,int iAlignment)
     {
          super(SName,iAlignment);
          setFont(new Font("WST_Span",Font.BOLD,11));
//          setForeground(FOREGROUND);
     }

}
