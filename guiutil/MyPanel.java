package guiutil;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

public class MyPanel extends JPanel 
{
     public MyPanel()
     {
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }
     public MyPanel(boolean bFlag)
     {
          super(bFlag);
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }

     private class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               raise();
          }
          public void mouseExited(MouseEvent me)
          {
               down();
          }
     }
     private class FocusList extends FocusAdapter
     {
          public void focusGained(FocusEvent fe)
          {
               raise();
          }
          public void focusLost(FocusEvent fe)
          {
               down();
          }
     }

     private void raise()
     {
//          setBackground(PANEL_ENHANCED_BACKGROUND);
     }
     private void down()
     {
//          setBackground(PANEL_NORMAL_BACKGROUND);
     }

}
