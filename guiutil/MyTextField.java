package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

/**
* Same as a JTextField except we can conrol
* the no of characters displayed
**/
public class MyTextField extends JTextField {

	int iColumns = 30;

     boolean bEnableHighLight;

	public MyTextField()
	{
		super();
	}

    public MyTextField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
    }


    public MyTextField(int columns) {
        super(columns);
		iColumns = columns;
    }

    public MyTextField(boolean bEnableHighLight) {
          this.bEnableHighLight = bEnableHighLight;
          if(bEnableHighLight)
               addFocusListener(new FocusList());

    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}
            super.insertString(offs, str, a);
        }
    }

     public class FocusList implements FocusListener
     {
          public void focusGained(FocusEvent fe)
          {
               setCaretPosition(0);
               moveCaretPosition((getText()).length());
          }

          public void focusLost(FocusEvent fe)
          {

          }

     }

}
