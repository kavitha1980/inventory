package jdbc;

import java.sql.*;

public class CMORAConnection
{
     static CMORAConnection connect = null;
     Connection theConnection     = null;

     public  CMORAConnection()
	{
		try
		{
               Class.forName("oracle.jdbc.OracleDriver");
			   // theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","rawcm","rawcm");
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","rawcm","rawcm");
		}
		catch(Exception e)
		{
               System.out.println("CMORAConnection : "+e);
               e.printStackTrace();
		}
	}

     public static CMORAConnection getORAConnection()
	{
		if (connect == null)
		{
               connect = new CMORAConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

