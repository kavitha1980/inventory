package jdbc;

import java.sql.*;

public class DORAConnection
{                             
     static    DORAConnection connect        = null;
               Connection     theConnection  = null;
     
     public  DORAConnection()
     {
          try
          {
               Class          . forName("oracle.jdbc.OracleDriver");
              // theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","dyeraw","dyeraw");
			   //kavitha
			    theConnection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.252.39:1521:amardye2","dyeraw","dyeraw");
			   
			   
               //theConnection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.0.66:1521:amardye2","dyeraw","dyeraw");
               //theConnection  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","dye1","dye1");
          }
          catch(Exception e)
          {
               System.out.println("DORAConnection : "+e);
               e.printStackTrace();
          }
     }
     
     public static DORAConnection getORAConnection()
     {
          if (connect == null)
          {
               connect = new DORAConnection();
          }
          return connect;
     }
     
     public Connection getConnection()
     {
          return theConnection;
     }
     
     public void finalize()
     {
          if (theConnection != null)
          {
               try
               {
                    theConnection.close();
               }
               catch (Exception e)
               {
                    e.printStackTrace();
               }
          }
     }
}
