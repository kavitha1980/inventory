package jdbc;

import java.sql.*;

public class KORAConnection
{
     static KORAConnection connect = null;
     Connection theConnection     = null;

     public  KORAConnection()
	{
		try
		{
               Class.forName("oracle.jdbc.OracleDriver");
			   //  theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","rawkam","rawkam");
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","rawkam","rawkam");
		}
		catch(Exception e)
		{
               System.out.println("KORAConnection : "+e);
               e.printStackTrace();
		}
	}

     public static KORAConnection getORAConnection()
	{
		if (connect == null)
		{
               connect = new KORAConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

