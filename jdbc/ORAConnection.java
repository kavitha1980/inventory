package jdbc;

import java.sql.*;

public class ORAConnection
{
	static ORAConnection connect = null;
     Connection theConnection     = null;

     public  ORAConnection()
	{
		try
		{
               Class.forName("oracle.jdbc.OracleDriver");
			  // theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","inventory","stores");
			   
               theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory","stores");
		}
		catch(Exception e)
		{
			System.out.println("ORAConnection : "+e);
               e.printStackTrace();
		}
	}

	public static ORAConnection getORAConnection()
	{
		if (connect == null)
		{
			connect = new ORAConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

