package jdbc;

import java.sql.*;

public class ORAConnection2
{
     static ORAConnection2 connect = null;
     Connection theConnection      = null;

     public ORAConnection2()
	{
		try
		{

            Class.forName("oracle.jdbc.OracleDriver");
			//theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","insur","insur");
			
            theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","insur","insur");
		}
		catch(Exception e)
		{
               System.out.println("ORAConnection2 : "+e);
		}
	}

     public static ORAConnection2 getORAConnection()
	{
		if (connect == null)
		{
               connect = new ORAConnection2();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

