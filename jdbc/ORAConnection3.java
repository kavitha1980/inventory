package jdbc;

import java.sql.*;

public class ORAConnection3
{
     static ORAConnection3 connect = null;
     Connection theConnection      = null;

     public ORAConnection3()
	{
		try
		{

            Class.forName("oracle.jdbc.OracleDriver");
			//theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","scm","rawmat");
            theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");
		}
		catch(Exception e)
		{
               System.out.println("ORAConnection3 : "+e);
		}
	}

     public static ORAConnection3 getORAConnection()
	{
		if (connect == null)
		{
               connect = new ORAConnection3();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

