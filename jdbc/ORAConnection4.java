package jdbc;

import java.sql.*;

public class ORAConnection4
{
     static ORAConnection4 connect = null;
     Connection theConnection      = null;

     public ORAConnection4()
	{
		try
		{

            Class.forName("oracle.jdbc.OracleDriver");
			
			//theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","hrdnew","hrdnew");
            theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
		}
		catch(Exception e)
		{
               System.out.println("ORAConnection4 : "+e);
		}
	}

     public static ORAConnection4 getORAConnection()
	{
		if (connect == null)
		{
               connect = new ORAConnection4();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

