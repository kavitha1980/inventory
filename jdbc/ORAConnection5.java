package jdbc;

import java.sql.*;

public class ORAConnection5
{
     static ORAConnection5 connect = null;
     Connection theConnection      = null;

     public ORAConnection5()
	{
		try
		{

            Class.forName("oracle.jdbc.OracleDriver");
			//theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","process","process");
            theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","process","process");
		}
		catch(Exception e)
		{
               System.out.println("ORAConnection5 : "+e);
		}
	}

     public static ORAConnection5 getORAConnection()
	{
		if (connect == null)
		{
               connect = new ORAConnection5();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

