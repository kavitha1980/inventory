/*
Spindle Transferring Utility            
*/
package spdlposting;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class SpdlPostFrame extends JInternalFrame
{
     JLayeredPane   Layer;
     Vector         VUnitCode,VDate,VSpdlWkd,VNoSpdl;
     JPanel         thePanel,BottomPanel;
     JTextArea      TA;
     JButton        BOkay,BCancel;
     JProgressBar   progress;
     Common         common = new Common();
     JLayeredPane   DeskTop;
     String         SFinYear  = "",SStDate   = "",SEnDate   = "";

     public SpdlPostFrame(JLayeredPane DeskTop,String SFinYear,String SStDate,String SEnDate)
     {
          super("Spindle Posting");

          this.DeskTop             = DeskTop;
          this.SFinYear            = SFinYear;
          this.SStDate             = SStDate;
          this.SEnDate             = SEnDate;

          VUnitCode                = new Vector();
          VDate                    = new Vector();
          VSpdlWkd                 = new Vector();
          VNoSpdl                  = new Vector();
          
          thePanel                 = new JPanel();
          thePanel                 . setOpaque(false);
          BottomPanel              = new JPanel();
          
          BOkay                    = new JButton("Start");
          BCancel                  = new JButton("Cancel");
          progress                 = new JProgressBar(JProgressBar.VERTICAL);
          progress                 . setMinimum(0);

          BottomPanel              .add(BOkay);
          BottomPanel              .add(BCancel);

          thePanel                 . setLayout(new BorderLayout());
          thePanel                 . add("North",new JLabel("Spindle Posting Utility"));
          thePanel                 . add("Center",new JScrollPane(TA=new JTextArea(5,50)));
          thePanel                 . add("East",progress);
          thePanel                 . add("South",BottomPanel);
          
          getContentPane()         . setLayout(new BorderLayout());
          getContentPane()         . add("Center",thePanel);
          
          TA                       .setFont(new Font("monospaced",Font.PLAIN,14));
          TA                       .setBackground(new Color(230,250,190));
          TA                       . setForeground(new Color(130,30,60));
          
          Dimension screenSize     = Toolkit.getDefaultToolkit().getScreenSize();
          double    x              = screenSize.getWidth();
          double    y              = screenSize.getHeight();
                    x              = x/2;
                    y              = y/2;
          
          setSize(600,350);
          setLocation(new Double(x-300).intValue(),new Double(y-175).intValue());
          BOkay.addActionListener(new ActList());
          BCancel.addActionListener(new CancelList());
          setVisible(true);
     }

     private void startPosting()
     {
          BOkay          . setEnabled(false);
          (new Posting()). start();
     }

     private class Posting extends Thread
     {
          Posting()
          {
               setPriority(Thread.MAX_PRIORITY);
          }
          public void run()
          {
               setProc("Spindle Posting Utility");
               getStartDate();
               getData("jdbc:odbc:AProd");
               getOracleData();
               progress.setStringPainted(true);
               progress.setMaximum(VUnitCode.size());
               setData();
               setProc("Posting is Over...\n");
               setVisible(false);
          }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOkay     . setEnabled(false);
               BCancel   . setEnabled(false);

               startPosting();
          }
     }

     private class CancelList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOkay     . setEnabled(false);
               BCancel   . setEnabled(false);

               setVisible(false);
          }
     }

     private void getStartDate()
     {
          try
          {
               String QS = "Select max(Sp_Date) From Spin where Sp_date <='"+SEnDate+"'";

               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnect     = oraConnection.getConnection();
               Statement      theStatement   = theConnect.createStatement();
               ResultSet      result         = theStatement.executeQuery(QS);

               while(result.next())
               {
                    SStDate = common.pureDate(common.getDate(common.parseDate(result.getString(1)),30,-1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }


     private void setData()
     {
          setProc("Posting is On...\n");
          try
          {
               String SDelQString = "Delete From Spin where Sp_date >= '"+SStDate+"' and Sp_date <='"+SEnDate+"'";

               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnect     = oraConnection.getConnection();
               Statement      theStatement   = theConnect.createStatement();

               theStatement   . execute(SDelQString);

               for(int i=0;i<VUnitCode.size();i++)
               {
                    String    QS = "Insert Into Spin(Unit_Code,Sp_Date,SpdlWkd,NoOfSpdl) Values(";
                              QS = QS+"0"+(String)VUnitCode.elementAt(i)+",";
                              QS = QS+"'"+(String)VDate.elementAt(i)+"',";
                              QS = QS+"0"+common.parseNull((String)VSpdlWkd.elementAt(i))+",";
                              QS = QS+"0"+common.parseNull((String)VNoSpdl.elementAt(i))+")";

                    theStatement   .    execute(QS);
                    progress       .    setValue(i);
                    progress       .    setString(String.valueOf(i));
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Exception got is Set"+ex);
               try
               {
                    PrintWriter    out  = new PrintWriter(new BufferedWriter(new FileWriter(common.getPrintPath()+"Raj.err")));
                                   ex   . printStackTrace(out);
                                   out  . close();
               }
               catch(Exception e)
               {
               }
          }
     }

     private void getData(String SDSN)
     {
          String QS = " Select Unit_Code,Sp_Date,SpdlWkd,NoOfSpdl From SpinTran"+
                      " where Sp_date >= '"+SStDate+"' and Sp_date <='"+SEnDate+"'";

          setProc("Fetching Data From :"+SDSN+"\n");

          try
          {
                              Class          . forName("oracle.jdbc.OracleDriver");
               String         OraDSN         = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
               Connection     theConnection  = DriverManager.getConnection(OraDSN,"Prod_A_0405","aunitprod");
               Statement      theStatement   = theConnection.createStatement();
               ResultSet      result         = theStatement.executeQuery(QS);

               while(result.next())
               {
                    VUnitCode . addElement(result.getString(1));
                    VDate     . addElement(result.getString(2));
                    VSpdlWkd  . addElement(result.getString(3));
                    VNoSpdl   . addElement(result.getString(4));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System    . out.println("Exception in Posting"+ex);
               TA        . append("\n"+ex.getMessage()+"\n");
          }
     }

     private void getOracleData()
     {
          setProc("Fetching Data From Oracle\n");
          try
          {
                              Class          . forName("oracle.jdbc.OracleDriver");
               String         OraDSN         = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
               Connection     theConnection  = DriverManager.getConnection(OraDSN,"Prod_B_0405","bunitprod");
               Statement      theStatement   = theConnection.createStatement();
               ResultSet      result         = theStatement.executeQuery(getQString());

               while(result.next())
               {
                    VUnitCode . addElement(result.getString(1));
                    VDate     . addElement(result.getString(2));
                    VSpdlWkd  . addElement(result.getString(3));
                    VNoSpdl   . addElement(result.getString(4));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Exception in Oracle"+ex);
               ex   . printStackTrace();
               TA   . append("\n"+ex.getMessage()+"\n");
          }
     }

     private String getQString()
     {
          String QS =    " Select Unit_Code,Sp_Date,SpdlWkd,NoOfSpdl From SpinTran" +
                         " where  Sp_date >= '"+SStDate+"' and Sp_date <='"+SEnDate+"'";

          return QS;
     }
     
     private void setProc(String str)
     {
          TA.append(str+"\n");
     }
}
