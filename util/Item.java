package util;

import java.util.*;
import java.sql.*;
import java.io.*;

import jdbc.*;
import guiutil.*;

public class Item
{
     public    String SRate;

     String    SCode,SName,SUoM,SBin,SDraw,SCatl;

     double    dROL,dMinQty,dMaxQty;
     double    dOpgQty,dOpgValue;
     Vector    VDocDate,VDocNo,VDocId,VBlock;
     Vector    VQty,VValue,VSupplier;
     Vector    VUnit,VDept,VGroup,VType,VGrnType;
     int       iMillCode = 0;
     String    SEnDate;
     String    SItemTable;
     String    SSupTable;
     Vector    VGUserCode;

     Common    common = new Common();

     public Item(String SCode,int iMillCode,String SItemTable,String SSupTable)
     {
          this.SCode      = SCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          dOpgQty        = 0;
          dOpgValue      = 0;

          setVectors();
     }

     public Item(String SCode,String SEnDate,int iMillCode,String SItemTable,String SSupTable)
     {
          this.SCode      = SCode;
          this.SEnDate    = SEnDate;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;

          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          dOpgQty        = 0;
          dOpgValue      = 0;

          setVectors(SEnDate);
     }

     public Item(String SCode,int iMillCode,String SItemTable,String SSupTable,Vector VGUserCode)
     {
          this.SCode      = SCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.VGUserCode = VGUserCode;

          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          dOpgQty        = 0;
          dOpgValue      = 0;

          setVectors(VGUserCode);
     }

     public Item(String SCode,String SEnDate,int iMillCode,String SItemTable,String SSupTable,Vector VGUserCode)
     {
          this.SCode      = SCode;
          this.SEnDate    = SEnDate;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          this.VGUserCode = VGUserCode;

          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          dOpgQty        = 0;
          dOpgValue      = 0;

          setVectors(SEnDate,VGUserCode);
     }

     private void setVectors()
     {
          ResultSet result = null;

          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();

          String QString  =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder "+
                              " where Rate > 0 and item_code = '"+SCode+"' and"+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
				              " group by id,item_code,orderdate ";

          String QStrPYOrd=   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                              " where Rate > 0 and item_code = '"+SCode+"' and"+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
				          " group by id,item_code,orderdate ";

          try
          {
               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
                              result         = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result    . close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(common.toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);
     
                    result.close();
               }

               result    = stat.executeQuery(getQS1());

               while(result.next())
               {
                    VDocDate  . addElement(common.parseDate(result.getString(1)));
                    VDocNo    . addElement(result.getString(2));
                    VBlock    . addElement(result.getString(3));
                    VQty      . addElement(result.getString(4));
                    VValue    . addElement(result.getString(5));
                    VSupplier . addElement(result.getString(6));
                    VUnit     . addElement(result.getString(7));
                    VDept     . addElement(result.getString(8));
                    VGroup    . addElement(result.getString(9));
                    VType     . addElement(result.getString(10));
                    VGrnType  . addElement(result.getString(11));
                    VDocId    . addElement(result.getString(12));
               }
               result    . close();

               String QS = "";
               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UOM.UoMName,LOCNAME,OPGQTY,OPGVAl,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl From InvItems "+
                         " inner join UOM on UOM.UOMCode = InvItems.UOMCode"+
                         " Where Item_Code='"+SCode+"'";
               }
               else
               {
                    QS = " Select Item_Name,UOM.UoMName,"+SItemTable+".LOCNAME,"+SItemTable+".OPgQty,"+SItemTable+".OPGVAl,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " inner join UOM on UOM.UOMCode = InvItems.UOMCode Where "+SItemTable+".Item_Code='"+SCode+"'";
               }

               result = stat.executeQuery(QS);
               while(result.next())
               {
                    SName     = result.getString(1);
                    SUoM      = result.getString(2);
                    SBin      = result.getString(3);
                    dOpgQty   = result.getDouble(4);
                    dOpgValue = result.getDouble(5);
                    dROL      = result.getDouble(6);
                    dMinQty   = result.getDouble(7);
                    dMaxQty   = result.getDouble(8);
                    SDraw     = result.getString(9);
                    SCatl     = result.getString(10);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setVectors(String SEnDate)
     {
          ResultSet result = null;

          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();

          String QString  =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder"+
                              " where Rate > 0 And item_code = '"+SCode+"' and"+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
				              " group by id,item_code,orderdate ";

          String QStrPYOrd=   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                              " where Rate > 0 And item_code = '"+SCode+"' and"+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
				              " group by id,item_code,orderdate ";
          try
          {
               ORAConnection connect    = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement stat           = theConnection.createStatement();

               result = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result.close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(common.toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);
     
                    result.close();
               }

               result = stat.executeQuery(getQS1(SEnDate));

               while(result.next())
               {
                    VDocDate  . addElement(common.parseDate(result.getString(1)));
                    VDocNo    . addElement(result.getString(2));
                    VBlock    . addElement(result.getString(3));
                    VQty      . addElement(result.getString(4));
                    VValue    . addElement(result.getString(5));
                    VSupplier . addElement(result.getString(6));
                    VUnit     . addElement(result.getString(7));
                    VDept     . addElement(result.getString(8));
                    VGroup    . addElement(result.getString(9));
                    VType     . addElement(result.getString(10));
                    VGrnType  . addElement(result.getString(11));
                    VDocId    . addElement(result.getString(12));
               }
               result.close();

               String QS = "";

               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UoM.UoMName,LOCNAME,OPGQTY,OPGVAl,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl From InvItems "+
                         " inner join UOM on UOM.UOMCOde = InvItems.UOMCode Where Item_Code='"+SCode+"'";
               }
               else
               {
                    QS = " Select Item_Name,UoM.UoMName,"+SItemTable+".LOCNAME,"+SItemTable+".OPgQty,"+SItemTable+".OPGVAl,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " inner join UOM on UOM.UOMCOde = InvItems.UOMCode Where "+SItemTable+".Item_Code='"+SCode+"'";
               }

               result = stat.executeQuery(QS);
               while(result.next())
               {
                    SName     = result.getString(1);
                    SUoM      = result.getString(2);
                    SBin      = result.getString(3);
                    dOpgQty   = result.getDouble(4);
                    dOpgValue = result.getDouble(5);
                    dROL      = result.getDouble(6);
                    dMinQty   = result.getDouble(7);
                    dMaxQty   = result.getDouble(8);
                    SDraw     = result.getString(9);
                    SCatl     = result.getString(10);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setVectors(Vector VGUserCode)
     {
          ResultSet result = null;

          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();

          String QString  =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder "+
                              " where Rate > 0 and item_code = '"+SCode+"' and"+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
				          " group by id,item_code,orderdate ";

          String QStrPYOrd=   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                              " where Rate > 0 and item_code = '"+SCode+"' and"+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
				          " group by id,item_code,orderdate ";

          try
          {
               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
                              result         = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result    . close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(common.toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);

                    result.close();
               }

               result    = stat.executeQuery(getQS1(VGUserCode));

               while(result.next())
               {
                    VDocDate  . addElement(common.parseDate(result.getString(1)));
                    VDocNo    . addElement(result.getString(2));
                    VBlock    . addElement(result.getString(3));
                    VQty      . addElement(result.getString(4));
                    VValue    . addElement(result.getString(5));
                    VSupplier . addElement(result.getString(6));
                    VUnit     . addElement(result.getString(7));
                    VDept     . addElement(result.getString(8));
                    VGroup    . addElement(result.getString(9));
                    VType     . addElement(result.getString(10));
                    VGrnType  . addElement(result.getString(11));
                    VDocId    . addElement(result.getString(12));
               }
               result    . close();

               String QS = "";
               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UOM.UoMName,LOCNAME,OPGQTY,OPGVAl,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl From InvItems "+
                         " inner join UOM on UOM.UOMCode = InvItems.UOMCode"+
                         " Where Item_Code='"+SCode+"'";
               }
               else
               {
                    QS = " Select Item_Name,UOM.UoMName,"+SItemTable+".LOCNAME,"+SItemTable+".OPgQty,"+SItemTable+".OPGVAl,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " inner join UOM on UOM.UOMCode = InvItems.UOMCode Where "+SItemTable+".Item_Code='"+SCode+"'";
               }

               result = stat.executeQuery(QS);
               while(result.next())
               {
                    SName     = result.getString(1);
                    SUoM      = result.getString(2);
                    SBin      = result.getString(3);
                    dOpgQty   = result.getDouble(4);
                    dOpgValue = result.getDouble(5);
                    dROL      = result.getDouble(6);
                    dMinQty   = result.getDouble(7);
                    dMaxQty   = result.getDouble(8);
                    SDraw     = result.getString(9);
                    SCatl     = result.getString(10);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setVectors(String SEnDate,Vector VGUserCode)
     {
          ResultSet result = null;

          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();

          String QString  =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder"+
                              " where Rate > 0 And item_code = '"+SCode+"' and"+
                              " id = (select max(id) from purchaseorder"+
                              " where orderdate = (select max(orderdate) from purchaseorder"+
                              " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
				          " group by id,item_code,orderdate ";

          String QStrPYOrd=   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                              " where Rate > 0 And item_code = '"+SCode+"' and"+
                              " id = (select max(id) from pyorder"+
                              " where orderdate = (select max(orderdate) from pyorder"+
                              " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
				          " group by id,item_code,orderdate ";
          try
          {
               ORAConnection connect    = ORAConnection.getORAConnection();
               Connection theConnection = connect.getConnection();
               Statement stat           = theConnection.createStatement();

               result = stat.executeQuery(QString);

               while(result.next())
                    SRate     =    result.getString(1);

               result.close();

                    /*add in 04.04.2008 for nill stock use pyorder to get rate*/

               if(common.toDouble(SRate.trim())==0)
               {
                    result = stat.executeQuery(QStrPYOrd);
                    while(result.next())
                         SRate     =    result.getString(1);
     
                    result.close();
               }

               result = stat.executeQuery(getQS1(SEnDate,VGUserCode));

               while(result.next())
               {
                    VDocDate  . addElement(common.parseDate(result.getString(1)));
                    VDocNo    . addElement(result.getString(2));
                    VBlock    . addElement(result.getString(3));
                    VQty      . addElement(result.getString(4));
                    VValue    . addElement(result.getString(5));
                    VSupplier . addElement(result.getString(6));
                    VUnit     . addElement(result.getString(7));
                    VDept     . addElement(result.getString(8));
                    VGroup    . addElement(result.getString(9));
                    VType     . addElement(result.getString(10));
                    VGrnType  . addElement(result.getString(11));
                    VDocId    . addElement(result.getString(12));
               }
               result.close();

               String QS = "";

               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UoM.UoMName,LOCNAME,OPGQTY,OPGVAl,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl From InvItems "+
                         " inner join UOM on UOM.UOMCOde = InvItems.UOMCode Where Item_Code='"+SCode+"'";
               }
               else
               {
                    QS = " Select Item_Name,UoM.UoMName,"+SItemTable+".LOCNAME,"+SItemTable+".OPgQty,"+SItemTable+".OPGVAl,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " inner join UOM on UOM.UOMCOde = InvItems.UOMCode Where "+SItemTable+".Item_Code='"+SCode+"'";
               }

               result = stat.executeQuery(QS);
               while(result.next())
               {
                    SName     = result.getString(1);
                    SUoM      = result.getString(2);
                    SBin      = result.getString(3);
                    dOpgQty   = result.getDouble(4);
                    dOpgValue = result.getDouble(5);
                    dROL      = result.getDouble(6);
                    dMinQty   = result.getDouble(7);
                    dMaxQty   = result.getDouble(8);
                    SDraw     = result.getString(9);
                    SCatl     = result.getString(10);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private String getQS1()
     {
          String str = "";

          str  =    " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                    " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                    " Issue.IssueNo as DocNo2 "+
                    " From ((Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Issue.Group_Code) "+
                    " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                    " Where Issue.Code = '"+SCode+"' "+
                    " and Issue.MillCode="+iMillCode+
                    " Union All "+
                    " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=0 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " Union All "+
                    " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=1 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " Order By 1,10";

          try
          {
               FileWriter     FW   = new FileWriter(common.getPrintPath()+"q1.txt");
                              FW   . write(str);
                              FW   . close();
          }
          catch(Exception ex){}
          return str;
     }

     private String getQS1(String SEnDate)
     {
          String str = "";

          str =     " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                    " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                    " Issue.IssueNo as DocNo2 "+
                    " From ((Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Issue.Group_Code) "+
                    " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                    " Where Issue.Code = '"+SCode+"' "+
                    " and Issue.MillCode="+iMillCode+
                    " and Issue.IssueDate <='"+SEnDate+"'"+
                    " Union All "+
                    " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=0 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.GrnDate <='"+SEnDate+"'"+
                    " Union All "+
                    " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=1 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.RejDate <='"+SEnDate+"'"+
                    " Order By 1,10";

          try
          {
               FileWriter     FW   = new FileWriter(common.getPrintPath()+"q1.txt");
                              FW   . write(str);
                              FW   . close();
          }
          catch(Exception ex){}
          return str;
     }

     private String getQS1(Vector VGUserCode)
     {
          String Str1 = "";

          int iCount=0;

          for(int i=0;i<VGUserCode.size();i++)
          {
               String SUserCode = (String)VGUserCode.elementAt(i);

               iCount++;

               if(iCount==1)
               {
                    Str1 = Str1 + SUserCode;
               }
               else
               {
                    Str1 = Str1 + ","+SUserCode;
               }
          }

          if(iCount==0)
               Str1 = Str1 + "";

          Str1 = Str1 + ")";


          String str = "";

          str  =    " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                    " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                    " Issue.IssueNo as DocNo2 "+
                    " From ((Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Issue.Group_Code) "+
                    " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                    " Where Issue.Code = '"+SCode+"' "+
                    " and Issue.MillCode="+iMillCode+
                    " and Issue.MrsAuthUserCode in ( "+Str1+
                    " Union All "+
                    " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=0 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.MrsAuthUserCode in ( "+Str1+
                    " Union All "+
                    " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=1 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.MrsAuthUserCode in ( "+Str1+
                    " Order By 1,10";

          try
          {
               FileWriter     FW   = new FileWriter(common.getPrintPath()+"q1.txt");
                              FW   . write(str);
                              FW   . close();
          }
          catch(Exception ex){}
          return str;
     }

     private String getQS1(String SEnDate,Vector VGUserCode)
     {
          String Str1 = "";

          int iCount=0;

          for(int i=0;i<VGUserCode.size();i++)
          {
               String SUserCode = (String)VGUserCode.elementAt(i);

               iCount++;

               if(iCount==1)
               {
                    Str1 = Str1 + SUserCode;
               }
               else
               {
                    Str1 = Str1 + ","+SUserCode;
               }
          }

          if(iCount==0)
               Str1 = Str1 + "";

          Str1 = Str1 + ")";


          String str = "";

          str =     " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                    " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                    " Issue.IssueNo as DocNo2 "+
                    " From ((Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Issue.Group_Code) "+
                    " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                    " Where Issue.Code = '"+SCode+"' "+
                    " and Issue.MillCode="+iMillCode+
                    " and Issue.IssueDate <='"+SEnDate+"'"+
                    " and Issue.MrsAuthUserCode in ( "+Str1+
                    " Union All "+
                    " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=0 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.GrnDate <='"+SEnDate+"'"+
                    " and Grn.MrsAuthUserCode in ( "+Str1+
                    " Union All "+
                    " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                    " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                    " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                    " 0 as DocNo2 "+
                    " From ((((Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock) "+
                    " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code) "+
                    " Inner Join Cata On Cata.Group_Code = Grn.Group_Code) "+
                    " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code) "+
                    " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                    " Where Grn.RejFlag=1 and Grn.Code = '"+SCode+"' "+
                    " and Grn.MillCode="+iMillCode+
                    " and Grn.RejDate <='"+SEnDate+"'"+
                    " and Grn.MrsAuthUserCode in ( "+Str1+
                    " Order By 1,10";

          try
          {
               FileWriter     FW   = new FileWriter(common.getPrintPath()+"q1.txt");
                              FW   . write(str);
                              FW   . close();
          }
          catch(Exception ex){}
          return str;
     }

     public String getName()
     {
          return SName;
     }

     public String getUoM()
     {
          return SUoM;
     }

     public String getCode()
     {
          return SCode;
     }

     public String getLocation()
     {
          return SBin;
     }

     public String getROLQty()
     {
          return common.getRound(dROL,3);
     }

     public String getMinQty()
     {
          return common.getRound(dMinQty,3);
     }

     public String getMaxQty()
     {
          return common.getRound(dMaxQty,3);
     }

     public String getOpgStock()
     {
          return common.getRound(dOpgQty,3);
     }

     public String getOpgValue()
     {
          return common.getRound(dOpgValue,2);
     }

     public String getClStock()
     {
          double dRecQty      = common.toDouble(getGRNQty());
          double dIssQty      = common.toDouble(getIssueQty());
          double dStockQty    = dOpgQty+dRecQty-dIssQty;

          return common       . getRound(dStockQty,3);
     }

     public String getClValue()
     {
          double dRecVal      = common.toDouble(getGRNValue());
          double dIssVal      = common.toDouble(getIssueValue());
          double dStockVal    = dOpgValue+dRecVal-dIssVal;
          return common       . getRound(dStockVal,2);
     }

     public String getGRNQty()
     {
          double dRecQty = 0;

          for(int i=0;i<VDocDate.size();i++)
          {
               int iType      = common.toInt((String)VType.elementAt(i));
               int iGrnType   = common.toInt((String)VGrnType.elementAt(i));
               
               if(iType==2 && iGrnType!=2)
                    continue;
               dRecQty = dRecQty+common.toDouble((String)VQty.elementAt(i));
          }
          return common.getRound(dRecQty,3);
     }

     public String getGRNValue()
     {
          double dRecValue = 0;

          for(int i=0;i<VDocDate.size();i++)
          {
               int iType      = common.toInt((String)VType.elementAt(i));
               int iGrnType   = common.toInt((String)VGrnType.elementAt(i));
               
               if(iType==2 && iGrnType!=2)
                    continue;

               dRecValue = dRecValue+common.toDouble((String)VValue.elementAt(i));
          }
          return common.getRound(dRecValue,2);
     }

     /*     Include Entries of NST,NSP and NPR     */

     public String getIssueQty()
     {
          double dIssQty = 0;
          for(int i=0;i<VDocDate.size();i++)
          {
               int iType     = common.toInt((String)VType.elementAt(i));
               String SBlock = ((String)VBlock.elementAt(i)).trim();
               int iGrnType  = common.toInt((String)VGrnType.elementAt(i));
               
               if((iType==1)  && (SBlock.equals("SSP") || SBlock.equals("SST")))
                    continue;
               if((iType==1 && iGrnType==2)  && (SBlock.equals("NSP") || SBlock.equals("NST") || SBlock.equals("NPR") ))
                    continue;
               
               dIssQty = dIssQty+common.toDouble((String)VQty.elementAt(i));
          }
          return common.getRound(dIssQty,3);
     }

     /*     Include Entries of NST,NSP and NPR     */

     public String getIssueValue()
     {
          double dIssValue = 0;
          for(int i=0;i<VDocDate.size();i++)
          {
               int       iType     = common.toInt((String)VType.elementAt(i));
               String    SBlock    = ((String)VBlock.elementAt(i)).trim();
               int       iGrnType  = common.toInt((String)VGrnType.elementAt(i));
               
               if((iType==1)  && (SBlock.equals("SSP") || SBlock.equals("SST")))
                    continue;
               if((iType==1 && iGrnType==2)  && (SBlock.equals("NSP") || SBlock.equals("NST") || SBlock.equals("NPR") ))
                    continue;

               dIssValue = dIssValue+common.toDouble((String)VValue.elementAt(i));
          }
          return common.getRound(dIssValue,2);
     }

     public String[] getColumnData()
     {
          String ColumnData[] = {"Date","Doc.Id","Doc.No","Type","Unit","Supplier","Dept","Class","Rec Qty","Rec Value","Iss Qty","Iss Value","Stock Qty","Stock Value","Net Rate"};
          return ColumnData;
     }

     public String[] getColumnType()
     {
          String ColumnType[] = {"S","S","S","S","S","S","S","S","N","N","N","N","N","N","N"};
          return ColumnType;
     }

     /*     Incluse Entries of NST,NSP and NPR at Both Recceipt & Issues   */

     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[VDocDate.size()+1][getColumnData().length];

          RowData[0][0]  = "";
          RowData[0][1]  = "";
          RowData[0][2]  = "";
          RowData[0][3]  = "Opening";
          RowData[0][4]  = "";
          RowData[0][5]  = "";
          RowData[0][6]  = "";
          RowData[0][7]  = "";
          RowData[0][8]  = "";
          RowData[0][9]  = "";
          RowData[0][10] = "";
          RowData[0][11] = "";
          RowData[0][12] = common.getRound(dOpgQty,3);
          RowData[0][13] = common.getRound(dOpgValue,2);
          RowData[0][14] = common.getRound(dOpgValue/dOpgQty,4);

          for(int i=0;i<VDocDate.size();i++)
          {
               int       iType     = common.toInt((String)VType.elementAt(i));
               String    SBlock    = ((String)VBlock.elementAt(i)).trim();
               int       iGrnType  = common.toInt((String)VGrnType.elementAt(i)); 

               if(iType==2)
               {
                    RowData[i+1][0]  = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]  = (String)VDocId.elementAt(i);
                    RowData[i+1][2]  = SBlock+" "+(String)VDocNo.elementAt(i);
                    RowData[i+1][3]  = "Issue";
                    RowData[i+1][4]  = (String)VUnit.elementAt(i);
                    RowData[i+1][5]  = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]  = (String)VDept.elementAt(i);
                    RowData[i+1][7]  = (String)VGroup.elementAt(i);
                    RowData[i+1][8]  = "";
                    RowData[i+1][9]  = "";
                    RowData[i+1][10] = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][11] = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][12] = common.getRound(common.toDouble((String)RowData[i][12])-common.toDouble((String)RowData[i+1][10]),3);
                    RowData[i+1][13] = common.getRound(common.toDouble((String)RowData[i][13])-common.toDouble((String)RowData[i+1][11]),2);
                    RowData[i+1][14] = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }

               if((iType==1) && (SBlock.equals("SST") || SBlock.equals("SSP")))
               {
                    RowData[i+1][0]  = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]  = (String)VDocId.elementAt(i);
                    RowData[i+1][2]  = SBlock+" "+(String)VDocNo.elementAt(i);
                    RowData[i+1][3]  = "Receipt";
                    RowData[i+1][4]  = (String)VUnit.elementAt(i);
                    RowData[i+1][5]  = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]  = (String)VDept.elementAt(i);
                    RowData[i+1][7]  = (String)VGroup.elementAt(i);
                    RowData[i+1][8]  = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]  = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10] = "";
                    RowData[i+1][11] = "";
                    RowData[i+1][12] = common.getRound(common.toDouble((String)RowData[i][12])+common.toDouble((String)RowData[i+1][8]),3);
                    RowData[i+1][13] = common.getRound(common.toDouble((String)RowData[i][13])+common.toDouble((String)RowData[i+1][9]),2);
                    RowData[i+1][14] = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }

               if((iType==1 && iGrnType!=2) && (SBlock.equals("NST") || SBlock.equals("NSP") || SBlock.equals("NPR")))
               {
                    RowData[i+1][0]  = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]  = (String)VDocId.elementAt(i);
                    RowData[i+1][2]  = SBlock+" "+(String)VDocNo.elementAt(i);
                    RowData[i+1][3]  = "Receipt & Direct Issue";
                    RowData[i+1][4]  = (String)VUnit.elementAt(i);
                    RowData[i+1][5]  = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]  = (String)VDept.elementAt(i);
                    RowData[i+1][7]  = (String)VGroup.elementAt(i);
                    RowData[i+1][8]  = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]  = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10] = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][11] = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][12] = common.getRound((String)RowData[i][12],3);
                    RowData[i+1][13] = common.getRound((String)RowData[i][13],2);
                    RowData[i+1][14] = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               } 

               if((iType==1 && iGrnType==2) && (SBlock.equals("NST") || SBlock.equals("NSP") || SBlock.equals("NPR")))
               {
                    RowData[i+1][0]  = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]  = (String)VDocId.elementAt(i);
                    RowData[i+1][2]  = SBlock+" "+(String)VDocNo.elementAt(i);
                    RowData[i+1][3]  = "Receipt";
                    RowData[i+1][4]  = (String)VUnit.elementAt(i);
                    RowData[i+1][5]  = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]  = (String)VDept.elementAt(i);
                    RowData[i+1][7]  = (String)VGroup.elementAt(i);
                    RowData[i+1][8]  = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]  = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10] = "";
                    RowData[i+1][11] = "";
                    RowData[i+1][12] = common.getRound(common.toDouble((String)RowData[i][12])+common.toDouble((String)RowData[i+1][8]),3);
                    RowData[i+1][13] = common.getRound(common.toDouble((String)RowData[i][13])+common.toDouble((String)RowData[i+1][9]),2);
                    RowData[i+1][14] = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }
          }
          return RowData;
     }

     public String getReservedQty()
     {
          String    SReservedQty   = "";
          String    Qs             = " select sum(qty-issueQty) from indent where code = '"+SCode.trim()+"' and MillCode = "+iMillCode;

           try
           {
               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(Qs);

               while(result.next())
               {
                    SReservedQty   = (String) result.getString(1);
               }

               result    . close();
               stat      . close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return SReservedQty;
     }

}
