package util;

import java.util.*;
import java.sql.*;
import java.io.*;

import jdbc.*;
import guiutil.*;

public class Item1
{
     public    String SRate;
     String    SCode,SName,SUoM,SBin,SDraw,SCatl,SStkGroup,SHsnCode;
     
     double    dROL,dMinQty,dMaxQty;
     double    dOpgQty,dOpgValue;
     double    dNonStock,dOldStock;
     Vector    VDocDate,VDocNo,VDocId,VBlock;
     Vector    VQty,VValue,VSupplier;
     Vector    VUnit,VDept,VGroup,VType,VGrnType;
     String    SStDate,SEnDate;
     int       iMillCode;
     String    SItemTable;
     String    SSupTable;
	 double    dServiceStock;

     Common    common = new Common();
     
     public Item1(String SCode,int iMillCode,String SItemTable,String SSupTable)
     {
          this.SCode      = SCode;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          SDraw          = "";
          SCatl          = "";
          SStkGroup      = "";
          dOpgQty        = 0;
          dOpgValue      = 0;
          dNonStock      = 0;
          dOldStock      = 0;
		  dServiceStock  = 0;

          setVectors();
     }
     
     public Item1(String SCode,String SStDate,String SEnDate,int iMillCode,String SItemTable,String SSupTable)
     {
          this.SCode      = SCode;
          this.SStDate    = SStDate;
          this.SEnDate    = SEnDate;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;
          this.SSupTable  = SSupTable;
          
          SName          = "";
          SBin           = "";
          SUoM           = "";
          SRate          = "";
          SDraw          = "";
          SCatl          = "";
          SStkGroup      = "";
          dOpgQty        = 0;
          dOpgValue      = 0;
          dNonStock      = 0;
          dOldStock      = 0;
		  dServiceStock  = 0;

          setVectors(SStDate,SEnDate);
     }

     private void setVectors()
     {
          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();
          
          try
          {
               String SQuery  =    " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder"+
                                   " where Rate > 0 and item_code = '"+SCode+"' and"+
                                   " id = (select max(id) from purchaseorder"+
                                   " where orderdate = (select max(orderdate) from purchaseorder"+
                                   " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
	 				          " group by id,item_code,orderdate ";

               String SQuPyRa =    " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                                   " where Rate > 0 and item_code = '"+SCode+"' and"+
                                   " id = (select max(id) from pyorder"+
                                   " where orderdate = (select max(orderdate) from pyorder"+
                                   " where item_code = '"+SCode+"') and  item_code = '"+SCode+"')"+
	 				          " group by id,item_code,orderdate ";

               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      res0           = stat.executeQuery(SQuery);

               while(res0.next())
                    SRate=res0.getString(1);

                res0.close();

               if(common.toDouble(SRate)==0)
               {
                    ResultSet respy = stat.executeQuery(SQuPyRa);

                    while(respy.next())
                         SRate=respy.getString(1);
                    
                    respy.close();
               }
               
               ResultSet res1 = stat.executeQuery(getQS1());
               
               while(res1.next())
               {
                    VDocDate  . addElement(common.parseDate(res1.getString(1)));
                    VDocNo    . addElement(res1.getString(2));
                    VBlock    . addElement(res1.getString(3));
                    VQty      . addElement(res1.getString(4));
                    VValue    . addElement(res1.getString(5));
                    VSupplier . addElement(res1.getString(6));
                    VUnit     . addElement(res1.getString(7));
                    VDept     . addElement(res1.getString(8));
                    VGroup    . addElement(res1.getString(9));
                    VType     . addElement(res1.getString(10));
                    VGrnType  . addElement(res1.getString(11));
                    VDocId    . addElement(res1.getString(12));
               }
               res1.close();
               
               String QS = "";
               
               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UoMName,locname,OPGQty,OpgVal,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl,StkGroupCode From InvItems"+
                         " inner join uom on uom.uomcode = invitems.uomcode and Item_Code='"+SCode+"'";
						 
						 //Inner join GRN on GRN.Code = invitems.Item_Code ;
               }
               else
               {
                    QS = " Select Item_Name,UoMName,"+SItemTable+".locname,"+SItemTable+".OPGQty,"+SItemTable+".OpgVal,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl,"+SItemTable+".StkGroupCode From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " and "+SItemTable+".Item_Code='"+SCode+"'"+
                         " inner join uom on uom.uomcode = invitems.uomcode";
               }

               ResultSet res2 = stat.executeQuery(QS);

               while(res2.next())
               {
                    SName     = res2.getString(1);
                    SUoM      = res2.getString(2);
                    SBin      = res2.getString(3);
                    dOpgQty   = res2.getDouble(4);
                    dOpgValue = res2.getDouble(5);
                    dROL      = res2.getDouble(6);
                    dMinQty   = res2.getDouble(7);
                    dMaxQty   = res2.getDouble(8);
                    SDraw     = res2.getString(9);
                    SCatl     = res2.getString(10);
                    SStkGroup = res2.getString(11);
               }
               res2.close();

               String NonQS = " Select Stock from ItemStock Where HodCode=6125 and MillCode="+iMillCode+" and ItemCode='"+SCode+"'";

               res2 = stat.executeQuery(NonQS);

               while(res2.next())
               {
                    dNonStock = res2.getDouble(1);
               }
               res2.close();
			   
			    String ServiceQS = " Select Stock from ItemStock Where HodCode=10371 and MillCode="+iMillCode+" and ItemCode='"+SCode+"'";
			    
			    res2 = stat.executeQuery(ServiceQS);
				
				while(res2.next())
               {
                    dServiceStock = res2.getDouble(1);
               }
			   res2.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setVectors(String SStDate,String SEnDate)
     {
          VDocDate  = new Vector();
          VDocNo    = new Vector();
          VBlock    = new Vector();
          VQty      = new Vector();
          VValue    = new Vector();
          VSupplier = new Vector();
          VUnit     = new Vector();
          VDept     = new Vector();
          VGroup    = new Vector();
          VType     = new Vector();
          VGrnType  = new Vector();
          VDocId    = new Vector();

          try
          {
               String SQuery   =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from purchaseorder"+
                                   " where Rate > 0 And item_code = '"+SCode+"' and"+
                                   " id = (select max(id) from purchaseorder"+
                                   " where orderdate = (select max(orderdate) from purchaseorder"+
                                   " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
	 				          " group by id,item_code,orderdate ";
     
               String SQuPyRa  =   " select round(sum(Net/Qty),4) as rate,id,item_code,orderdate from pyorder"+
                                   " where Rate > 0 And item_code = '"+SCode+"' and"+
                                   " id = (select max(id) from pyorder"+
                                   " where orderdate = (select max(orderdate) from pyorder"+
                                   " where item_code = '"+SCode+"' and orderdate<='"+SEnDate+"') and  item_code = '"+SCode+"'and orderdate<='"+SEnDate+"')"+
	 				          " group by id,item_code,orderdate ";

               ORAConnection  connect        = ORAConnection.getORAConnection();
               Connection     theConnection  = connect.getConnection();
               Statement      stat           = theConnection.createStatement();
               
               ResultSet res0 = stat.executeQuery(SQuery);
               while(res0.next())
                    SRate=res0.getString(1);
               
               res0.close();

               if(common.toDouble(SRate)==0)
               {
                    ResultSet respy = stat.executeQuery(SQuPyRa);

                    while(respy.next())
                         SRate=respy.getString(1);
                    
                    respy.close();
               }

               ResultSet res1 = stat.executeQuery(getQS1(SStDate,SEnDate));
               
               while(res1.next())
               {
                    VDocDate  . addElement(common.parseDate(res1.getString(1)));
                    VDocNo    . addElement(res1.getString(2));
                    VBlock    . addElement(res1.getString(3));
                    VQty      . addElement(res1.getString(4));
                    VValue    . addElement(res1.getString(5));
                    VSupplier . addElement(res1.getString(6));
                    VUnit     . addElement(res1.getString(7));
                    VDept     . addElement(res1.getString(8));
                    VGroup    . addElement(res1.getString(9));
                    VType     . addElement(res1.getString(10));
                    VGrnType  . addElement(res1.getString(11));
                    VDocId    . addElement(res1.getString(12));
               }
               res1.close();

               double dOpgGrnQty   = 0;
               double dOpgGrnVal   = 0;
               double dOpgIssQty   = 0;
               double dOpgIssVal   = 0;
               
               String SGrnFilter   = "";
               String SIssueFilter = "";
               
               SGrnFilter     = " And  GRN.MillCode = "+iMillCode;
               SIssueFilter   = " And  Issue.MillCode = "+iMillCode;

               String QS3  =  " Select Sum(OpgGrnQty) as OpgGrnQty, sum(OpgGrnVal) as OpgGrnVal from "+
                              " (SELECT Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal "+
                              " FROM GRN "+
                              " WHERE (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.GrnDate <'"+SStDate+"' "+SGrnFilter+" "+
                              " and Grn.RejFlag=0 and Grn.Code='"+SCode+"'"+
                              " Union All "+
                              " SELECT Sum(Grn.GrnQty) as OpgGrnQty, Sum(GRN.GrnValue) as OpgGrnVal "+
                              " FROM GRN "+
                              " WHERE (Grn.GrnBlock<2 Or Grn.GrnType=2)  And GRN.RejDate <'"+SStDate+"' "+SGrnFilter+" "+
                              " and Grn.RejFlag=1 and Grn.Code='"+SCode+"')";

               String QS4  =  " SELECT Sum(Issue.Qty) as OpgIssQty, Sum(Issue.IssueValue) as OpgIssVal "+
                              " FROM Issue "+
                              " WHERE Issue.IssueDate <'"+SStDate+"' "+SIssueFilter+" "+
                              " and Issue.Code='"+SCode+"'";

               ResultSet res3 = stat.executeQuery(QS3);
               while(res3.next())
               {
                    dOpgGrnQty = common.toDouble(res3.getString(1));
                    dOpgGrnVal = common.toDouble(res3.getString(2));
               }
               res3.close();
               
               ResultSet res4 = stat.executeQuery(QS4);
               while(res4.next())
               {
                    dOpgIssQty = common.toDouble(res4.getString(1));
                    dOpgIssVal = common.toDouble(res4.getString(2));
               }
               res4.close();

               String QS = "";

               if(iMillCode == 0)
               {
                    QS = " Select Item_Name,UoMName,locname,OPGQty,OpgVal,ROQ+MinQty,MinQty, "+
                         " MaxQty,Draw,Catl,StkGroupCode ,hsncode From InvItems"+
                         " inner join uom on uom.uomcode = invitems.uomcode and Item_Code='"+SCode+"'";
               }
               else
               {
                    QS = " Select Item_Name,UoMName,"+SItemTable+".locname,"+SItemTable+".OPGQty,"+SItemTable+".OpgVal,"+SItemTable+".ROQ+"+SItemTable+".MinQty,"+SItemTable+".MinQty, "+
                         " "+SItemTable+".MaxQty,Draw,Catl,"+SItemTable+".StkGroupCode,hsncode From "+SItemTable+" Inner Join InvItems On InvItems.Item_Code = "+SItemTable+".Item_Code "+
                         " and "+SItemTable+".Item_Code='"+SCode+"'"+
                         " inner join uom on uom.uomcode = invitems.uomcode";
               }
               ResultSet res2 = stat.executeQuery(QS);
               while(res2.next())
               {
                    SName     = res2 . getString(1);
                    SUoM      = res2 . getString(2);
                    SBin      = res2 . getString(3);
                    dOpgQty   = res2 . getDouble(4)+dOpgGrnQty-dOpgIssQty;
                    dOpgValue = res2 . getDouble(5)+dOpgGrnVal-dOpgIssVal;
                    dROL      = res2 . getDouble(6);
                    dMinQty   = res2 . getDouble(7);
                    dMaxQty   = res2 . getDouble(8);
                    SDraw     = res2 . getString(9);
                    SCatl     = res2 . getString(10);
                    SStkGroup = res2 . getString(11);
                    SHsnCode  = res2.  getString(12);
               }
               res2 . close();

               String NonQS = " Select Stock from ItemStock Where HodCode=6125 and MillCode="+iMillCode+" and ItemCode='"+SCode+"'";

               res2 = stat.executeQuery(NonQS);

               while(res2.next())
               {
                    dNonStock = res2.getDouble(1);
               }
               res2.close();
			   
			   String ServiceQS = " Select Stock from ItemStock Where HodCode=10371 and MillCode="+iMillCode+" and ItemCode='"+SCode+"'";
			    
			    res2 = stat.executeQuery(ServiceQS);
				
				while(res2.next())
               {
                    dServiceStock = res2.getDouble(1);
               }
			   res2.close();
			   
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     private String getQS1()
     {
          String str = "";
          
          str =   " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                  " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                  " Issue.IssueNo as DocNo2 "+
                  " From Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code "+
                  " and Issue.Code = '"+SCode+"' "+
                  " and Issue.MillCode="+iMillCode+
                  " Inner Join Cata On Cata.Group_Code = Issue.Group_Code "+
                  " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                  " Union All "+
                  " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                  " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                  " 0 as DocNo2 "+
                  " From Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock "+
                  " and RejFlag=0 and Grn.Code = '"+SCode+"' "+
                  " and Grn.MillCode="+iMillCode+
                  " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code "+
                  " Inner Join Cata On Cata.Group_Code = Grn.Group_Code "+
                  " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code "+
                  " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                  " Union All "+
                  " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                  " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                  " 0 as DocNo2 "+
                  " From Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock "+
                  " and RejFlag=1 and Grn.Code = '"+SCode+"' "+
                  " and Grn.MillCode="+iMillCode+
                  " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code "+
                  " Inner Join Cata On Cata.Group_Code = Grn.Group_Code "+
                  " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code "+
                  " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                  " Order By 1,10,2";

          try
          {
               FileWriter FW = new FileWriter(common.getPrintPath()+"q1.txt");
               FW.write(str);
               FW.close();
          }
          catch(Exception ex){}
          return str;
     }

     private String getQS1(String SStDate,String SEnDate)
     {
          String str = "";

          str =   " Select Issue.IssueDate as DocDate,Issue.UIRefNo as DocNo,' ' as Block,  "+
                  " Issue.Qty as DocQty,Issue.IssueValue as DocValue,' ' as Supplier,"+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'2' as DocType,'1' as GrnType, "+
                  " Issue.IssueNo as DocNo2 "+
                  " From Issue Inner Join Dept On Issue.Dept_Code = Dept.Dept_Code "+
                  " and Issue.Code = '"+SCode+"' "+
                  " and Issue.IssueDate >= '"+SStDate+"' and Issue.IssueDate <= '"+SEnDate+"' "+
                  " and Issue.MillCode="+iMillCode+
                  " Inner Join Cata On Cata.Group_Code = Issue.Group_Code "+
                  " Inner Join Unit On Issue.Unit_Code = Unit.Unit_Code "+
                  " Union All "+
                  " Select Grn.GrnDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                  " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                  " 0 as DocNo2 "+
                  " From Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock "+
                  " and RejFlag=0 and Grn.Code = '"+SCode+"' "+
                  " and Grn.GrnDate >= '"+SStDate+"' and Grn.GrnDate <= '"+SEnDate+"' "+
                  " and Grn.MillCode="+iMillCode+
                  " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code "+
                  " Inner Join Cata On Cata.Group_Code = Grn.Group_Code "+
                  " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code "+
                  " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                  " Union All "+
                  " Select Grn.RejDate As DocDate,Grn.GrnNo as DocNo,OrdBlock.BlockName as Block, "+
                  " Grn.GrnQty as DocQty,Grn.GrnValue as DocValue,"+SSupTable+".Name as Supplier, "+
                  " Unit.Unit_Name,Dept.Dept_Name,Cata.Group_Name,'1' as DocType,'0' as GRNType, "+
                  " 0 as DocNo2 "+
                  " From Grn Inner Join OrdBlock On OrdBlock.Block = Grn.GrnBlock "+
                  " and RejFlag=1 and Grn.Code = '"+SCode+"' "+
                  " and Grn.RejDate >= '"+SStDate+"' and Grn.RejDate <= '"+SEnDate+"' "+
                  " and Grn.MillCode="+iMillCode+
                  " Inner Join Dept On Grn.Dept_Code = Dept.Dept_Code "+
                  " Inner Join Cata On Cata.Group_Code = Grn.Group_Code "+
                  " Inner Join Unit On Grn.Unit_Code = Unit.Unit_Code "+
                  " Inner Join "+SSupTable+" On Grn.Sup_Code = "+SSupTable+".Ac_Code "+
                  " Order By 1,10,2";

          try
          {
               FileWriter FW = new FileWriter(common.getPrintPath()+"q1.txt");
               FW.write(str);
               FW.close();
          }
          catch(Exception ex){}
          return str;
     }

     public String getName()
     {
          return SName;
     }

     public String getUoM()
     {
          return SUoM;
     }

     public String getCode()
     {
          return SCode;
     }

     public String getHsnCode()
     {
          return SHsnCode;
     }
    

     public String getLocation()
     {
          return SBin;
     }

     public String getStockGroupCode()
     {
          return SStkGroup;
     }

     public String getROLQty()
     {
          return common.getRound(dROL,3);
     }

     public String getMinQty()
     {
          return common.getRound(dMinQty,3);
     }

     public String getMaxQty()
     {
          return common.getRound(dMaxQty,3);
     }

     public String getOpgStock()
     {
          return common.getRound(dOpgQty,3);
     }

     public String getOpgValue()
     {
          return common.getRound(dOpgValue,2);
     }

     /* newly Added*/

     public String getCatl()
     {
          return SCatl;
     }

     public String getDraw()
     {
          return SDraw;
     }

     public String getRate()
     {
          return SRate;
     }

        /*End*/

     public String getClStock()
     {
          double dRecQty      = common.toDouble(getGRNQty());
          double dIssQty      = common.toDouble(getIssueQty());
          double dStockQty    = dOpgQty+dRecQty-dIssQty;
          return common       . getRound(dStockQty,3);
     }

     public String getClValue()
     {
          double dRecVal      = common.toDouble(getGRNValue());
          double dIssVal      = common.toDouble(getIssueValue());
          double dStockVal    = dOpgValue+dRecVal-dIssVal;
          return common       . getRound(dStockVal,2);
     }

     public String getGRNQty()
     {
          double dRecQty = 0;

          for(int i=0;i<VDocDate.size();i++)
          {
               int iType      = common.toInt((String)VType.elementAt(i));
               int iGrnType   = common.toInt((String)VGrnType.elementAt(i));
               
               if(iType==2 && iGrnType!=2)
                    continue;
               dRecQty = dRecQty+common.toDouble((String)VQty.elementAt(i));
          }
          return common.getRound(dRecQty,3);
     }

     public String getGRNValue()
     {
          double dRecValue = 0;
          
          for(int i=0;i<VDocDate.size();i++)
          {
               int iType      = common.toInt((String)VType.elementAt(i));
               int iGrnType   = common.toInt((String)VGrnType.elementAt(i));
               
               if(iType==2 && iGrnType!=2)
                    continue;
               
               dRecValue = dRecValue+common.toDouble((String)VValue.elementAt(i));
          }
          return common.getRound(dRecValue,2);
     }

/*
Include Entries of NST,NSP and NPR
*/

     public String getIssueQty()
     {
          double dIssQty = 0;
          for(int i=0;i<VDocDate.size();i++)
          {
               int       iType     = common.toInt((String)VType.elementAt(i));
               String    SBlock    = ((String)VBlock.elementAt(i)).trim();
               int       iGrnType  = common.toInt((String)VGrnType.elementAt(i));
               
               if((iType==1)  && (SBlock.equals("SSP") || SBlock.equals("SST")))
                    continue;
               if((iType==1 && iGrnType==2)  && (SBlock.equals("NSP") || SBlock.equals("NST") || SBlock.equals("NPR") ))
                    continue;
               
               dIssQty = dIssQty+common.toDouble((String)VQty.elementAt(i));
          }
          return common.getRound(dIssQty,3);
     }

     /*     Include Entries of NST,NSP and NPR     */

     public String getIssueValue()
     {
          double dIssValue = 0;
          for(int i=0;i<VDocDate.size();i++)
          {
               int iType     = common.toInt((String)VType.elementAt(i));
               String SBlock = ((String)VBlock.elementAt(i)).trim();
               int iGrnType  = common.toInt((String)VGrnType.elementAt(i));
               
               if((iType==1)  && (SBlock.equals("SSP") || SBlock.equals("SST")))
                    continue;
               if((iType==1 && iGrnType==2)  && (SBlock.equals("NSP") || SBlock.equals("NST") || SBlock.equals("NPR") ))
                    continue;
               
               dIssValue = dIssValue+common.toDouble((String)VValue.elementAt(i));
          }
          return common.getRound(dIssValue,2);
     }

     public String[] getColumnData()
     {
          String ColumnData[] = {"Date","Doc.Id","Doc.No","Type","Unit","Supplier","Dept","Class","Rec Qty","Rec Value","Iss Qty","Iss Value","Stock Qty","Stock Value","Net Rate"};
          return ColumnData;
     }

     public String[] getColumnType()
     {
          String ColumnType[] = {"S","S","S","S","S","S","S","S","N","N","N","N","N","N","N"};
          return ColumnType;
     }

/*   Incluse Entries of NST,NSP and NPR at Both Recceipt & Issues     */

     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[VDocDate.size()+1][getColumnData().length];
          
          RowData[0][0]  = "";
          RowData[0][1]  = "";
          RowData[0][2]  = "";
          RowData[0][3]  = "Opening";
          RowData[0][4]  = "";
          RowData[0][5]  = "";
          RowData[0][6]  = "";
          RowData[0][7]  = "";
          RowData[0][8]  = "";
          RowData[0][9]  = "";
          RowData[0][10] = "";
          RowData[0][11] = "";
          RowData[0][12] = common.getRound(dOpgQty,3);
          RowData[0][13] = common.getRound(dOpgValue,2);
          RowData[0][14] = common.getRound(dOpgValue/dOpgQty,4);

          for(int i=0;i<VDocDate.size();i++)
          {
               int iType      = common.toInt((String)VType.elementAt(i));
               String SBlock  = ((String)VBlock.elementAt(i)).trim();
               int iGrnType   = common.toInt((String)VGrnType.elementAt(i)); 

               if(iType==2)
               {
                    RowData[i+1][0]          = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]          = (String)VDocId.elementAt(i);
                    RowData[i+1][2]          = SBlock+" "+(String)VDocNo.elementAt(i);
                    RowData[i+1][3]          = "Issue";
                    RowData[i+1][4]          = (String)VUnit.elementAt(i);
                    RowData[i+1][5]          = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]          = (String)VDept.elementAt(i);
                    RowData[i+1][7]          = (String)VGroup.elementAt(i);
                    RowData[i+1][8]          = "";
                    RowData[i+1][9]          = "";
                    RowData[i+1][10]         = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][11]         = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][12]         = common.getRound(common.toDouble((String)RowData[i][12])-common.toDouble((String)RowData[i+1][10]),3);
                    RowData[i+1][13]         = common.getRound(common.toDouble((String)RowData[i][13])-common.toDouble((String)RowData[i+1][11]),2);
                    RowData[i+1][14]         = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }

               if((iType==1) && (SBlock.equals("SST") || SBlock.equals("SSP")))
               {
                    double dQt               = common.toDouble(common.parseNull((String)VQty.elementAt(i)));

                    RowData[i+1][0]          = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]          = (String)VDocId.elementAt(i);
                    RowData[i+1][2]          = SBlock+" "+(String)VDocNo.elementAt(i);

                    if(dQt>0)
                         RowData[i+1][3]     = "Receipt";
                    else
                         RowData[i+1][3]     = "Rejection";

                    RowData[i+1][4]          = (String)VUnit.elementAt(i);
                    RowData[i+1][5]          = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]          = (String)VDept.elementAt(i);
                    RowData[i+1][7]          = (String)VGroup.elementAt(i);
                    RowData[i+1][8]          = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]          = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10]         = "";
                    RowData[i+1][11]         = "";
                    RowData[i+1][12]         = common.getRound(common.toDouble((String)RowData[i][12])+common.toDouble((String)RowData[i+1][8]),3);
                    RowData[i+1][13]         = common.getRound(common.toDouble((String)RowData[i][13])+common.toDouble((String)RowData[i+1][9]),2);
                    RowData[i+1][14]         = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }

               if((iType==1 && iGrnType!=2) && (SBlock.equals("NST") || SBlock.equals("NSP") || SBlock.equals("NPR")))
               {
                    double dQt               = common.toDouble(common.parseNull((String)VQty.elementAt(i)));

                    RowData[i+1][0]          = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]          = (String)VDocId.elementAt(i);
                    RowData[i+1][2]          = SBlock+" "+(String)VDocNo.elementAt(i);

                    if(dQt>0)
                         RowData[i+1][3]     = "Receipt & Direct Issue";
                    else
                         RowData[i+1][3]     = "Rejection";

                    RowData[i+1][4]          = (String)VUnit.elementAt(i);
                    RowData[i+1][5]          = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]          = (String)VDept.elementAt(i);
                    RowData[i+1][7]          = (String)VGroup.elementAt(i);
                    RowData[i+1][8]          = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]          = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10]         = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][11]         = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][12]         = common.getRound((String)RowData[i][12],3);
                    RowData[i+1][13]         = common.getRound((String)RowData[i][13],2);
                    RowData[i+1][14]         = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               } 

               if((iType==1 && iGrnType==2) && (SBlock.equals("NST") || SBlock.equals("NSP") || SBlock.equals("NPR")))
               {
                    double dQt               = common.toDouble(common.parseNull((String)VQty.elementAt(i)));

                    RowData[i+1][0]          = (String)VDocDate.elementAt(i);
                    RowData[i+1][1]          = (String)VDocId.elementAt(i);
                    RowData[i+1][2]          = SBlock+" "+(String)VDocNo.elementAt(i);
                    
                    if(dQt>0)
                         RowData[i+1][3]     = "Receipt";
                    else
                         RowData[i+1][3]     = "Rejection";

                    RowData[i+1][4]          = (String)VUnit.elementAt(i);
                    RowData[i+1][5]          = (String)VSupplier.elementAt(i);
                    RowData[i+1][6]          = (String)VDept.elementAt(i);
                    RowData[i+1][7]          = (String)VGroup.elementAt(i);
                    RowData[i+1][8]          = common.getRound((String)VQty.elementAt(i),3);
                    RowData[i+1][9]          = common.getRound((String)VValue.elementAt(i),2);
                    RowData[i+1][10]         = "";
                    RowData[i+1][11]         = "";
                    RowData[i+1][12]         = common.getRound(common.toDouble((String)RowData[i][12])+common.toDouble((String)RowData[i+1][8]),3);
                    RowData[i+1][13]         = common.getRound(common.toDouble((String)RowData[i][13])+common.toDouble((String)RowData[i+1][9]),2);
                    RowData[i+1][14]         = common.getRound(common.toDouble((String)RowData[i+1][13])/common.toDouble((String)RowData[i+1][12]),4);
               }
          }
          return RowData;
     }

     public String getNonStock()
     {
          return common.getRound(dNonStock,3);
     }
	  public String getServiceStock()
     {
          return common.getRound(dServiceStock,3);
     }

}
