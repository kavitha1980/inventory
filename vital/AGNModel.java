package vital;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;
public class AGNModel extends DefaultTableModel
{
        Object    RowData[][],ColumnNames[],ColumnType[];
        Common common = new Common();
        public AGNModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
        }
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column,0));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
     public void setAccQty(Vector RowVector,String SAcQty)
     {
          RowVector.setElementAt(common.getRound(SAcQty,2),6);
     }
     public Object[][] getFromVector()
     {
          Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
          for(int i=0;i<super.dataVector.size();i++)
          {
               Vector curVector = (Vector)super.dataVector.elementAt(i);
               for(int j=0;j<curVector.size();j++)
                    FinalData[i][j] = (String)curVector.elementAt(j);
          }
          return FinalData;
     }
     public int getRows()
     {
          return super.dataVector.size();
     }
     public Vector getCurVector(int i)
     {
          return (Vector)super.dataVector.elementAt(i);
     }
     public void removeAllElements()
     {
          try
          {
               for(int i=0;i<getRows();i++)
                    removeRow(i);
          }
          catch(Exception ex){}
     }
     public void appendRow(int i)
     {
           Vector v1 = new Vector();
           for(int j=0;j<ColumnType.length;j++)
                 v1.addElement(" ");
           insertRow(i,v1);
      }
}
