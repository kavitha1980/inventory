package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class BookToMasterFrame extends JInternalFrame 
{

     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TBookTo;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VBookName;
     Common         common = new Common();

     Connection theConnection = null;

     int iUserCode=0;

     public  BookToMasterFrame(JLayeredPane Layer,int iUserCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          //setVector();
          //setTabReport();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          TBookTo        = new AddressField(30);
          VBookName      = new Vector();
          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("BookTo Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }
     private void addComponents()
     {

          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel.add(new JLabel("BookTo Name"));
          TopPanel.add(TBookTo);

          MiddlePanel.add("Center",thePane);
          thePane.addTab("BookTo List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"BOOKTO NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VBookName.size()][ColumnName.length];
               for(int i=0;i<VBookName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VBookName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("BookTo List ",theReport);
                    else
                         thePane.addTab("BookTo List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TBookTo.setText("");
                    TBookTo.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TBookTo.getText());

               if(common.parseNull(TBookTo.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"BookTo Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TBookTo.requestFocus();
                    return false;
               }
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "BookTo Name already Exists", "Error", 
                    JOptionPane.ERROR_MESSAGE);
                    TBookTo.requestFocus();
                    return false;
               }

               if(TBookTo.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid BookTo Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TBookTo.requestFocus();
                    TBookTo.setText("");
                    return false;
               }
               if(TBookTo.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"BookTo must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TBookTo.requestFocus();
                    TBookTo.setText("");
                    return false;
               } 
               return true;
          }

          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               int iBookCode         = getMaxBookCode();
               String SBookName      = TBookTo.getText().trim().toUpperCase();

               insertBookTo(iBookCode,SBookName,iUserCode);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New BookTo Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VBookName   = new Vector();

          String QS   = "Select ToName from BookTo ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VBookName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertBookTo(int iBookCode,String SBookName,int iUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertBookToQS(iBookCode,SBookName,iUserCode));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInsertBookToQS(int iBookCode,String SBookName,int iUserCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into BookTo(id,ToCode,ToName,UserCode,CreationDate,Status,Flag) Values (";
               QS = QS+" BookTo_seq.nextval ,";
               QS = QS+" "+iBookCode+",";
               QS = QS+" '"+SBookName+"',";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ,";
               QS = QS+" 0 , ";
               QS = QS+" 0 ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert BookTo "+e);
          }
          return QS;
     }
     public  int getMaxBookCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxBookQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxBookQS()
     {
          String QS = " Select Max(ToCode)+1 from BookTo ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select ToName from BookTo Where ToName='"+SName+"' ";
          return QS;
     }



}

