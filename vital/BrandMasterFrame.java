package vital;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class BrandMasterFrame extends JInternalFrame
{
 
     protected    JLayeredPane Layer;
   
     JPanel       TopPanel,MiddlePanel,BottomPanel;

     JButton      BSubmit,BExit;

     Vector VCode,VName;
      
     JTabbedPane    thePane; 
     private   JTable              theTable;

     JTextField TName;

     JLabel       LEmpName,LMaxSize;

     Common common  = new Common();

     private        ArrayList                     theList;

   
     Vector         theVector;
   

     TabReport      theReport;

     public BrandMasterFrame(JLayeredPane Layer)
     {
                
         try
          {
          
              this.Layer   = Layer;
              createComponents();
              setDataIntoVector();
              setTabReport();
              setLayouts();
              addComponents();
              addListeners();
          
         }
        catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }

     }

    public void createComponents()
     {
          TopPanel            = new JPanel();
          MiddlePanel         = new JPanel();
          BottomPanel         = new JPanel();
          thePane             = new JTabbedPane();
          LEmpName            = new JLabel("");
                
          TName            = new JTextField();

             
          BSubmit             = new JButton("Save");
          BExit               = new JButton("Exit");
          
         
     }
    
  public void setLayouts()
     {

          setTitle("Brand Name Frame");
          setMaximizable(true);
          setClosable(true);
          setResizable(true);
          setIconifiable(true);
          setSize(700,400);
          show();

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());
     }

  public void addComponents()
     {

          
          TopPanel.add(new MyLabel(" Name"));
          TopPanel.add(TName);

          TopPanel.add("Right",new MyLabel("(Max,30)"));

          MiddlePanel.add("Center",thePane);
          thePane.addTab("Brand Name List",theReport);
        
          
          BottomPanel.add(BSubmit);
          BottomPanel.add(BExit);

          BSubmit.setMnemonic('S');
          BExit.setMnemonic('E');
         

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }
   public void addListeners()
     {
         
          BSubmit.addActionListener(new ActList());
          BExit.addActionListener(new ActList());

     }

	 private class ActList implements ActionListener
         {
          public void actionPerformed(ActionEvent ae)
          {
                    try
                    {
                         if (ae.getSource() == BSubmit)
                         {
                             if(isValidData())
                             {
                              if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
                                  {
                                     SaveDetails();
                                     setDataIntoVector();
                                     setTabReport();
                                     getContentPane().remove(MiddlePanel);
                                     MiddlePanel.remove(thePane);
                                     thePane = new JTabbedPane();
                                     if(theReport != null)
                                          thePane.addTab("Brand Name List ",theReport);
                                     MiddlePanel.add("Center",thePane);
                                     getContentPane().add("Center",MiddlePanel);
                                     updateUI();
                                     TName.setText("");
                                     TName.requestFocus();
                                  }
                             }
                             
                         }
                        
                        if (ae.getSource() == BExit)
                           {
                              removeHelpFrame();
                           } 
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
                    
               }
            }
            private void removeHelpFrame()
            {
              try
                {
                   Layer.remove(this);
                   Layer.updateUI();
                }
              catch(Exception ex)
              {
                ex.printStackTrace();
              }
           }
               public void itemStateChanged(ItemEvent ie)
               {
                    try
                    {
                         //clearData();
                         setDataIntoVector();
                         //refresh();
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }

               }
	
 

    public void setDataIntoVector()
     {
          int iCode=0;
          String SName="";
         
          VCode = new Vector();
          VName = new Vector();

        
 
       try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      thestatement  =  theConnection.createStatement();
               ResultSet      rs            =  thestatement.executeQuery("select Code,Name from Brand");
               
               while (rs.next())
               {
                          VName.addElement(rs.getString(2));
                          VCode.addElement(rs.getString(1));
                                	
               }
          } catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
      }         
   private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TName.getText());
               String SName = common.getNarration(TName.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, " Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    TName.requestFocus();
                    return false;
               }
               if(SName.length()>30)
               {
               JOptionPane.showMessageDialog(null,"Name Can't Exceed 30 Char. Current Length - "+SName.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TName.requestFocus();
               return false;
               }
                if(common.parseNull(TName.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null," Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TName.requestFocus();
                    return false;
               }
               if(TName.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid  Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TName.requestFocus();
                    TName.setText("");
                    return false;
               }
               if(TName.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null," Name must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TName.requestFocus();
                    TName.setText("");
                    return false;
               } 
               return true;
           }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }
      private void setTabReport()
        {
          try
          {
               String ColumnName[] = {"Name"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {200};

               Object RowData[][] = new Object[VCode.size()][ColumnName.length];
               for(int i=0;i<VName.size();i++)
               {
                     RowData[i][0] = common.parseNull((String)VName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          } catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
               ex.printStackTrace();
          }
        

     }
   
 private void SaveDetails()
     {
            int  iInsertCount=0;
          try
          {
              ORAConnection oraConnection     =   ORAConnection.getORAConnection();
              Connection theConnection           =   oraConnection.getConnection();
             
              theConnection                 . setAutoCommit(false);
	   

                 String Name      = TName.getText().trim().toUpperCase();
                 PreparedStatement ps  = null;
                 ps               = theConnection.prepareStatement(getSaveQS());

                 ps.setString(1,Name);
                 iInsertCount =  ps               . executeUpdate();

                 ps               . close();
                 ps               = null;
                 iInsertCount  = iInsertCount+1;
	
	      if(iInsertCount>0)
	      {	      	
	              JOptionPane.showMessageDialog(null, "Data Saved Succesfully");
	              
              }
	      else
	      {
			JOptionPane.showMessageDialog(null," Problem in Saving");
			
              }
              
              theConnection                 . commit();
	       theConnection		    = null;
           
          }catch (Exception ex)
          {
              System.out.println(ex);
              ex.printStackTrace();
              JOptionPane.showMessageDialog(null, "Problem in  Saving Data", "Error", 
                   JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
	      try
	      {
              ORAConnection oraConnection     =   ORAConnection.getORAConnection();
              Connection theConnection           =   oraConnection.getConnection();
	      theConnection		. rollback(); } catch(Exception e){}
          }
	  
     }
 private String getSaveQS()
     {
          StringBuffer SB= null;
          SB             = new StringBuffer();

          SB.append(" Insert Into Brand ");
          SB.append(" ( ");
          SB.append(" Code ,Name");
          SB.append(" ) ");
          SB.append(" Values ");
          SB.append(" ( ");
          SB.append("  Brand_Seq.nextval , ?");
          SB.append(" ) ");

          return SB.toString();
     }

public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               ORAConnection  oraConnection =  ORAConnection.getORAConnection();
               Connection     theConnection =  oraConnection.getConnection();               
               Statement      thestatement  =  theConnection.createStatement();
               ResultSet theResult       = thestatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               thestatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select Code,Name from Brand Where Name='"+SName+"' ";
          return QS;
     }
   
 }
