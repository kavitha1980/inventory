package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class DepartmentMasterFrame extends JInternalFrame 
{

     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TDepartment;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VDeptName,VSDeptName;
     Vector         VMillName,VMillCode;
     Common         common = new Common();

     MyComboBox     JCBranch;

     Connection theConnection = null;

     int iUserCode=0;

     public  DepartmentMasterFrame(JLayeredPane Layer,int iUserCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;

          setMillVector();
          setVectorSample();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          TDepartment    = new AddressField(30);
          VDeptName      = new Vector();
          VSDeptName     = new Vector();
          JCBranch       = new MyComboBox(VMillName);

          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("Department Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(2,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }
     private void addComponents()
     {

          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel.add(new JLabel("Department Name"));
          TopPanel.add(TDepartment);
          TopPanel.add(new JLabel("Company Name"));
          TopPanel.add(JCBranch);


          MiddlePanel.add("Center",thePane);
          thePane.addTab("Department List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"DEPARTMENT NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VDeptName.size()][ColumnName.length];
               for(int i=0;i<VDeptName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VDeptName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("Department List ",theReport);
                    else
                         thePane.addTab("Department List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TDepartment.setText("");
                    TDepartment.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TDepartment.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Department Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    TDepartment.requestFocus();
                    return false;
               }

               if(common.parseNull(TDepartment.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Department Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TDepartment.requestFocus();
                    return false;
               }

               if(TDepartment.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Department Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TDepartment.requestFocus();
                    TDepartment.setText("");
                    return false;
               }
               if(TDepartment.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Department must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TDepartment.requestFocus();
                    TDepartment.setText("");
                    return false;
               } 
               return true;
          }

          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               int iDeptCode         = getMaxDeptCode();
               int iMillCode         = common.toInt((String)VMillCode.elementAt(JCBranch.getSelectedIndex()));
               String SDeptName      = TDepartment.getText().trim().toUpperCase();

               insertDepartment(iDeptCode,SDeptName,iMillCode,iUserCode);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Department Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VDeptName   = new Vector();

          String QS   = "Select Dept_Name from Dept ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VDeptName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setMillVector()
     {
          VMillName   = new Vector();
          VMillCode   = new Vector();

          VMillName.addElement("ALL");
          VMillCode.addElement("2");

          String QS   = "Select MillName,MillCode from Mill Order by 1 ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VMillName.addElement(result.getString(1));
                    VMillCode.addElement(result.getString(2));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setVectorSample()
     {
          VSDeptName   = new Vector();

          String QS   = "Select Dept_Name from Dept ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VSDeptName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     public void insertDepartment(int iDeptCode,String SDeptName,int iMillCode,int iUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertDepartmentQS(iDeptCode,SDeptName,iMillCode,iUserCode));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInsertDepartmentQS(int iDeptCode,String SDeptName,int iMillCode,int iUserCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into Dept(id,Dept_Code,Dept_Name,MillCode,TranSignal,UserCode,CreationDate,Status,Flag) Values (";
               QS = QS+" Dept_seq.nextval ,";
               QS = QS+" "+iDeptCode+",";
               QS = QS+" '"+SDeptName+"',";
               QS = QS+" "+iMillCode+" ,";
               QS = QS+" 0 ,";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ,";
               QS = QS+" 0 , ";
               QS = QS+" 0 ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Department "+e);
          }
          return QS;
     }
     public  int getMaxDeptCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxDeptQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxDeptQS()
     {
          String QS = " Select Max(Dept_Code)+1 from Dept ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select Dept_Name from Dept Where Dept_Name='"+SName+"' ";
          return QS;
     }



}

