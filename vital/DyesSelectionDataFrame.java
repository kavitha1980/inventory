package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;

import util.*;
import guiutil.*;
import jdbc.*;

import java.io.*;
import java.net.*;

public class DyesSelectionDataFrame extends JInternalFrame
{
 
    protected    JLayeredPane Layer;

    Common common  = new Common();
 
    JPanel    BottomPanel,TopPanel,MainPanel;
	 
	JLabel  LGotsApp,LOekoTex,LReach,LLight,LWashing,LWet,LDry,LExhausion,LFixiation,LMin,LMax,LDisCharge;
	JLabel  LSTDGotsApp,LSTDOekoTex,LSTDReach,LSTDLight,LSTDWashing,LSTDWet,LSTDDry,LSTDExhausion,LSTDFixiation,LSTDMin,LSTDMax,LSTDDisCharge;
     
    JButton   BExit,BOK;
    
    int iItemCode;
	 
	String SItemName;

    Vector     theVector;

    private    ArrayList            theEmpList1;
    
 
    public DyesSelectionDataFrame(JLayeredPane Layer,int iItemCode,String SItemName)
    {
	
		this.Layer          = Layer;
        this.iItemCode = iItemCode;
        this.SItemName = SItemName;
        try
        {
          
               this.Layer   = Layer;
               createComponents();
               setLayouts();
               addComponents();
               addListeners();
			   setTableDetails();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               System.out.println(ex);
          }
     }
     public void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
		  MainPanel      = new JPanel();
          BExit          = new JButton("Exit");
		  BOK            = new JButton("OK");
		 
		  LGotsApp       = new JLabel("GOTS APPROVED");
		  LSTDGotsApp    = new JLabel();
		  LOekoTex       = new JLabel("OEKO TEX APPROVED");
		  LSTDOekoTex    = new JLabel();
		  LReach         = new JLabel("REACH APPROVED");
		  LSTDReach      = new JLabel();
		  LLight         = new JLabel("LIGHT FASTNESS PASSING RANGE(AATCC)");
		  LSTDLight      = new JLabel();
		  LWashing       = new JLabel("WASHING FASTNESS AT DIFFERENT%");
		  LSTDWashing    = new JLabel();
		  LWet           = new JLabel("WET RUB");
		  LSTDWet        = new JLabel();
		  LDry           = new JLabel("DRY RUB");
		  LSTDDry        = new JLabel();
		  LExhausion     = new JLabel("EXHAUSION CHART(%)");
		  LSTDExhausion  = new JLabel();
		  LFixiation     = new JLabel("FIXIATION CHART(%)");
		  LSTDFixiation  = new JLabel();
		  LMin           = new JLabel("MINIMUM % OF DEPTH RECOMENTATION");
		  LSTDMin        = new JLabel();
		  LMax           = new JLabel("MAXIMUM % OF RECOMENTATION");
		  LSTDMax        = new JLabel();
		  LDisCharge     = new JLabel("DISCHARGABILITY");
		  LSTDDisCharge  = new JLabel();
     }
     public void setLayouts()
     {

          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
		  setBounds(10, 20, 618, 470);
          setTitle("Dyes Selection Data List");

          
          getContentPane().setLayout(new BorderLayout());
          TopPanel.setLayout(new GridLayout(12,2));
		  MainPanel.setLayout(new BorderLayout());
		  BottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
          
      }

     public void addComponents()
     {

        //  TopPanel.setBorder(new TitledBorder("Dyes Data Sheet Details"));
		  
          TopPanel.add(LGotsApp);
          TopPanel.add(LSTDGotsApp);
		  TopPanel.add(LOekoTex);
		  TopPanel.add(LSTDOekoTex);
		  TopPanel.add(LReach);
		  TopPanel.add(LSTDReach);
		  TopPanel.add(LLight);
		  TopPanel.add(LSTDLight);
		  TopPanel.add(LWashing);
		  TopPanel.add(LSTDWashing);
		  TopPanel.add(LWet);
		  TopPanel.add(LSTDWet);
		  TopPanel.add(LDry);
		  TopPanel.add(LSTDDry);
		  TopPanel.add(LExhausion);
		  TopPanel.add(LSTDExhausion);
		  TopPanel.add(LFixiation);
		  TopPanel.add(LSTDFixiation);
		  TopPanel.add(LMin);
		  TopPanel.add(LSTDMin);
		  TopPanel.add(LMax);
		  TopPanel.add(LSTDMax);
		  TopPanel.add(LDisCharge);
		  TopPanel.add(LSTDDisCharge);
		  
         // BottomPanel.setBorder(new TitledBorder("Controls"));
          BottomPanel.add(BOK);
          BottomPanel.add(BExit);
            
          MainPanel.add(TopPanel);
		  		 
          this.getContentPane().add(MainPanel,BorderLayout.CENTER);
          this.getContentPane().add(BottomPanel,BorderLayout.SOUTH);
                  

        
       }

     public void addListeners()
     {
          BExit.addActionListener(new ActList1());
          BOK.addActionListener(new ActList()); 
     }

     private class ActList1 implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               close1();
          }  
    } 
    public void close1()
    {
        try
        {
               
            dispose();
          
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
    private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
			   if(Validation())
			    {
					if(isExists())
					{
						UpdateCheckStatus();
					}
					else
					{
						JOptionPane.showMessageDialog(null, " Already Saved");
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, " Null Value in StdNorms ");
				}
          }  
    } 
    private void setTableDetails()
    {

        try
        {
               ORAConnection oraConnection     =   ORAConnection.getORAConnection();
               Connection connection           =   oraConnection.getConnection();
               Statement  theStatement         =   connection.createStatement();
               ResultSet  theResult            =   theStatement.executeQuery(getQS());
               while(theResult.next())
                    organizeData(theResult);
               theResult.close();
               theStatement.close();
        }
        catch(Exception ex)
        {
               System.out.println(ex);
               ex.printStackTrace();
        }
     

       
    }
    private String getQS()
    {
       

         String QS = " Select PropertyName,StandardNorms from DyesSelectionInfo "+
					 " Inner join DyesProperties on DyesSelectionInfo.PropertyCode = DyesProperties.PropertyCode "+
					 " where DyesSelectionInfo.ItemCode="+iItemCode;
             
          return QS;
     }

     private void organizeData(ResultSet theResult) throws Exception
     {
         String SPropertyName          = theResult.getString(1);
         String SSTDNorms              = theResult.getString(2);
		

		 if(SPropertyName.equals("GOTS APPROVED"))
			LSTDGotsApp.setText(SSTDNorms);
		 if(SPropertyName.equals("OEKO TEX APPROVED"))	
			LSTDOekoTex.setText(SSTDNorms);
		 if(SPropertyName.equals("REACH APPROVED"))
			LSTDReach.setText(SSTDNorms);
		 if(SPropertyName.equals("LIGHT FASTNESS PASSING RANGE(AATCC)"))	
			LSTDLight.setText(SSTDNorms);
		 if(SPropertyName.equals("WASHING FASTNESS AT DIFFERENT%"))	
			LSTDWashing.setText(SSTDNorms);
		 if(SPropertyName.equals("WET RUB"))	
			LSTDWet.setText(SSTDNorms);
		 if(SPropertyName.equals("DRY RUB"))	
			LSTDDry.setText(SSTDNorms);
		 if(SPropertyName.equals("EXHAUSION CHART"))	
			LSTDExhausion.setText(SSTDNorms);
		 if(SPropertyName.equals("FIXIATION CHART"))
		    LSTDFixiation.setText(SSTDNorms);
		 if(SPropertyName.equals("MINIMUM % OF DEPTH RECOMENTATION"))	
			LSTDMin.setText(SSTDNorms);
		 if(SPropertyName.equals("MAXIMUM % OF RECOMENTATION"))	
			LSTDMax.setText(SSTDNorms);
		 if(SPropertyName.equals("DISCHARGABILITY"))	
			LSTDDisCharge.setText(SSTDNorms);
		
        
         
       }
	   
	private void UpdateCheckStatus()
    {
		int iUpdateCount=0,iUpCount=0;

        try
        {
                ORAConnection oraConnection     =   ORAConnection.getORAConnection();
                Connection connection           =   oraConnection.getConnection();
			    PreparedStatement ps			=   connection.prepareStatement(getUpdateQS());
                ps    .    setInt(1,iItemCode);
			  
			    iUpCount =  ps               . executeUpdate();
                ps.close();
			    iUpdateCount = iUpdateCount + 1;
			    if(iUpdateCount>0)
				{          
                    JOptionPane.showMessageDialog(null, "Data Updated Succesfully");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Problem in Updated ");
				}
               
        }
        catch(Exception ex)
        {
               System.out.println(ex);
               ex.printStackTrace();
        }
     

       
    }
    private String getUpdateQS()
     {
          StringBuffer SB = null;
          SB = new StringBuffer();
          
          SB.append(" Update DyesSelectionInfo set CheckStatus=1 where ItemCode=? ");
             
          return SB.toString();
     }
	private boolean Validation()
    {
		if(LSTDGotsApp.getText().equals(""))
              return false;
		if(LSTDOekoTex.getText().equals(""))
              return false;
		if(LSTDReach.getText().equals(""))
              return false;
		if(LSTDLight.getText().equals(""))
              return false;
		if(LSTDWashing.getText().equals(""))
              return false;
		if(LSTDWet.getText().equals(""))
              return false;
		if(LSTDDry.getText().equals(""))
              return false;
		if(LSTDExhausion.getText().equals(""))
              return false;
		if(LSTDFixiation.getText().equals(""))
              return false;
		if(LSTDMin.getText().equals(""))
              return false;
		if(LSTDMax.getText().equals(""))
              return false;
		if(LSTDDisCharge.getText().equals(""))
              return false;
			 
		 
		return true;
	}
	
	private boolean isExists()
    {
		int iCount=0;

        try
        {
                ORAConnection oraConnection     =   ORAConnection.getORAConnection();
                Connection connection           =   oraConnection.getConnection();
			    PreparedStatement thePS			=   connection.prepareStatement("Select Count(*) from DyesSelectionInfo Where CheckStatus = 1 and  ItemCode = "+iItemCode+"  ");
	            ResultSet rs                    = thePS.executeQuery();
                         if(rs.next())
                         {
                              iCount              = rs.getInt(1);
                              System.out.println("Count:"+iCount);
                         }
                         rs                       . close();
                         thePS                    . close();
                         
                        if(iCount == 0)
                        {
								return true;
						}
						
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
		return false;
	}
    private void refresh()
     {
        try
        {
          theVector.removeAllElements();
                      
        }
        catch(Exception e)
        {
          System.out.println(e);
          e.printStackTrace();
        }

     }
 


    private void removeFrame()
     {
		try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
        catch(Exception ex){}
     }
    
}

