package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class GroupMasterFrame extends JInternalFrame 
{

     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TGroup;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VGroupName;
     Vector         VMillName,VMillCode;
     Common         common = new Common();

     MyComboBox     JCGroup,JCBranch;

     Connection theConnection = null;

     int iUserCode=0;

     public  GroupMasterFrame(JLayeredPane Layer,int iUserCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;

          setMillVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          TGroup         = new AddressField(30);
          VGroupName     = new Vector();

          JCGroup        = new MyComboBox();

          JCGroup.addItem("Maintenance");
          JCGroup.addItem("NonMaintenance");

          JCBranch       = new MyComboBox(VMillName);

          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("Group Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(3,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }
     private void addComponents()
     {

          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel.add(new JLabel("Group Or Classification Name"));
          TopPanel.add(TGroup);
          TopPanel.add(new JLabel("Costing Classification"));
          TopPanel.add(JCGroup);
          TopPanel.add(new JLabel("Company Name"));
          TopPanel.add(JCBranch);

          MiddlePanel.add("Center",thePane);
          thePane.addTab("Group List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"GROUP OR CLASSIFICATION NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VGroupName.size()][ColumnName.length];
               for(int i=0;i<VGroupName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VGroupName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("Group List ",theReport);
                    else
                         thePane.addTab("Group List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TGroup.setText("");
                    TGroup.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TGroup.getText());

               if(common.parseNull(TGroup.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Group Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TGroup.requestFocus();
                    return false;
               }
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Group Name already Exists", "Error", 
                    JOptionPane.ERROR_MESSAGE);
                    TGroup.requestFocus();
                    return false;
               }

               if(TGroup.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Group Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TGroup.requestFocus();
                    TGroup.setText("");
                    return false;
               }
               if(TGroup.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Group must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TGroup.requestFocus();
                    TGroup.setText("");
                    return false;
               } 
               return true;
          }

          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               int iGroupCode         = getMaxGroupCode();
               int iSignal            = JCGroup.getSelectedIndex();
               int iMillCode          = common.toInt((String)VMillCode.elementAt(JCBranch.getSelectedIndex()));
               String SGroupName      = TGroup.getText().trim().toUpperCase();

               insertGroup(iGroupCode,SGroupName,iMillCode,iUserCode,iSignal);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Group Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VGroupName   = new Vector();

          String QS   = "Select Group_Name from Cata ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VGroupName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setMillVector()
     {
          VMillName   = new Vector();
          VMillCode   = new Vector();

          VMillName.addElement("ALL");
          VMillCode.addElement("2");

          String QS   = "Select MillName,MillCode from Mill Order by 1 ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VMillName.addElement(result.getString(1));
                    VMillCode.addElement(result.getString(2));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void insertGroup(int iGroupCode,String SGroupName,int iMillCode,int iUserCode,int iSignal)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertGroupQS(iGroupCode,SGroupName,iMillCode,iUserCode,iSignal));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInsertGroupQS(int iGroupCode,String SGroupName,int iMillCode,int iUserCode,int iSignal)
     {
               String QS = "";
          try
          {
               QS = "Insert Into Cata(id,Group_Code,Group_Name,Signal,MillCode,TranSignal,UserCode,CreationDate,Status,Flag) Values (";
               QS = QS+" cata_seq.nextval ,";
               QS = QS+" "+iGroupCode+",";
               QS = QS+" '"+SGroupName+"',";
               QS = QS+" "+iSignal+" ,";
               QS = QS+" "+iMillCode+" ,";
               QS = QS+" 0 ,";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ,";
               QS = QS+" 0 , ";
               QS = QS+" 0 ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Group "+e);
          }
          return QS;
     }
     public  int getMaxGroupCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxGroupQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxGroupQS()
     {
          String QS = " Select Max(Group_Code)+1 from Cata ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select Group_Name from Cata Where Group_Name='"+SName+"' ";
          return QS;
     }


}

