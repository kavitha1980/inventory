package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class HodMasterFrame extends JInternalFrame 
{

     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   THodName;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VHodName,VSHodName;
     Common         common = new Common();

     //MyComboBox     JCBranch;

     Connection theConnection = null;

     int iUserCode=0;
     int iMillCode=0;

     public  HodMasterFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer     = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;

          setVectorSample();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          THodName    = new AddressField(30);
          VHodName      = new Vector();
          VSHodName     = new Vector();
          //JCBranch       = new MyComboBox();

          //JCBranch.addItem("AmarjothiSpinningMill");
          //JCBranch.addItem("AmarjothiDyeingDivision");
          //JCBranch.addItem("Both");

          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle(" HOD Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }
     private void addComponents()
     {

          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel.add(new JLabel("HOD Name"));
          TopPanel.add(THodName);


          MiddlePanel.add("Center",thePane);
          thePane.addTab("Hod List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"HOD NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VHodName.size()][ColumnName.length];
               for(int i=0;i<VHodName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VHodName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("Hod List ",theReport);
                    else
                         thePane.addTab("Hod List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    THodName.setText("");
                    THodName.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(THodName.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Hod Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    THodName.requestFocus();
                    return false;
               }
               if(common.parseNull(THodName.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Hod Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    THodName.requestFocus();
                    return false;
               }
               if(THodName.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Hod Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    THodName.requestFocus();
                    THodName.setText("");
                    return false;
               }
               if(THodName.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Hod Name must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    THodName.requestFocus();
                    THodName.setText("");
                    return false;
               } 
               return true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               int iHodCode             = getMaxHodCode();
               String SHodName          = THodName.getText().trim().toUpperCase();

               insertHodName(iHodCode,SHodName,iMillCode,iUserCode);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Hod Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VHodName   = new Vector();

          String QS   = "Select HodName from Hod ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VHodName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void setVectorSample()
     {
          VSHodName   = new Vector();

          String QS   = "Select HodName from Hod ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VSHodName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     public void insertHodName(int iHodCode,String SHodName,int iMillCode,int iUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertHodNameQS(iHodCode,SHodName,iMillCode,iUserCode));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInsertHodNameQS(int iHodCode,String SHodName,int iMillCode,int iUserCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into Hod(id,HodCode,HodName,status,flag,MillCode,UserCode,CreationDate) Values (";
               QS = QS+"  Hod_seq.nextval ,";
               QS = QS+" '"+iHodCode+"',";
               QS = QS+" '"+SHodName+"',";
               QS = QS+" 0 ,";
               QS = QS+" 0 ,";
               QS = QS+" "+iMillCode+" ,";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Hod "+e);
          }
          return QS;
     }
     public  int getMaxHodCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxHodQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxHodQS()
     {
          String QS = " Select Max(HodCode)+1 from Hod ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select HodName from Hod Where HodName='"+SName+"' ";
          return QS;
     }



}

