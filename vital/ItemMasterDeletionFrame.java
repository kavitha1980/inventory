package vital;

import util.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class ItemMasterDeletionFrame extends JInternalFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

     Common common   = new Common();

     Vector VMItemCode,VMTable,VMField;
     Vector VDItemCode,VDTable,VDField;
     Vector VDUsedCode;
     Vector VDelItemCode;

     JTextArea     TInfo;

     JButton   BApply;
     JLayeredPane layer;
     int          iMillCode;
     String       SItemTable;

     Connection theConnection  = null;
     Connection theDConnection = null;

     boolean bComflag = true;

     public ItemMasterDeletionFrame(JLayeredPane layer,int iMillCode,String SItemTable)
     {
          super("Utility for Deleting Item Master");
          this.layer      = layer;
          this.iMillCode  = iMillCode;
          this.SItemTable = SItemTable;

          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
          theConnection                 = oraConnection.getConnection();

          DORAConnection jdbc           = DORAConnection.getORAConnection();
          theDConnection                = jdbc.getConnection();

          updateLookAndFeel();
          setSize(500,500);
          
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          MiddlePanel.setLayout(new BorderLayout());

          TInfo       = new JTextArea(50,50);
          BApply      = new JButton("Apply");

          TopPanel.add(BApply);
          MiddlePanel.add(TInfo);

          BApply.addActionListener(new AppList());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          setVisible(true);
          setClosable(true);
     }
     private class AppList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
 
               BApply.setEnabled(false);
               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               MiddlePanel.add(TInfo);
               TInfo.append("Processing ....."+"\n");
 
               TInfo.append("Fetching Item Master Data ....."+"\n");
               getItemData();
               getDyeingItemData();
               getDyeingUsedData();
 
               if(VMItemCode.size()>0)
               {
                    TInfo.append("Fetching Deleted Item Data ....."+"\n");
                    getDeletedItemData();
     
                    TInfo.append("Deletion is going on ....."+"\n");
                    deleteData();
               }
               getACommit();
 
               TInfo.append("Updation Over .....");
               removeFrame();
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    theDConnection . commit();

                    JOptionPane.showMessageDialog(null,VDelItemCode.size()+" Items Deleted ");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    theDConnection . rollback();

                    System         . out.println("RollBack");
               }
               theConnection    . setAutoCommit(true);
               theDConnection   . setAutoCommit(true);

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void getItemData()
     {
          VMItemCode  = new Vector();
          VMTable     = new Vector();
          VMField     = new Vector();

          String QS1 = " Select TblName,FldName from ItemCheckTables Where CheckStatus=0 order by 1";

          try
          {
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(getItemMasterQS());
               while(theResult.next())
               {
                    VMItemCode.addElement(common.parseNull((String)theResult.getString(1)));
               }
               theResult.close();

               theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VMTable.addElement(common.parseNull((String)theResult.getString(1)));
                    VMField.addElement(common.parseNull((String)theResult.getString(2)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getDyeingItemData()
     {
          VDItemCode  = new Vector();
          VDTable     = new Vector();
          VDField     = new Vector();

          String QS1 = " Select TblName,FldName from ItemCheckTables Where CheckStatus=0 order by 1";

          try
          {
               Statement theStatement   = theDConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery("Select Distinct(Item_Code) from InvItems order by 1");
               while(theResult.next())
               {
                    VDItemCode.addElement(common.parseNull((String)theResult.getString(1)));
               }
               theResult.close();

               theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VDTable.addElement(common.parseNull((String)theResult.getString(1)));
                    VDField.addElement(common.parseNull((String)theResult.getString(2)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getDyeingUsedData()
     {
          VDUsedCode   = new Vector();

          int iCount=0;

          try
          {
               Statement theStatement   = theDConnection.createStatement();

               for(int i=0;i<VDItemCode.size();i++)
               {
                    String SItemCode = (String)VDItemCode.elementAt(i);

                    for(int j=0;j<VDTable.size();j++)
                    {
                         iCount=0;

                         String STableName  = (String)VDTable.elementAt(j);
                         String SColumnName = (String)VDField.elementAt(j);

                         String QS = " Select count(*) from "+STableName+" Where "+SColumnName+"='"+SItemCode+"'";

                         ResultSet theResult      = theStatement.executeQuery(QS);
                         while(theResult.next())
                         {
                              iCount = theResult.getInt(1);
                         }
                         theResult.close();

                         if(iCount>0)
                         {
                              VDUsedCode.addElement(SItemCode);
                              break;
                         }
                    }
               }
               theStatement.close();
               System.out.println(VDUsedCode.size());
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getDeletedItemData()
     {
          VDelItemCode   = new Vector();

          int iCount=0;

          try
          {
               Statement theStatement   = theConnection.createStatement();

               String SDelQS = " Delete from DeletedItems ";

               theStatement.execute(SDelQS);

               for(int i=0;i<VMItemCode.size();i++)
               {
                    String SItemCode = (String)VMItemCode.elementAt(i);
                    System.out.println(i+":"+SItemCode);

                    for(int j=0;j<VMTable.size();j++)
                    {
                         iCount=0;

                         String STableName  = (String)VMTable.elementAt(j);
                         String SColumnName = (String)VMField.elementAt(j);

                         String QS = " Select count(*) from "+STableName+" Where "+SColumnName+"='"+SItemCode+"'";

                         ResultSet theResult      = theStatement.executeQuery(QS);
                         while(theResult.next())
                         {
                              iCount = theResult.getInt(1);
                         }
                         theResult.close();

                         if(iCount>0)
                         {
                              break;
                         }
                    }

                    if(iCount==0)
                    {
                         int iIndex = common.indexOf(VDUsedCode,SItemCode);

                         if(iIndex<0)
                         {
                              String SInsQS = " Insert Into DeletedItems Values ('"+SItemCode+"')";

                              theStatement.execute(SInsQS);
                              VDelItemCode.addElement(SItemCode);
                         }
                    }
               }
               theStatement.close();
               System.out.println(VDelItemCode.size());
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void deleteData()
     {
          try
          {
               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               if(theDConnection  . getAutoCommit())
                    theDConnection  . setAutoCommit(false);


               Statement stat1 = theConnection.createStatement();
               Statement stat2 = theDConnection.createStatement();

               String QS1 = " Insert into DeletedInvItems Select * from InvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS2 = " Insert into DeletedDyeingInvItems Select * from DyeingInvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS3 = " Insert into DeletedMelangeInvItems Select * from MelangeInvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS4 = " Insert into DeletedKamadhenuInvItems Select * from KamadhenuInvItems Where Item_Code in (Select Item_Code from DeletedItems)";

               stat1.executeUpdate(QS1);
               stat1.executeUpdate(QS2);
               stat1.executeUpdate(QS3);
               stat1.executeUpdate(QS4);

               String QS5 = " Insert into DeletedInvItems Select * from InvItems Where Item_Code in (Select Item_Code from inventory.DeletedItems@amarml)";

               stat2.executeUpdate(QS5);


               String QS6 = " Delete from DyeingInvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS7 = " Delete from MelangeInvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS8 = " Delete from KamadhenuInvItems Where Item_Code in (Select Item_Code from DeletedItems)";
               String QS9 = " Delete from InvItems Where Item_Code in (Select Item_Code from DeletedItems)";

               stat1.executeUpdate(QS6);
               stat1.executeUpdate(QS7);
               stat1.executeUpdate(QS8);
               stat1.executeUpdate(QS9);

               String QS10 = " Delete from DyesRate Where Item_Code in (Select Item_Code from inventory.DeletedItems@amarml)";
               String QS11 = " Delete from InvItems Where Item_Code in (Select Item_Code from inventory.DeletedItems@amarml)";

               stat2.executeUpdate(QS10);
               stat2.executeUpdate(QS11);

               stat1.close();
               stat2.close();
          }
          catch(Exception ex)
          {
               bComflag = false;
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+ex);
          }

     }

     private void removeFrame()
     {
          layer.remove(this);
          layer.repaint();
          layer.updateUI();
     }
     private void updateLookAndFeel()
     {
          try
          {
               UIManager.setLookAndFeel(windows);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                    

     }

     private String getItemMasterQS()
     {
          String QS = " select item_code from ( "+
                      " select item_code,sum(opgqty) ,sum(opgval),sum(recqty),sum(recval),sum(issqty),sum(issval) from "+
                      " (select item_code,sum(opgqty) as opgqty ,sum(opgval) as opgval,sum(recqty) as recqty,sum(recval) as recval,sum(issqty) as issqty,sum(issval) as issval from invitems group by item_code "+
                      " union all "+
                      " select item_code,sum(opgqty) as opgqty ,sum(opgval) as opgval,sum(recqty) as recqty,sum(recval) as recval,sum(issqty) as issqty,sum(issval) as issval from dyeinginvitems group by item_code "+
                      " union all "+
                      " select item_code,sum(opgqty) as opgqty ,sum(opgval) as opgval,sum(recqty) as recqty,sum(recval) as recval,sum(issqty) as issqty,sum(issval) as issval from melangeinvitems group by item_code "+
                      " union all "+
                      " select item_code,sum(opgqty) as opgqty ,sum(opgval) as opgval,sum(recqty) as recqty,sum(recval) as recval,sum(issqty) as issqty,sum(issval) as issval from kamadhenuinvitems group by item_code "+
                      " union all "+
                      " select item_code,sum(qty) as opgqty ,sum(net) as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from pyorder group by item_code "+
                      " union all "+
                      " select code as item_code,sum(grnqty) as opgqty ,sum(grnvalue) as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from pygrn group by code "+
                      " union all "+
                      " select code as item_code,sum(qty) as opgqty ,sum(issuevalue) as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from pyissue group by code "+
                      " union all "+
                      " select item_code,sum(qty) as opgqty ,0 as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from mrs group by item_code "+
                      " union all "+
                      " select item_code,sum(qty) as opgqty ,0 as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from enquiry group by item_code "+
                      " union all "+
                      " select code as item_code,1 as opgqty ,0 as opgval,0 as recqty,0 as recval,0 as issqty,0 as issval from advrequest group by code "+
                      " ) group by item_code having sum(opgqty)=0 and sum(opgval)=0 and sum(recqty)=0 and sum(recval)=0 and sum(issqty)=0 and sum(issval)=0) "+
                      " order by item_code ";

          return QS;
     }

     
}



