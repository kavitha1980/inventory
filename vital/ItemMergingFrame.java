package vital;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import jdbc.*;
import util.*;
import guiutil.*;

import java.net.*;

public class ItemMergingFrame extends JInternalFrame
{
     JPanel         TopPanel,TopLeft,TopRight,BottomPanel;
     
     JButton        BMaterial1,BMaterial2,BApply;
     JTextField     TMatCode1,TMatCode2;
     JLabel         LCatl1,LDraw1,LCatl2,LDraw2;
     
     JLayeredPane   DeskTop;

     Vector VMTable,VMField;
     Vector VDTable,VDField;
     Vector VOITable;
     Vector VMillCode;
     
     Common         common   = new Common();

     Connection theConnection  = null;
     Connection theDConnection = null;

     boolean bComflag = true;

     JProgressBar   progress;
     
     public ItemMergingFrame(JLayeredPane DeskTop)
     {
          this.DeskTop    = DeskTop;
          
          ORAConnection  oraConnection  = ORAConnection.getORAConnection();
          theConnection                 = oraConnection.getConnection();

          DORAConnection jdbc           = DORAConnection.getORAConnection();
          theDConnection                = jdbc.getConnection();

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          TopPanel       = new JPanel(true);
          TopLeft        = new JPanel(true);
          TopRight       = new JPanel(true);
          BottomPanel    = new JPanel(true);
          
          BMaterial1     = new JButton("Select Original Material");
          BMaterial2     = new JButton("Select Duplicate Material");
          BApply         = new JButton("Merge Items");
          TMatCode1      = new JTextField();
          TMatCode2      = new JTextField();
          LCatl1         = new JLabel("");
          LDraw1         = new JLabel("");
          LCatl2         = new JLabel("");
          LDraw2         = new JLabel("");
          
          TMatCode1      . setEditable(false);
          TMatCode2      . setEditable(false);

          progress                 = new JProgressBar();
          progress                 . setMinimum(0);

     }
     
     public void setLayouts()
     {
          TopPanel       . setLayout(new GridLayout(1,2));
          BottomPanel    . setLayout(new FlowLayout());
          TopLeft        . setLayout(new GridLayout(4,1));
          TopRight       . setLayout(new GridLayout(4,1));
          
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setTitle("Merging of Two Items");
          setBounds(0,0,650,300);
     }
     
     public void addComponents()
     {
          TopLeft             . add(BMaterial1);
          TopLeft             . add(TMatCode1);
          TopLeft             . add(LCatl1);
          TopLeft             . add(LDraw1);

          TopRight            . add(BMaterial2);
          TopRight            . add(TMatCode2);
          TopRight            . add(LCatl2);
          TopRight            . add(LDraw2);

          BottomPanel         . add(progress);
          BottomPanel         . add(BApply);
          
          TopPanel            . add(TopLeft);
          TopPanel            . add(TopRight);
          
          TopLeft             . setBorder(new TitledBorder("Original Material"));
          TopRight            . setBorder(new TitledBorder("Duplicate Material"));
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("South",BottomPanel);
     }
     
     public void addListeners()
     {
          BMaterial1          . addActionListener(new ActList());
          BMaterial2          . addActionListener(new ActList());
          BApply              . addActionListener(new ApplyList(this));
     }
     
     public class ActList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
             if(ae.getSource()==BMaterial1)
             {
                  ItemSearchList3 itemsearchlist = new ItemSearchList3(TMatCode1,BMaterial1,0,"InvItems",LCatl1,LDraw1);
             }
             if(ae.getSource()==BMaterial2)
             {
                  ItemSearchList3 itemsearchlist = new ItemSearchList3(TMatCode2,BMaterial2,0,"InvItems",LCatl2,LDraw2);
             }
         }
     }

     private class ApplyList implements ActionListener
     {
         ItemMergingFrame itemMergingFrame;

         public ApplyList(ItemMergingFrame itemMergingFrame)
         {
              this.itemMergingFrame = itemMergingFrame;
         }
         public void actionPerformed(ActionEvent ae)
         {
              if(validData())
              {
                   if(JOptionPane.showConfirmDialog(null,"Are You Sure to Merge the Two Items?","Information",JOptionPane.YES_NO_OPTION) == 0)
                   {
                        try
                        {
                             new MergeAuth(DeskTop,itemMergingFrame);
                        }
                        catch(Exception ex){}
                   }
              }
         }
     }

     public void startMerging()
     {
          BApply         . setEnabled(false);
          (new Posting()). start();
     }

     private class Posting extends Thread
     {
          Posting()
          {
               setPriority(Thread.MAX_PRIORITY);
          }
          public void run()
          {
               getItemData();
               getDyeingItemData();
               progress.setStringPainted(true);
               progress.setIndeterminate(true);

               updateMillData();
               progress.hide();
               getACommit();
 
               removeFrame();
          }
     }

     private void getACommit()
     {
          try
          {
               if(bComflag)
               {
                    theConnection  . commit();
                    theDConnection . commit();

                    JOptionPane.showMessageDialog(null," Merging Completed ");
                    System         . out.println("Commit");
               }
               else
               {
                    theConnection  . rollback();
                    theDConnection . rollback();

                    System         . out.println("RollBack");
               }
               theConnection    . setAutoCommit(true);
               theDConnection   . setAutoCommit(true);

               Statement stat1 = theConnection.createStatement();
               stat1.executeQuery("Alter Table MatDesc Enable Constraint FK_MATDESC_PO");
               stat1.close();

          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void removeFrame()
     {
          DeskTop.remove(this);
          DeskTop.repaint();
          DeskTop.updateUI();
     }

     public boolean validData()
     {
          String SCode1 = (TMatCode1.getText()).trim();
          String SCode2 = (TMatCode2.getText()).trim();

          if(SCode1.length()==0)
          {
               JOptionPane.showMessageDialog(null,"Select Original Item","Information",JOptionPane.INFORMATION_MESSAGE);
               BMaterial1.requestFocus();
               return false;
          }

          if(SCode2.length()==0)
          {
               JOptionPane.showMessageDialog(null,"Select Duplicate Item","Information",JOptionPane.INFORMATION_MESSAGE);
               BMaterial2.requestFocus();
               return false;
          }

          if(SCode1.equals(SCode2))
          {
               JOptionPane.showMessageDialog(null,"Select Materials With Different Code","Information",JOptionPane.INFORMATION_MESSAGE);
               BMaterial2.requestFocus();
               return false;
          }

          return true;
     }

     private void getItemData()
     {
          VMTable     = new Vector();
          VMField     = new Vector();

          VOITable    = new Vector();

          VMillCode   = new Vector();

          String QS1 = " Select TblName,FldName from ItemCheckTables Where MergeStatus=0 order by 1";
          String QS2 = " Select Item_Table from Mill where MillCode>0 ";
          String QS3 = " Select MillCode from Mill Order by MillCode ";

          try
          {
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VMTable.addElement(common.parseNull((String)theResult.getString(1)));
                    VMField.addElement(common.parseNull((String)theResult.getString(2)));
               }
               theResult.close();

               theResult      = theStatement.executeQuery(QS2);
               while(theResult.next())
               {
                    VOITable.addElement(common.parseNull((String)theResult.getString(1)));
               }
               theResult.close();

               theResult      = theStatement.executeQuery(QS3);
               while(theResult.next())
               {
                    VMillCode.addElement(common.parseNull((String)theResult.getString(1)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void getDyeingItemData()
     {
          VDTable     = new Vector();
          VDField     = new Vector();

          String QS1 = " Select TblName,FldName from ItemCheckTables Where MergeStatus=0 order by 1";

          try
          {
               Statement theStatement   = theDConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VDTable.addElement(common.parseNull((String)theResult.getString(1)));
                    VDField.addElement(common.parseNull((String)theResult.getString(2)));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateMillData()
     {
          try
          {
               Statement stat1 = theConnection.createStatement();
               Statement stat2 = theDConnection.createStatement();

               stat1.executeQuery("Alter Table MatDesc Disable Constraint FK_MATDESC_PO");


               if(theConnection  . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               if(theDConnection  . getAutoCommit())
                    theDConnection  . setAutoCommit(false);


               String SCode1 = (TMatCode1.getText()).trim();
               String SCode2 = (TMatCode2.getText()).trim();

               for(int i=0;i<VMTable.size();i++)
               {
                    String STableName  = (String)VMTable.elementAt(i);
                    String SColumnName = (String)VMField.elementAt(i);

                    String QS = " Update "+STableName+" Set "+SColumnName+"='"+SCode1+"' Where "+SColumnName+"='"+SCode2+"'";

                    System.out.println(QS);
                    stat1.executeQuery(QS);
               }

               int iCount=0;

               String QS1 = " Select count(*) from InvItems where Item_Code='"+SCode2+"'";

               ResultSet theResult      = stat2.executeQuery(QS1);
               while(theResult.next())
               {
                    iCount = theResult.getInt(1);
               }
               theResult.close();

               if(iCount>0)
               {
                    for(int j=0;j<VDTable.size();j++)
                    {
                         String STableName  = (String)VDTable.elementAt(j);
                         String SColumnName = (String)VDField.elementAt(j);
     
                         String QS = " Update "+STableName+" Set "+SColumnName+"='"+SCode1+"' Where "+SColumnName+"='"+SCode2+"'";
     
                         System.out.println(QS);
                         stat2.executeQuery(QS);
                    }

                    Vector VDITable = new Vector();

                    VDITable.addElement("DYESRATE");
                    VDITable.addElement("INVITEMS");

                    for(int k=0;k<VDITable.size();k++)
                    {
                         int iCount1=0;
                         int iCount2=0;
     
                         String SITable = (String)VDITable.elementAt(k);
     
                         String QS2 = " Select count(*) from "+SITable+" where Item_Code='"+SCode1+"'";
                         String QS3 = " Select count(*) from "+SITable+" where Item_Code='"+SCode2+"'";
          
                         theResult      = stat2.executeQuery(QS2);
                         while(theResult.next())
                         {
                              iCount1 = theResult.getInt(1);
                         }
                         theResult.close();
     
                         theResult      = stat2.executeQuery(QS3);
                         while(theResult.next())
                         {
                              iCount2 = theResult.getInt(1);
                         }
                         theResult.close();
     
                         if(iCount1<=0 && iCount2<=0)
                              continue;
     
                         if(iCount1>0 && iCount2<=0)
                              continue;
     
                         if(iCount1>0 && iCount2>0)
                         {
                              if(SITable.equals("INVITEMS"))
                              {
                                   String QS4 = " Update "+SITable+" Set "+
                                                " Opg_Qty = Opg_Qty + (Select Opg_Qty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " Opg_Val = Opg_Val + (Select Opg_Val from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " MSOpgQty = MSOpgQty + (Select MSOpgQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " MSRecQty = MSRecQty + (Select MSRecQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " MSIssQty = MSIssQty + (Select MSIssQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " MSStock = MSStock + (Select MSStock from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " CSStock = CSStock + (Select CSStock from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " ResQty = ResQty + (Select ResQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " IndentQty = IndentQty + (Select IndentQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                                " CSValue = CSValue + (Select CSValue from "+SITable+" Where Item_Code='"+SCode2+"'),"+
									   " ExcessPer = (Select Max(ExcessPer) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                                                " MinStock = (Select Max(MinStock) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                                                " MinOrder = (Select Max(MinOrder) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                                                " CSMinQty = (Select Max(CSMinQty) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                                                " CSIndQty = (Select Max(CSIndQty) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"'))"+
                                                " Where Item_Code='"+SCode1+"'";
          
                                   System.out.println(QS4);
                                   stat2.executeQuery(QS4);
                              }
                              String QS5 = " Delete from "+SITable+" Where Item_Code='"+SCode2+"'";
     
                              System.out.println(QS5);
                              stat2.executeQuery(QS5);
                         }
     
                         if(iCount1<=0 && iCount2>0)
                         {
                              String QS6 = " Update "+SITable+" Set Item_Code='"+SCode1+"' Where Item_Code='"+SCode2+"'";
     
                              System.out.println(QS6);
                              stat2.executeQuery(QS6);
                         }
                    }
               }

               for(int k=0;k<VOITable.size();k++)
               {
                    int iCount1=0;
                    int iCount2=0;

                    String SITable = (String)VOITable.elementAt(k);

                    String QS2 = " Select count(*) from "+SITable+" where Item_Code='"+SCode1+"'";
                    String QS3 = " Select count(*) from "+SITable+" where Item_Code='"+SCode2+"'";
     
                    theResult      = stat1.executeQuery(QS2);
                    while(theResult.next())
                    {
                         iCount1 = theResult.getInt(1);
                    }
                    theResult.close();

                    theResult      = stat1.executeQuery(QS3);
                    while(theResult.next())
                    {
                         iCount2 = theResult.getInt(1);
                    }
                    theResult.close();

                    if(iCount1<=0 && iCount2<=0)
                         continue;

                    if(iCount1>0 && iCount2<=0)
                         continue;

                    if(iCount1>0 && iCount2>0)
                    {
                         String QS4 = " Update "+SITable+" Set "+
                                      " OpgQty = OpgQty + (Select OpgQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " OpgVal = OpgVal + (Select OpgVal from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " RecQty = RecQty + (Select RecQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " RecVal = RecVal + (Select RecVal from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " IssQty = IssQty + (Select IssQty from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " IssVal = IssVal + (Select IssVal from "+SITable+" Where Item_Code='"+SCode2+"'),"+
                                      " Regular = (Select Max(Regular) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                                      " IsoRating = (Select Max(IsoRating) from "+SITable+" Where Item_Code in ('"+SCode1+"','"+SCode2+"'))"+
                                      " Where Item_Code='"+SCode1+"'";

                         String QS5 = " Delete from "+SITable+" Where Item_Code='"+SCode2+"'";

                         System.out.println(QS4);
                         System.out.println(QS5);
                         stat1.executeQuery(QS4);
                         stat1.executeQuery(QS5);
                    }

                    if(iCount1<=0 && iCount2>0)
                    {
                         String QS6 = " Update "+SITable+" Set Item_Code='"+SCode1+"' Where Item_Code='"+SCode2+"'";

                         System.out.println(QS6);
                         stat1.executeQuery(QS6);
                    }
               }

               for(int k=0;k<VMillCode.size();k++)
               {
                    String SMillCode = (String)VMillCode.elementAt(k);

                    int iMillCount1=0;
                    int iMillCount2=0;

                    String QS7 = " Select count(*) from ItemStock where ItemCode='"+SCode1+"' and MillCode="+SMillCode;
                    String QS8 = " Select count(*) from ItemStock where ItemCode='"+SCode2+"' and MillCode="+SMillCode;
     
                    theResult      = stat1.executeQuery(QS7);
                    while(theResult.next())
                    {
                         iMillCount1 = theResult.getInt(1);
                    }
                    theResult.close();

                    theResult      = stat1.executeQuery(QS8);
                    while(theResult.next())
                    {
                         iMillCount2 = theResult.getInt(1);
                    }
                    theResult.close();

                    if(iMillCount1<=0 && iMillCount2<=0)
                         continue;

                    if(iMillCount1>0 && iMillCount2<=0)
                         continue;


                    if(iMillCount1>0 && iMillCount2>0)
                    {
                         Vector VHodCode = new Vector();

                         String QS9 = " Select Distinct(HodCode) from ItemStock where ItemCode='"+SCode1+"' and MillCode="+SMillCode;

                         theResult      = stat1.executeQuery(QS9);
                         while(theResult.next())
                         {
                              VHodCode.addElement(common.parseNull(theResult.getString(1)));
                         }
                         theResult.close();

                         String QS10 = " Select Distinct(HodCode) from ItemStock where ItemCode='"+SCode2+"' and MillCode="+SMillCode;

                         theResult      = stat1.executeQuery(QS10);
                         while(theResult.next())
                         {
                              String SHodCode = common.parseNull(theResult.getString(1));

                              int iHIndex = common.indexOf(VHodCode,SHodCode);

                              if(iHIndex>=0)
                              {
                                   String QS11 = " Update ItemStock Set "+
                                                 " Stock = Stock + (Select Stock from ItemStock Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode2+"'),"+
                                                 " IndentQty = IndentQty + (Select IndentQty from ItemStock Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode2+"'),"+
                                                 " TransferQty = TransferQty + (Select TransferQty from ItemStock Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode2+"')"+
                                                 " Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode1+"'";

                                   String QS12 = " Delete from ItemStock Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode2+"'";

                                   System.out.println(QS11);
                                   System.out.println(QS12);
                                   stat1.executeQuery(QS11);
                                   stat1.executeQuery(QS12);
                              }
                              else
                              {
                                   String QS13 = " Update ItemStock Set ItemCode='"+SCode1+"' Where MillCode="+SMillCode+" and HodCode="+SHodCode+" and ItemCode='"+SCode2+"'";

                                   System.out.println(QS13);
                                   stat1.executeQuery(QS13);
                              }
                         }
                         theResult.close();
                    }

                    if(iMillCount1<=0 && iMillCount2>0)
                    {
                         String QS14 = " Update ItemStock Set ItemCode='"+SCode1+"' Where MillCode="+SMillCode+" and ItemCode='"+SCode2+"'";

                         System.out.println(QS14);
                         stat1.executeQuery(QS14);
                    }
               }

               String QS15 = " Update InvItems Set "+
                             " OpgQty = OpgQty + (Select OpgQty from InvItems Where Item_Code='"+SCode2+"'),"+
                             " OpgVal = OpgVal + (Select OpgVal from InvItems Where Item_Code='"+SCode2+"'),"+
                             " RecQty = RecQty + (Select RecQty from InvItems Where Item_Code='"+SCode2+"'),"+
                             " RecVal = RecVal + (Select RecVal from InvItems Where Item_Code='"+SCode2+"'),"+
                             " IssQty = IssQty + (Select IssQty from InvItems Where Item_Code='"+SCode2+"'),"+
                             " IssVal = IssVal + (Select IssVal from InvItems Where Item_Code='"+SCode2+"'),"+
                             " Regular = (Select Max(Regular) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                             " IsoRating = (Select Max(IsoRating) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                             " QtyAllowance = (Select Max(QtyAllowance) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                             " TPer = (Select Max(TPer) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                             " CenPer = (Select Max(CenPer) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"')),"+
                             " SurchargePer = (Select Max(SurchargePer) from InvItems Where Item_Code in ('"+SCode1+"','"+SCode2+"'))"+
                             " Where Item_Code='"+SCode1+"'";

               String QS16 = " Delete from InvItems Where Item_Code='"+SCode2+"'";

               System.out.println(QS15);
               System.out.println(QS16);
               stat1.executeQuery(QS15);
               stat1.executeQuery(QS16);

               Vector VIMillCode  = new Vector();
               Vector VIItemTable = new Vector();
               Vector VISupTable  = new Vector();


               String QS17 = " Select Distinct(ItemStock.MillCode),Mill.Item_Table,Mill.Sup_Table "+
                             " From ItemStock Inner Join Mill on ItemStock.MillCode=Mill.MillCode "+
                             " and ItemStock.ItemCode='"+SCode1+"' Order by ItemStock.MillCode ";

               theResult      = stat1.executeQuery(QS17);
               while(theResult.next())
               {
                    VIMillCode.addElement(common.parseNull(theResult.getString(1)));
                    VIItemTable.addElement(common.parseNull(theResult.getString(2)));
                    VISupTable.addElement(common.parseNull(theResult.getString(3)));
               }
               theResult.close();

               for(int k=0;k<VIMillCode.size();k++)
               {
                    int iMillCode     = common.toInt((String)VIMillCode.elementAt(k));
                    String SItemTable = (String)VIItemTable.elementAt(k);
                    String SSupTable  = (String)VISupTable.elementAt(k);

                    Item1 IC          = new Item1(SCode1,common.getServerPureDate(),common.getServerPureDate(),iMillCode,SItemTable,SSupTable);
          
                    double dAllStock = common.toDouble(IC.getClStock());
                    double dAllValue = common.toDouble(IC.getClValue());
          
                    double dRate   = 0;
                    try
                    {
                         dRate = dAllValue/dAllStock;
                    }
                    catch(Exception ex)
                    {
                         dRate=0;
                    }
                    System.out.println(dAllStock+":"+dAllValue+":"+dRate);
          
                    if(dAllStock==0)
                    {
                         dRate = common.toDouble(IC.SRate);
                         System.out.println("Rate:"+dRate);
                    }

                    String QS18 = " Update ItemStock set StockValue="+common.getRound(dRate,4)+
                                  " Where ItemCode='"+SCode1+"' and MillCode="+iMillCode;

                    System.out.println(QS18);
                    stat1.executeQuery(QS18);
               }

               String QS19 = " Insert into MergingItems values ("+
                             "'"+SCode1+"',"+
                             "'"+SCode2+"',"+
                             "'"+common.getServerDate()+"',"+
                             "'"+InetAddress.getLocalHost().getHostName()+"')";

               stat1.executeQuery(QS19);

               stat1.close();
               stat2.close();
          }
          catch(Exception e)
          {
               bComflag = false;
               e.printStackTrace();
               JOptionPane.showMessageDialog(null,"Updation Error:"+e);
          }
     }


}
