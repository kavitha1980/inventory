package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSItemAuthenticateDialog
{
     // Global Declarations...

     private        JDialog                  theDialog;

     private        MRSItemAuthenticateFrame     theFrame;
     private        int                      iUserCode, iMillCode, iAuthCode, iRow;
     private        String                   SItemTable, SNewItemName;
     
     private        JPanel                   TopPanel, MiddlePanel;
     public         AddressField             TItemName;

     private        JList                    theList;

     private        Common                   common;

     private        Connection               theConnection;
     private        ArrayList                AItemCode, AItemName;
     private        Vector                   VItems;

     // create Construtors..

     public MRSItemAuthenticateDialog(MRSItemAuthenticateFrame theFrame, int iMillCode, int iAuthCode, int iUserCode, String SItemTable, int iRow, String SNewItemName)
     {
          this      . theFrame          = theFrame;
          this      . iMillCode         = iMillCode;
          this      . iAuthCode         = iAuthCode;
          this      . iUserCode         = iUserCode;
          this      . SItemTable        = SItemTable;
          this      . iRow              = iRow;
          this      . SNewItemName      = SNewItemName;
     
          common                        = new Common();
     
          try
          {
               createConnections();

               createComponents();
               setLayouts();
               addComponents();

               setInvItemsInArrayList();

               theDialog                . setVisible(true);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // Create Components, set Layouts and Add Components..

     private void createComponents()
     {
          theDialog                     = new JDialog(new Frame(), "List of Materials for Authenticate", true);
          theDialog                     . setBounds(20, 40, 500, 420);

          TopPanel                      = new JPanel();
          MiddlePanel                   = new JPanel();

          TItemName                     = new AddressField(30);

          theList                       = new JList();
          theList                       . setFont(new Font("monospaced", Font.PLAIN, 11));

          // Add Listeners...

          TItemName                     . addKeyListener(new KeyList());
          theList                       . addKeyListener(new JListKeyList());
     }

     private void setLayouts()
     {
          TopPanel                      . setLayout(new GridLayout(2,3,2,2));
          MiddlePanel                   . setLayout(new BorderLayout());

          TopPanel                      . setBorder(new TitledBorder("Search"));
          MiddlePanel                   . setBorder(new TitledBorder("List of Items"));
     }

     private void addComponents()
     {
          TopPanel                      . add(new JLabel("MRS Item", JLabel.CENTER));
          TopPanel                      . add(new JLabel("<html><b>"+SNewItemName+"</b></html>", JLabel.LEFT));
          TopPanel                      . add(new JLabel("", JLabel.CENTER));

          TopPanel                      . add(new JLabel("Search", JLabel.CENTER));
          TopPanel                      . add(TItemName);
          TopPanel                      . add(new JLabel("", JLabel.CENTER));

          MiddlePanel                   . add(new JScrollPane(theList));

          theDialog . getContentPane()  . add("North", TopPanel);
          theDialog . getContentPane()  . add("Center", MiddlePanel);
     }

     // Listener Classes...

     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setInvItemsInArrayList();
          }
     }

     private class JListKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               if(ke.getKeyCode() == KeyEvent.VK_ENTER)
               {
                    setItemDetailsInFrameTable();
               }
          }
     }

     private void setItemDetailsInFrameTable()
     {
          if(theList.getSelectedIndex() == -1)
          {
               JOptionPane.showMessageDialog(null, "No Items Selected..", "Information", JOptionPane.ERROR_MESSAGE);
               return;
          }

          try
          {
               theDialog                . setVisible(false);

               int iIndex               = theList.getSelectedIndex();

               String SSelectedValue    = (String)AItemName.get(iIndex);
               String SItemCode         = (String)AItemCode.get(iIndex);

               theFrame                 . theTable     . setValueAt(SItemCode, iRow, 4);
               theFrame                 . theTable     . setValueAt(SSelectedValue, iRow, 5);
               theFrame                 . theTable     . setValueAt(new Boolean(true), iRow, 7);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return;
          }
     }

     // Connecting With DB....

     private void createConnections()
     {
          theConnection                 = null;

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setInvItemsInArrayList()
     {
          AItemCode                     = null;
          AItemName                     = null;

          AItemCode                     = new ArrayList();
          AItemName                     = new ArrayList();

          AItemCode                     . clear();
          AItemName                     . clear();

          VItems                        = null;
          VItems                        = new Vector();
          VItems                        . removeAllElements();

          String SItemName              = common.parseNull(TItemName.getText()).trim();

          if(SItemName.length() == 0)
          {
               theList                  . setListData(VItems);
          }
          else
          {
               try
               {
                    StringBuffer SB     = null;
                    SB                  = new StringBuffer();
     
                    SB.append(" Select Item_Code, RPad(Item_Name, 50) from InvItems ");
                    SB.append(" where Item_Name like '%"+SItemName+"%' ");
                    SB.append(" Order by 2 ");
     
                    PreparedStatement thePre = theConnection.prepareCall(SB.toString());
                    ResultSet rs        = thePre.executeQuery();
     
                    while(rs.next())
                    {
                         AItemCode      . add(common.parseNull(rs.getString(1)));
                         AItemName      . add(common.parseNull(rs.getString(2)).trim());

                         String SItemC  = rs.getString(1);
                         String SItemN  = rs.getString(2)+"   "+SItemC;

                         VItems         . addElement(SItemN);
                    }
     
                    rs                  . close();
                    thePre              . close();
     
                    // set Null..
     
                    SB                  = null;
     
                    theList             . setListData(VItems);
     
                    if(theList.getModel().getSize() > 0)
                    {
                         theList        . setSelectedIndex(0);
                    }
               }
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }
}
