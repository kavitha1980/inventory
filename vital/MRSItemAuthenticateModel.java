package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSItemAuthenticateModel extends DefaultTableModel
{
     String ColumnName[] = {"Sl.No.","UserName", "New Item Code", "New Item Name","Matching Item_Code","Matching Item_Name",  "Reason For Rejection", "Authenticate","Reject"};
     String ColumnType[] = {"S"     ,"S"       , "S"            , "E"            ,"S"                  ,"S"                ,"E"                 , "E"   ,"E" };
     int  iColumnWidth[] = {20      ,100       , 40             , 150            ,40                   ,150                , 220                 , 30    ,30 };

     public MRSItemAuthenticateModel()
     {
          setDataVector(getRowData(), ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
