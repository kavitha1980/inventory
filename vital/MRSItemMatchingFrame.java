package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MRSItemMatchingFrame extends JInternalFrame
{
     // Global Declarations...

     protected      JLayeredPane             Layer;
     private        int                      iUserCode, iMillCode, iAuthCode;
     private        String                   SItemTable;
     
     private        JPanel                   TopPanel, MiddlePanel, BottomPanel;
     private        JButton                  BRefresh, BMaterial, BSave, BExit,BDyesData;

     private        MRSItemMatchingModel     theModel;
     public         JTable                   theTable;

     private        ArrayList                theMRSItemList;
     private        Common                   common;

     private        Connection               theConnection;

     private        MaterialFrame            materialFrame;
	 
	 private        DyesSelectionDataFrame   dyesSelectionDataFrame;

     // create Construtors..

     public MRSItemMatchingFrame(JLayeredPane Layer, int iMillCode, int iAuthCode, int iUserCode, String SItemTable)
     {
          this      . Layer             = Layer;
          this      . iMillCode         = iMillCode;
          this      . iAuthCode         = iAuthCode;
          this      . iUserCode         = iUserCode;
          this      . SItemTable        = SItemTable;
     
          common                        = new Common();
     
          try
          {
               createConnections();

               createComponents();
               setLayouts();
               addComponents();

               setTableData();

               ExitFrame(this);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
          
     // Create Components, set Layouts and Add Components..

     private void createComponents()
     {
          TopPanel                 = new JPanel();
          MiddlePanel              = new JPanel();
          BottomPanel              = new JPanel();

          BRefresh                 = new JButton("Refresh");
          BMaterial                = new JButton("Add Material");
          BSave                    = new JButton("Match Items");
		  BDyesData                = new JButton("Dyes Data");
          BExit                    = new JButton("Exit");

          theModel                 = new MRSItemMatchingModel();
          theTable                 = new JTable(theModel);

          TableColumnModel TC      = theTable.getColumnModel();

          for(int i=0;i<theModel.ColumnName.length;i++)
          {
               TC                  . getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }

          // AddListeners..

          BRefresh                 . addActionListener(new ActionList());
          BMaterial                . addActionListener(new ActionList());
          BSave                    . addActionListener(new ActionList());
		  BDyesData                . addActionListener(new ActionList());
          BExit                    . addActionListener(new ActionList());

          theTable                 . addKeyListener(new KeyList(this));
     }
     
     private void setLayouts()
     {
          setTitle("MRS Item Matching List");
          setBounds(10, 20, 618, 470);
          setResizable(true);
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
                                             
          TopPanel                 . setLayout(new FlowLayout(FlowLayout.LEFT));
          MiddlePanel              . setLayout(new BorderLayout());
          BottomPanel              . setLayout(new FlowLayout());

          TopPanel                 . setBorder(new TitledBorder("Option"));
          MiddlePanel              . setBorder(new TitledBorder("Pending MRS Items for Matching"));
          BottomPanel              . setBorder(new TitledBorder("Controls"));
     }
     
     private void addComponents()
     {
          TopPanel                 . add(BRefresh);
          TopPanel                 . add(new JLabel("<html><body><font color='Green'><b>F3 - To Open Inventory Items</b></font></body></html>"));

          MiddlePanel              . add(new JScrollPane(theTable));

          BottomPanel              . add(BSave);
          BottomPanel              . add(BExit);
          BottomPanel              . add(new JLabel("           "));
          BottomPanel              . add(BMaterial);
		  if(iMillCode==1)
		  BottomPanel              . add(BDyesData);

          getContentPane()         . add("North" , TopPanel);
          getContentPane()         . add("Center", MiddlePanel);
          getContentPane()         . add("South" , BottomPanel);
     }

     // Listener Classes
     
     private class ActionList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BRefresh)
               {
                    setTableData();
               }

               if(ae.getSource() == BSave)
               {
                    if(isSelectedAnyOne() && isItemCodeEmpty())
                    {
                         if(JOptionPane.showConfirmDialog(null, "Confirm Matching the Item(s)?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
                         {
                              SaveDetails();
                         }
                    }
               }
               
               if(ae.getSource() == BExit)
               {
                    removeHelpFrame();
               }

               if(ae.getSource() == BMaterial)
               {
                    openMaterialFrame();
               }
			    if(ae.getSource() == BDyesData)
               {
                    openDyeData();
               }
          }
     }

     private class KeyList extends KeyAdapter
     {
          MRSItemMatchingFrame theFrame;

          public KeyList(MRSItemMatchingFrame theFrame)
          {
               this.theFrame  = theFrame;
          }

          public void keyReleased(KeyEvent ke)
          {
               if(ke.getKeyCode() == KeyEvent.VK_F3)
               {
                    int iRow                           = theTable.getSelectedRow();
                    String SNewItemName                = common.parseNull((String)theModel.getValueAt(iRow, 2));

                    MRSItemMatchingDialog theDialog    = new MRSItemMatchingDialog(theFrame, iMillCode, iAuthCode, iUserCode, SItemTable, iRow, SNewItemName);
                    theDialog                          . TItemName.requestFocus();
               }
          }
     }

     // General Methods And Validations..
     
     private void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . updateUI();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void openMaterialFrame()
     {
          System              . runFinalization();
          System              . gc();

          try
          {
               Layer          . remove(materialFrame);
               Layer          . updateUI();
          }
          catch(Exception ex){}

          try
          {
               materialFrame  = new MaterialFrame(Layer, iMillCode, iAuthCode, iUserCode, SItemTable);
               Layer          . add(materialFrame);
               Layer          . repaint();
               materialFrame  . setSelected(true);
               Layer          . updateUI();
               materialFrame  . show();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          System              . runFinalization();
          System              . gc();
     }

     // set Table Data...

     private void setTableData()
     {
          theModel                      . setNumRows(0);

          setMRSItemDetails();
          
          for(int i=0; i<theMRSItemList.size(); i++)
          {
               HashMap theMap           = (HashMap)theMRSItemList.get(i);

               ArrayList theList        = null;
               theList                  = new ArrayList();
               theList                  . clear();

               theList                  . add(String.valueOf(i+1));
               theList                  . add(common.parseNull((String)theMap.get("ITEM_CODE")));
               theList                  . add(common.parseNull((String)theMap.get("ITEM_NAME")));
               theList                  . add("");
               theList                  . add("");
               theList                  . add(new Boolean(false));
               
               theModel                 . appendRow(new Vector(theList));
          }
     }

     // General Methods..

     private boolean isSelectedAnyOne()
     {
          for(int i=0; i<theModel.getRowCount(); i++)
          {
               Boolean bValue           = (Boolean)theModel.getValueAt(i, 5);

               if(bValue.booleanValue())
               {
                    return true;
               }
          }

          JOptionPane.showMessageDialog(null, "No Data Selected", "Information", JOptionPane.ERROR_MESSAGE);
          return false;
     }

     private boolean isItemCodeEmpty()
     {
          for(int i=0; i<theModel.getRowCount(); i++)
          {
               Boolean bValue           = (Boolean)theModel.getValueAt(i, 5);
               String SItemCode         = common.parseNull((String)theModel.getValueAt(i, 3));

               if(bValue.booleanValue() && SItemCode.length() == 0)
               {
                    JOptionPane.showMessageDialog(null, "You Must Select Any One Matched Item in Row No. "+(i+1), "Information", JOptionPane.ERROR_MESSAGE);
                    setTableSelection(i);
                    return false;
               }
          }

          return true;
     }

     private void setTableSelection(int i)
     {
          try
          {
               theTable            . setRowSelectionInterval(i, i);
               Rectangle theRect   = theTable.getCellRect(i, 0, true);
               theTable            . scrollRectToVisible(theRect);
          }catch(Exception ex){}
     }

     // New Methods...

     private void ExitFrame(final MRSItemMatchingFrame theFrame)
     {
          KeyStroke escapeKeyStroke     = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);

          Action escapeAction           = new AbstractAction()
          {
              // close the frame when the user presses escape

              public void actionPerformed(ActionEvent e)
              {
                  theFrame              . dispose();
              }
          };

          theFrame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "ESCAPE");
          theFrame.getRootPane().getActionMap().put("ESCAPE", escapeAction);
     }

     // Connecting With DB....

     private void createConnections()
     {
          theConnection                 = null;

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setMRSItemDetails()
     {
          theMRSItemList                = null;
          theMRSItemList                = new ArrayList();
          theMRSItemList                . clear();

          try
          {
               PreparedStatement thePre = theConnection.prepareCall(getQS());
               ResultSet rs             = thePre.executeQuery();
               ResultSetMetaData rsmd   = rs.getMetaData();

               while(rs.next())
               {
                    HashMap theMap      = null;
                    theMap              = new HashMap();

                    for(int i=0; i<rsmd.getColumnCount(); i++)
                    {
                         theMap         . put(rsmd.getColumnName(i+1), common.parseNull(rs.getString(i+1)));
                    }

                    theMRSItemList      . add(theMap);
               }

               rs                       . close();
               thePre                   . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getQS()
     {
          StringBuffer SB               = null;
          SB                            = new StringBuffer();

          SB.append(" Select Item_Code,ModifiedItem_Name as Item_Name,SoEntryItemName from MRSItems ");
          SB.append(" where Matching_Item_Code is null  and SoAuthenticationStatus=1 ");
          SB.append(" and MillCode="+iMillCode);
          SB.append(" Order by 2 ");

          return SB.toString();
     }

     // Save Details...

     private void SaveDetails()
     {
          try
          {
               theConnection                      . setAutoCommit(false);

               for(int i=0; i<theModel.getRowCount(); i++)
               {
                    Boolean bValue                = (Boolean)theModel.getValueAt(i, 5);
     
                    if(bValue.booleanValue())
                    {
                         String SMRSItemCode      = common.parseNull((String)theModel.getValueAt(i, 1));
                         String SInvItemCode      = common.parseNull((String)theModel.getValueAt(i, 3));

                         StringBuffer SB          = null;
                         SB                       = new StringBuffer();

                         SB.append(" Update MRSItems set Matching_Item_Code = '"+SInvItemCode+"' ");
                         SB.append(" where Item_Code = "+SMRSItemCode+" ");

                         PreparedStatement thePre = theConnection.prepareCall(SB.toString());

                         thePre                   . executeQuery();
                         thePre                   . close();

                         // set Null..

                         SB                       = null;
                         SMRSItemCode             = null;
                         SInvItemCode             = null;
                    }
               }

               theConnection                      . commit();
               theConnection                      . setAutoCommit(true);

               JOptionPane.showMessageDialog(null, "Item(s) Matched Sucessfully..", "Information", JOptionPane.INFORMATION_MESSAGE);

               setTableData();
          }
          catch(Exception ex)
          {
               try
               {
                    theConnection                 . rollback();
                    theConnection                 . setAutoCommit(true);
               }catch(Exception e){}

               ex.printStackTrace();

               JOptionPane.showMessageDialog(null, "Item(s) Not Matched.. Try Again.."+ex.getMessage().toString(), "Error", JOptionPane.ERROR_MESSAGE);
          }
     }
	   private void openDyeData()
	 {
		try
		{
			int iRow                           = theTable.getSelectedRow();
			int iItemCode                   = common.toInt((String)theModel.getValueAt(iRow, 1));
			String SItemName                = common.parseNull((String)theModel.getValueAt(iRow, 2));	
			
			try
			{
				Layer          . remove(materialFrame);
				Layer          . updateUI();
			}
			catch(Exception ex){}

			try
			{
				dyesSelectionDataFrame = new DyesSelectionDataFrame(Layer,iItemCode,SItemName);
						
				Layer          . add(dyesSelectionDataFrame);
				Layer          . repaint();
				dyesSelectionDataFrame  . setSelected(true);
				Layer          . updateUI();
				dyesSelectionDataFrame  . show();
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
				
		}
	 

	 }
}
