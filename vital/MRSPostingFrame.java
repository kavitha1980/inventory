package vital;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MRSPostingFrame extends JInternalFrame 
{
     JLayeredPane DeskTop;
     int          iMillCode;
     String       SSupTable;

     JPanel TopPanel,MiddlePanel,BottomPanel;
     Common common   = new Common();

     AGNModel theModel;
     JTable   theTable;

     Object RowData[][];
     String ColumnData[] = {"Block","Order No","Date","Code","Name","MRS No","Supplier"};
     String ColumnType[] = {"S","N","S","N","S","N","S"};

     Vector    VNo,VBlock,VDate,VCode,VItem,VSup,VId,VMrsNo,VMrsSlNo,VMrsUserCode;
     Vector    VSeleId;

     Vector    VPMrsNo,VPMrsSlNo,VPMrsUserCode,VPMrsId;

     DateField TStDate,TEnDate;
     JButton   BApply,BPost;
     String    SStDate,SEnDate;

     JLabel LIns = new JLabel("Press INSERT in Mrs Column ");

     Connection theConnection = null;

     public MRSPostingFrame(JLayeredPane DeskTop,int iMillCode,String SSupTable)
     {
          this.DeskTop   = DeskTop;
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          BottomPanel = new JPanel(true);

          TStDate     = new DateField();
          TEnDate     = new DateField();

          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          BApply      = new JButton("Apply");
          BPost       = new JButton("Post MRS");

          BPost.setEnabled(false);
     }

     private void setLayouts()
     {
          setTitle("Mrs Posting Frame ");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,500);

          LIns.setFont(new Font("DEFAULT",1,12));
          LIns.setForeground(Color.BLUE);

          MiddlePanel.setLayout(new BorderLayout());
     }

     private void addComponents()
     {
          TopPanel.add(new JLabel("From : "));
          TopPanel.add(TStDate);
          TopPanel.add(new JLabel("To : "));
          TopPanel.add(TEnDate);
          TopPanel.add(BApply);

          BottomPanel.add(LIns);
          BottomPanel.add(BPost);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     private void addListeners()
     {
          BApply.addActionListener(new AppList());
          BPost.addActionListener(new PostList());
     }

      private class PostList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               BPost.setEnabled(false);
               setDataIntoMRSTable();
               JOptionPane.showMessageDialog(null,"Posting Over","Information",JOptionPane.INFORMATION_MESSAGE);
               removeHelpFrame();
          }
      }

      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               SStDate = TStDate.toNormal();
               SEnDate = TEnDate.toNormal();

               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               setTable();
               BPost.setEnabled(true);
          }
      }

     private void fetchData()
     {
          VNo          = new Vector();
          VBlock       = new Vector();
          VDate        = new Vector();
          VCode        = new Vector();
          VItem        = new Vector();
          VSup         = new Vector();
          VId          = new Vector();
          VMrsNo       = new Vector();
          VMrsSlNo     = new Vector();
          VMrsUserCode = new Vector();
          VSeleId      = new Vector();

          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
                ResultSet theResult      = theStatement.executeQuery(getQS());
                while(theResult.next())
                {
                    VNo         .addElement(theResult.getString(1));
                    VBlock      .addElement(getBlock(theResult.getInt(2)));
                    VDate       .addElement(common.parseDate(theResult.getString(3)));
                    VCode       .addElement(theResult.getString(4));
                    VItem       .addElement(theResult.getString(5));
                    VSup        .addElement(theResult.getString(6));
                    VId         .addElement(theResult.getString(7));
                    VMrsNo      .addElement("0");
                    VMrsSlNo    .addElement("0");
                    VMrsUserCode.addElement("0");
                }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getBlock(int iBlock)
     {
          String SBlock = " ";

          if(iBlock==0)
               SBlock = "SST";
          else if(iBlock==1)
               SBlock = "SSP";
          else if(iBlock==2)
               SBlock = "NST";
          else if(iBlock==3)
               SBlock = "NSP";
          else if(iBlock==4)
               SBlock = "NPR";
          return SBlock;
     }
     private String getQS()
     {
          String QS = " ";

          QS = " SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderBlock, "+
               " PurchaseOrder.OrderDate, PurchaseOrder.Item_Code, InvItems.ITEM_NAME, "+
               " "+SSupTable+".NAME, PurchaseOrder.ID "+
               " FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code = "+SSupTable+".AC_CODE) "+
               " INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.ITEM_CODE "+
               " WHERE purchaseorder.orderdate>='"+SStDate+"' and "+
               " purchaseorder.orderdate<='"+SEnDate+"' And "+
               " purchaseorder.millcode="+iMillCode+
               " and PurchaseOrder.MrsNo=0 and PurchaseOrder.InvQty=0 "+
               " ORDER BY PurchaseOrder.Item_Code,PurchaseOrder.OrderNo ";

          return QS;
     }

     private void setTable()
     {
          fetchData();
          setRowData();

          if(VNo.size()<=0)
          {
               JOptionPane.showMessageDialog(null,"No Data Available ","Error Message",JOptionPane.INFORMATION_MESSAGE);
               setRowData();
               return;
          }
          RowData = new Object[VNo.size()][ColumnData.length];
          for(int i=0;i<VNo.size();i++)
          {
               RowData[i][0] = (String)VBlock.elementAt(i);
               RowData[i][1] = (String)VNo.elementAt(i);
               RowData[i][2] = (String)VDate.elementAt(i);
               RowData[i][3] = (String)VCode.elementAt(i);            
               RowData[i][4] = (String)VItem.elementAt(i);
               RowData[i][5] = "";
               RowData[i][6] = (String)VSup.elementAt(i);
          }

          theModel  = new AGNModel(RowData,ColumnData,ColumnType);
          theTable  = new JTable(theModel);

          MiddlePanel.setLayout(new BorderLayout());
          MiddlePanel.add("North",theTable.getTableHeader());
          MiddlePanel.add("Center",new JScrollPane(theTable));
          repaint();
          MiddlePanel.updateUI();
          theTable   .addKeyListener(new KeyList(this));
     }
     private void setRowData()
     {
           RowData = new Object[1][ColumnData.length];
           for(int i=0;i<ColumnData.length-1;i++)
                 RowData[0][i]="";
     }

     private class KeyList extends KeyAdapter
     {
          MRSPostingFrame mrspostingframe;

          public KeyList(MRSPostingFrame mrspostingframe)
          {
               this.mrspostingframe = mrspostingframe;
          }

          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_INSERT)
               {
                    int i  = theTable.getSelectedRow();
                    int jj = theTable.getSelectedColumn();

                    if(jj==5)
                    {
                         String SSeleMrsNo = (String)theTable.getModel().getValueAt(i,5);

                         if(common.toInt(SSeleMrsNo)==0)
                         {
                              String SItemCode  = (String)theTable.getModel().getValueAt(i,3);
     
                              setPendingData(SItemCode);
     
                              if(VPMrsNo.size()>0)
                              {
                                   PendingMrsSelectionFrame PF = new PendingMrsSelectionFrame(DeskTop,theTable,iMillCode,VPMrsNo,VPMrsSlNo,VPMrsUserCode,VPMrsId,mrspostingframe);
                                   try
                                   {
                                        DeskTop   . add(PF);
                                        DeskTop   . repaint();
                                        PF        . setSelected(true);
                                        DeskTop   . updateUI();
                                        PF        . show();
                                   }
                                   catch(java.beans.PropertyVetoException ex){}
                              }
                              else
                              {
                                   JOptionPane.showMessageDialog(null,"No Pending Mrs for this Item","Dear User",JOptionPane.INFORMATION_MESSAGE);
                              }
                         }
                         else
                         {
                              JOptionPane.showMessageDialog(null,"This Row already Processed","Dear User",JOptionPane.INFORMATION_MESSAGE);
                         }
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Only in MrsNo Column","Dear User",JOptionPane.INFORMATION_MESSAGE);
                    }
               }
          }
     }

     public void setPendingData(String SItemCode)
     {
          VPMrsNo       = new Vector();
          VPMrsSlNo     = new Vector();
          VPMrsUserCode = new Vector();
          VPMrsId       = new Vector();

          try
          {
               String QS = " Select MrsNo,SlNo,MrsAuthUserCode,Id from Mrs "+
                           " Where OrderNo=0 and Item_Code='"+SItemCode+"'"+
                           " and MillCode="+iMillCode+" Order by MrsNo,SlNo ";

               if(theConnection == null)
               {
                  ORAConnection jdbc   = ORAConnection.getORAConnection();
                  theConnection        = jdbc.getConnection();
               }
               Statement stat = theConnection.createStatement();
               ResultSet res  = stat.executeQuery(QS);
               while(res.next())
               {
                    String SId = res.getString(4);

                    int iIndex = common.indexOf(VSeleId,SId);

                    if(iIndex<0)
                    {
                         VPMrsNo.addElement(res.getString(1));
                         VPMrsSlNo.addElement(res.getString(2));
                         VPMrsUserCode.addElement(res.getString(3));
                         VPMrsId.addElement(SId);
                    }
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setDataIntoMRSTable()
     {
          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
                for(int i=0;i<VNo.size();i++)
                {
                    int iMrsNo = common.toInt((String)theModel.getValueAt(i,5));

                    if(iMrsNo<=0)
                         continue;

                    String SOrdNo = (String)VNo.elementAt(i);
                    String SDate  = (String)VDate.elementAt(i);
                    String SBlock = (String)VBlock.elementAt(i);
                    String SCode  = (String)VCode.elementAt(i);

                    String SMrsSlNo     = (String)VMrsSlNo.elementAt(i);
                    String SMrsUserCode = (String)VMrsUserCode.elementAt(i);


                    String QS1 = " UPDATE MRS SET "+
                                 " OrderNo="+SOrdNo+","+
                                 " OrderBlock="+getOrdBlock(SBlock)+
                                 " WHERE MrsNo="+iMrsNo+" AND MillCode="+iMillCode+
                                 " And Item_Code='"+SCode+"'";

                    String QS2 = " UPDATE PurchaseOrder SET "+
                                 " MrsNo="+iMrsNo+","+
                                 " MrsSlNo="+SMrsSlNo+","+
                                 " MrsAuthUserCode="+SMrsUserCode+
                                 " WHERE id="+(String)VId.elementAt(i);


                    theStatement.execute(QS1);
                    theStatement.execute(QS2);
                }
                theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("99 : "+ex);
          }
     }
     private int getOrdBlock(String SBlock)
     {
          int iBlock = 0;

          if((SBlock.trim()).startsWith("SST"))
               iBlock = 0;
          else if((SBlock.trim()).startsWith("SSP"))
               iBlock = 1;
          else if((SBlock.trim()).startsWith("NST"))
               iBlock = 2;
          else if((SBlock.trim()).startsWith("NSP"))
               iBlock = 3;
          else if((SBlock.trim()).startsWith("NPR"))
               iBlock = 4;

          return iBlock;
     }

     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }


}
