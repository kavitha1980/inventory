package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MatCreatePanel extends JPanel
{

     JTextField     TMatCode,TMatName,TMatCatl,TMatDraw,TMatPaperColour,TMatPaperSize;
     JComboBox      JCDept,JCUom,JMatPaperSets,JMatPaperSide,JCMill;
     JButton        BOk,BCancel;
     JPanel         TopPanel,BottomPanel;

     Vector         VDept,VDeptCode,VSeries,VUom,VUomCode;
     Vector         VPaperSet,VSetCode,VSideName,VSideCode;

     Common         common    = new Common();
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable;

     boolean        bStatFlag = false;
     boolean        bAutoCommitFlag = true;

     Connection     theConnection  = null;
     Connection     theDConnection = null;

     public MatCreatePanel(int iMillCode,int iAuthCode,int iUserCode,String SItemTable)
     {
          super(true);
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.iUserCode  = iUserCode;
          this.SItemTable = SItemTable;

          getDeptUom();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          TMatCode       = new JTextField();
          TMatName       = new JTextField();
          TMatCatl       = new JTextField();
          TMatDraw       = new JTextField();
          TMatPaperColour= new JTextField();
          TMatPaperSize  = new JTextField();

          JMatPaperSide  = new JComboBox(VSideName);
          JMatPaperSets  = new JComboBox(VPaperSet);
          JCDept         = new JComboBox(VDept);
          JCUom          = new JComboBox(VUom);
          JCMill         = new JComboBox();


          BOk            = new JButton("Okay");
          BCancel        = new JButton("Cancel");
          TopPanel       = new JPanel(true);
          BottomPanel    = new JPanel(true);
          
          TMatCode       . setEditable(false);

          if(iMillCode==0)
          {
               JCMill.setEnabled(true);
          }
          else
          {
               JCMill.setEnabled(false);
          }
     }

     public void setLayouts()
     {
          setLayout(new BorderLayout());
          TopPanel       . setLayout(new GridLayout(12,2));
          BottomPanel    . setLayout(new FlowLayout());
     }

     public void addComponents()
     {
          JCMill.addItem("NO");
          JCMill.addItem("YES");

          TopPanel.add(new JLabel("Stock Group"));
          TopPanel.add(JCDept);
          TopPanel.add(new JLabel("Material Belongs to All Company"));
          TopPanel.add(JCMill);
          TopPanel.add(new JLabel("Material Code"));
          TopPanel.add(TMatCode);
          TopPanel.add(new JLabel("Material Name"));
          TopPanel.add(TMatName);
          TopPanel.add(new JLabel("Unit of Measure"));
          TopPanel.add(JCUom);
          TopPanel.add(new JLabel("Catalogue No"));
          TopPanel.add(TMatCatl);
          TopPanel.add(new JLabel("Drawing No"));
          TopPanel.add(TMatDraw);

          if(iAuthCode>0)
          {
               BottomPanel.add(BOk);
          }
          BottomPanel.add(BCancel);

          add("North",TopPanel);
          add("South",BottomPanel);
     }

     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          JCDept    . addActionListener(new SelectList());
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(validData())
               {
                    BOk  . setEnabled(false);
                    setItemCode();
     
                    if (InsertMaterial())
                         refresh();
     
                    try
                    {
                         if(bAutoCommitFlag)
                         {
                              theConnection  . commit();
                              theDConnection . commit();
                              System         . out.println("Commit");
                              theConnection  . setAutoCommit(true);
                              theDConnection . setAutoCommit(true);
                         }
                         else
                         {
                              theConnection  . rollback();
                              theDConnection . rollback();
                              System         . out.println("RollBack");
                              theConnection  . setAutoCommit(true);
                              theDConnection . setAutoCommit(true);
                         }
                    }catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    BOk   . setEnabled(true);
                    JCDept. requestFocus();
               }
          }
     }

     public class SelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(((String)JCDept.getSelectedItem()).equals("STATIONARY"))
               {
                    bStatFlag = true;
                    TopPanel  . removeAll();

                    TopPanel  . add(new JLabel("Stock Group"));
                    TopPanel  . add(JCDept);
                    TopPanel  . add(new JLabel("Material Belongs to All Company"));
                    TopPanel  . add(JCMill);
                    TopPanel  . add(new JLabel("Material Code"));
                    TopPanel  . add(TMatCode);
                    TopPanel  . add(new JLabel("Material Name"));
                    TopPanel  . add(TMatName);
                    TopPanel  . add(new JLabel("Unit of Measure"));
                    TopPanel  . add(JCUom);
                    TopPanel  . add(new JLabel("Catalogue No"));
                    TopPanel  . add(TMatCatl);
                    TopPanel  . add(new JLabel("Drawing No"));
                    TopPanel  . add(TMatDraw);
          
                    TopPanel  . add(new JLabel(""));
                    TopPanel  . add(new JLabel(""));
          
                    TopPanel  . add(new JLabel("Paper Colour"));
                    TopPanel  . add(TMatPaperColour);
                    TopPanel  . add(new JLabel("Paper Sets"));
                    TopPanel  . add(JMatPaperSets);
                    TopPanel  . add(new JLabel("Paper Size"));
                    TopPanel  . add(TMatPaperSize);
                    TopPanel  . add(new JLabel("Paper Side"));
                    TopPanel  . add(JMatPaperSide);

                    JMatPaperSets  . setSelectedIndex(0);
                    JMatPaperSide  . setSelectedIndex(0);
                    TMatPaperColour. setText("");
                    TMatPaperSize  . setText("");
                    TopPanel  . updateUI();
               }
               else
               {
                    bStatFlag = false;

                    TopPanel  . removeAll();
                    TopPanel  . add(new JLabel("Stock Group"));
                    TopPanel  . add(JCDept);
                    TopPanel  . add(new JLabel("Material Belongs to All Company"));
                    TopPanel  . add(JCMill);
                    TopPanel  . add(new JLabel("Material Code"));
                    TopPanel  . add(TMatCode);
                    TopPanel  . add(new JLabel("Material Name"));
                    TopPanel  . add(TMatName);
                    TopPanel  . add(new JLabel("Unit of Measure"));
                    TopPanel  . add(JCUom);
                    TopPanel  . add(new JLabel("Catalogue No"));
                    TopPanel  . add(TMatCatl);
                    TopPanel  . add(new JLabel("Drawing No"));
                    TopPanel  . add(TMatDraw);
                    TopPanel  . updateUI();
               }
          }
     }

     public boolean validData()
     {
          String SName = common.getNarration(TMatName.getText());
          String SCatl = common.getNarration(TMatCatl.getText());
          String SDraw = common.getNarration(TMatDraw.getText());

                 SName = SName     . trim();
                 SCatl = SCatl     . trim();
                 SDraw = SDraw     . trim();

          if(SName.length()==0)
          {
               JOptionPane.showMessageDialog(null,"Item Name Must be filled","Information",JOptionPane.INFORMATION_MESSAGE);
               TMatName.requestFocus();
               return false;
          }

          if(SName.length()>50)
          {
               JOptionPane.showMessageDialog(null,"Name Can't Exceed 50 Char. Current Length - "+SName.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatName.requestFocus();
               return false;
          }

          if(SCatl.length()>30)
          {
               JOptionPane.showMessageDialog(null,"Catalogue Can't Exceed 30 Char. Current Length - "+SCatl.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatCatl.requestFocus();
               return false;
          }

          if(SDraw.length()>40)
          {
               JOptionPane.showMessageDialog(null,"DrawingNo Can't Exceed 40 Char. Current Length - "+SDraw.length(),"Information",JOptionPane.INFORMATION_MESSAGE);
               TMatDraw.requestFocus();
               return false;
          }

          return true;
     }

     public boolean InsertMaterial()
     {
          Vector VMillTable = new Vector();

          String SName = common.getNarration(TMatName.getText());
          String SCode = TMatCode  . getText();
          String SCatl = common.getNarration(TMatCatl.getText());
          String SDraw = common.getNarration(TMatDraw.getText());

                 SName = SName     . trim();
                 SCode = SCode     . trim();
                 SCatl = SCatl     . trim();
                 SDraw = SDraw     . trim();

          if(SName.length()==0)
               return false;
          
          if(SCode.length()==0)
               return false;
          
          if(iMillCode == 0)
          {
               if(JCMill.getSelectedIndex()==1)
                    VMillTable = getAllItemTables();

               String    QS = "";

               try
               {
                    ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                                   theConnection  = oraConnection.getConnection();
                                   theConnection  . setAutoCommit(true);
                    Statement      stat           = theConnection.createStatement();


                    DORAConnection DoraConnection = DORAConnection.getORAConnection();
                                   theDConnection = DoraConnection.getConnection();
                                   theDConnection . setAutoCommit(true);
                                   Statement dyestat =  theDConnection.createStatement();
     
                    if(theConnection.getAutoCommit())
                              theConnection  . setAutoCommit(false);
     
                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);


                    if(!bStatFlag)
                    {
                         QS = " Insert Into InvItems (Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,millcode,USERCODE,ID,CREATIONDATE) Values (";
                         QS = QS+"'"+TMatCode.getText()+"',";
                         QS = QS+"'"+SName.toUpperCase()+"',";
                         QS = QS+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                         QS = QS+"'"+SCatl.toUpperCase()+"',";
                         QS = QS+"'"+SDraw.toUpperCase()+"',";
                         QS = QS+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                         QS = QS+iMillCode+",";
                         QS = QS+iUserCode+",";
                         QS = QS+getNextVal("INVITEMS_SEQ")+",";
                         QS = QS+"'"+common.getServerDate()+"')";
                    }
                    else
                    {
                         QS = " Insert Into InvItems (Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,millcode,USERCODE,ID,CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE) Values (";
                         QS = QS+"'"+TMatCode.getText()+"',";
                         QS = QS+"'"+SName.toUpperCase()+"',";
                         QS = QS+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                         QS = QS+"'"+SCatl.toUpperCase()+"',";
                         QS = QS+"'"+SDraw.toUpperCase()+"',";
                         QS = QS+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                         QS = QS+iMillCode+",";
                         QS = QS+iUserCode+",";
                         QS = QS+getNextVal("INVITEMS_SEQ")+",";
                         QS = QS+"'"+common.getServerDate()+"',";
                         QS = QS+"'"+(((String)TMatPaperColour   . getText()).trim()).toUpperCase()+"',";
                         QS = QS+"'"+(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex())+"',";
                         QS = QS+"'"+(((String)TMatPaperSize.getText()).trim()).toUpperCase()+"',";
                         QS = QS+"'"+(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex())+"')";
                    }

                    stat . execute(QS);

                    if(JCMill.getSelectedIndex()==1)
                    {
                         String    QS1  = "";
                         String    QS2  = "";

                         if(VMillTable.size()>0)
                         {
                              for(int i=0;i<VMillTable.size();i++)
                              {
                                   QS1 = "";

                                   String SMillTable = (String)VMillTable.elementAt(i);

                                   String SSeq = SMillTable+"_SEQ";
               
                                   QS1 = " Insert Into "+SMillTable+" (Item_Code,StkGroupCode,USERCODE,ID,CREATIONDATE) Values (";
                                   QS1 = QS1+"'"+TMatCode.getText()+"',";
                                   QS1 = QS1+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                                   QS1 = QS1+iUserCode+",";
                                   QS1 = QS1+getNextVal(SSeq)+",";
                                   QS1 = QS1+"'"+common.getServerDate()+"')";

                                   stat . execute(QS1);
                              }
                         }

                         String SStkGroupCode = (String)VDeptCode.elementAt(JCDept.getSelectedIndex());
     
                         if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         {
                              QS2 = " Insert Into InvItems (Item_Code,Item_Name,UOM,StkGroupCode) Values (";
                              QS2 = QS2+"'"+TMatCode.getText()+"',";
                              QS2 = QS2+"'"+SName.toUpperCase()+"',";
                              QS2 = QS2+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                              QS2 = QS2+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"')";

                              dyestat.execute(QS2);
                         }
                    }
                    stat.close();
                    dyestat.close();

               }catch(Exception e)
               {
                    System.out.println(e);
                    bAutoCommitFlag     = false;
                    e.printStackTrace();
               }
          }
          else
          {
               String    QS   = "";
               String    QS1  = "";
               String    QS2  = "";
               try
               {
                    if(!bStatFlag)
                    {
                         QS = " Insert Into InvItems (Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,millcode,USERCODE,ID,CREATIONDATE) Values (";
                         QS = QS+"'"+TMatCode.getText()+"',";
                         QS = QS+"'"+SName.toUpperCase()+"',";
                         QS = QS+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                         QS = QS+"'"+SCatl.toUpperCase()+"',";
                         QS = QS+"'"+SDraw.toUpperCase()+"',";
                         QS = QS+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                         QS = QS+iMillCode+",";
                         QS = QS+iUserCode+",";
                         QS = QS+getNextVal("INVITEMS_SEQ")+",";
                         QS = QS+"'"+common.getServerDate()+"')";
                    }
                    else
                    {
                         QS = " Insert Into InvItems (Item_Code,Item_Name,UOMCode,Catl,Draw,StkGroupCode,millcode,USERCODE,ID,CREATIONDATE,PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE) Values (";
                         QS = QS+"'"+TMatCode.getText()+"',";
                         QS = QS+"'"+SName.toUpperCase()+"',";
                         QS = QS+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                         QS = QS+"'"+SCatl.toUpperCase()+"',";
                         QS = QS+"'"+SDraw.toUpperCase()+"',";
                         QS = QS+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                         QS = QS+iMillCode+",";
                         QS = QS+iUserCode+",";
                         QS = QS+getNextVal("INVITEMS_SEQ")+",";
                         QS = QS+"'"+common.getServerDate()+"',";
                         QS = QS+"'"+(((String)TMatPaperColour   . getText()).trim()).toUpperCase()+"',";
                         QS = QS+"'"+(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex())+"',";
                         QS = QS+"'"+(((String)TMatPaperSize     . getText()).trim()).toUpperCase()+"',";
                         QS = QS+"'"+(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex())+"')";
                    }

                    String SSeq = SItemTable+"_SEQ";

                    QS1 = " Insert Into "+SItemTable+" (Item_Code,StkGroupCode,USERCODE,ID,CREATIONDATE) Values (";
                    QS1 = QS1+"'"+TMatCode.getText()+"',";
                    QS1 = QS1+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"',";
                    QS1 = QS1+iUserCode+",";
                    QS1 = QS1+getNextVal(SSeq)+",";
                    QS1 = QS1+"'"+common.getServerDate()+"')";


                    String SStkGroupCode = (String)VDeptCode.elementAt(JCDept.getSelectedIndex());

                    if(iMillCode==1)
                    {
                         if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         {
                              QS2 = " Insert Into InvItems (Item_Code,Item_Name,UOM,StkGroupCode) Values (";
                              QS2 = QS2+"'"+TMatCode.getText()+"',";
                              QS2 = QS2+"'"+SName.toUpperCase()+"',";
                              QS2 = QS2+"'"+(String)VUomCode.elementAt(JCUom.getSelectedIndex())+"',";
                              QS2 = QS2+"'"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"')";
                         }
                    }

                    ORAConnection  oraConnection =  ORAConnection.getORAConnection();
                                   theConnection =  oraConnection.getConnection();
                                   theConnection .  setAutoCommit(true);
                    Statement      stat          =  theConnection.createStatement();

               DORAConnection DoraConnection = DORAConnection.getORAConnection();
                              theDConnection = DoraConnection.getConnection();
                              theDConnection . setAutoCommit(true);
                              Statement dyestat =  theDConnection.createStatement();
               

               if(theConnection.getAutoCommit())
                         theConnection  . setAutoCommit(false);

                    if(theDConnection.getAutoCommit())
                         theDConnection . setAutoCommit(false);


                    stat . execute(QS);
                    stat . execute(QS1);

                    if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                         dyestat.execute(QS2);

                    stat.close();
                    dyestat.close();
               }
               catch(Exception ex)
               {
                    bAutoCommitFlag     = false;
                    System.out.println(ex);
               }
          }
          return true;
     }

     public Vector getAllItemTables()
     {
          Vector vect = new Vector();

          String QS = " Select distinct(Item_Table) from Mill where MillCode>0 ";

          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();
                               theConnection . setAutoCommit(true);
               Statement stat                = theConnection.createStatement();
               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    vect . addElement(res.getString(1));

               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System         . out.println(ex);
          }
          return vect;
     }

     public void setItemCode()
     {
          String SCode="";

          String str   =  (String)VDeptCode.elementAt(JCDept.getSelectedIndex());

          String QS = " Select (maxno+1) From StockGroup Where GroupCode='"+str+"' for update of MaxNo noWait";

          Vector vect  = new Vector();
          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();

               if(theConnection . getAutoCommit())
                    theConnection  . setAutoCommit(false);

               Statement stat               = theConnection.createStatement();

               PreparedStatement thePrepare = theConnection.prepareStatement(" Update StockGroup set MaxNo = ?  where GroupCode='"+str+"'");

               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    SCode = common.parseNull((String)res.getString(1));

               res.close();


               thePrepare.setInt(1,common.toInt(SCode));
               thePrepare.executeUpdate();

               stat.close();
               thePrepare.close();

               SCode = common.stuff(SCode);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();

               String SException = String.valueOf(ex);

               if((SException.trim()).equals("java.sql.SQLException: ORA-00054: resource busy and acquire with NOWAIT specified"))
               {
                    JOptionPane    . showMessageDialog(null,"retry","Information",JOptionPane.INFORMATION_MESSAGE);
                    try
                    {
                    }catch(Exception Ex)
                    {
                        System.out.println(Ex);
                        Ex.printStackTrace();
                    }
                    setItemCode();
                    bAutoCommitFlag  = true;
               }
               else
               {
                    TMatCode       . setText("");
                    JOptionPane    . showMessageDialog(null,"Problem in Assigning Material Code","Information",JOptionPane.INFORMATION_MESSAGE);
                    bAutoCommitFlag  = false;
               }
          }
          TMatCode       . setText(str+SCode);
          JOptionPane    . showMessageDialog(null,"Material Code is "+str+SCode,"Information",JOptionPane.INFORMATION_MESSAGE);
     }

     /*public void setItemCode()
     {
          String str   =  (String)VDeptCode.elementAt(JCDept.getSelectedIndex());

          String    QS = " SELECT substr(Item_Code,4,6)"+
                         " FROM InvItems "+
                         " WHERE substr(item_code,1,3)='"+str+"'"+
                         " ORDER BY 1 ";

          Vector vect  = new Vector();
          try
          {
               ORAConnection   oraConnection = ORAConnection.getORAConnection();
               Connection      theConnection = oraConnection.getConnection();
                               theConnection . setAutoCommit(true);
               Statement stat                = theConnection.createStatement();
               ResultSet res = stat.executeQuery(QS);

               while(res.next())
                    vect . addElement(res.getString(1));                        

               res.close();
               stat.close();

               String SCode = common.stuff(String.valueOf(common.getMissing(vect)));

               TMatCode       . setText(str+SCode);
               JOptionPane    . showMessageDialog(null,"Material Code is "+str+SCode,"Information",JOptionPane.INFORMATION_MESSAGE);
          }
          catch(Exception ex)
          {
               System         . out.println(ex);
               TMatCode       . setText("");
               JOptionPane    . showMessageDialog(null,"Problem in Assigning Material Code","Information",JOptionPane.INFORMATION_MESSAGE);
          }
     }*/

     public void refresh()
     {
          TMatCode       . setText(""); 
          TMatName       . setText("");
          TMatCatl       . setText("");
          TMatDraw       . setText("");
          TMatPaperColour. setText(""); 
          TMatPaperSize  . setText("");

          JCDept         . setSelectedIndex(0);
          JCUom          . setSelectedIndex(0);

          if(((String)JCDept.getSelectedItem()).equals("STATIONARY"))
          {
               JMatPaperSets  . setSelectedIndex(0);
               JMatPaperSide  . setSelectedIndex(0);
          }

          JCMill.setSelectedIndex(0);

          if(iMillCode==0)
          {
               JCMill.setEnabled(true);
          }
          else
          {
               JCMill.setEnabled(false);
          }
     }

     public void getDeptUom()
     {
          ResultSet result    = null;

          VDept          = new Vector();
          VDeptCode      = new Vector();
          VUom           = new Vector();
          VUomCode       = new Vector();
          VPaperSet      = new Vector();
          VSetCode       = new Vector();
          VSideName      = new Vector();
          VSideCode      = new Vector();

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                               theConnection .  setAutoCommit(true);
               Statement       stat          =  theConnection.createStatement();

               result = stat.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select UomName,UomCode From UOM Order By UomName");

               while(result.next())
               {
                    VUom      . addElement(result.getString(1));
                    VUomCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SetName,SetCode From PaperSet Order By SetCode");

               while(result.next())
               {
                    VPaperSet . addElement(result.getString(1));
                    VSetCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SideName,SideCode From PaperSide Order By SideCode");

               while(result.next())
               {
                    VSideName . addElement(result.getString(1));
                    VSideCode . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("Dept & UOM :"+ex);
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int       iValue    = -1;
          String    QS        = " Select "+SSequence+".NextVal from dual ";
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }
}
