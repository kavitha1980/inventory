package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MatGroupMaster extends JInternalFrame 
{
     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TMatName;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VMatName,VSMatName;
     Common         common = new Common();

     //MyComboBox     JCBranch;

     Connection theConnection = null;

     int iUserCode=0;
     int iMillCode=0;

     public  MatGroupMaster(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer     = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;

          setVectorSample();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          TMatName       = new AddressField(30);
          VMatName      = new Vector();
          VSMatName     = new Vector();
          //JCBranch       = new MyComboBox();

          //JCBranch.addItem("AmarjothiSpinningMill");
          //JCBranch.addItem("AmarjothiDyeingDivision");
          //JCBranch.addItem("Both");

          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle(" Mat Group Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,600,500);

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          MiddlePanel.setBorder(new TitledBorder("Party Details"));
     }
     private void addComponents()
     {
          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel     .    add(new JLabel("Group"));
          TopPanel     .    add(TMatName);


          MiddlePanel.add("Center",thePane);
          thePane.addTab("Mat Group List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"GROUP NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VMatName.size()][ColumnName.length];
               for(int i=0;i<VMatName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VMatName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("MatGroup List ",theReport);
                    else
                         thePane.addTab("MatGroup List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TMatName.setText("");
                    TMatName.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TMatName.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Mat Group Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    TMatName.requestFocus();
                    return false;
               }
               if(common.parseNull(TMatName.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Mat Group Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TMatName.requestFocus();
                    return false;
               }
               if(TMatName.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Mat Group Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TMatName.requestFocus();
                    TMatName.setText("");
                    return false;
               }
               if(TMatName.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Mat Group Name must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TMatName.requestFocus();
                    TMatName.setText("");
                    return false;
               } 
               return true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               int iMatCode             = getMaxMatCode();
               String SMatName          = TMatName.getText().trim().toUpperCase();

               insertMatName(iMatCode,SMatName,iMillCode,iUserCode);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Group Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VMatName   = new Vector();

          String QS   = "Select GroupName from MatGroup ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VMatName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void setVectorSample()
     {
          VSMatName   = new Vector();

          String QS   = "Select GroupName from MatGroup ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VSMatName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     public void insertMatName(int iMatCode,String SMatName,int iMillCode,int iUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertMatNameQS(iMatCode,SMatName,iMillCode,iUserCode));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInsertMatNameQS(int iMatCode,String SMatName,int iMillCode,int iUserCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into MatGroup(id,GroupCode,GroupName,UserCode,CreationDate) Values (";
               QS = QS+"  MatGroup_seq.nextval ,";
               QS = QS+" '"+iMatCode+"',";
               QS = QS+" '"+SMatName+"',";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Mat "+e);
          }
          return QS;
     }
     public  int getMaxMatCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxMatQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxMatQS()
     {
          String QS = " Select Max(GroupCode)+1 from MatGroup ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select GroupName from MatGroup Where GroupName='"+SName+"' ";
          return QS;
     }



}

