package vital;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MatTranPanel extends JPanel
{

     JPanel         DeptPanel,SortPanel;
     JPanel         ApplyPanel;
     
     Common         common = new Common();
     Vector         VDept,VDeptCode;
     JComboBox      JCDept;
     
     JRadioButton   JRAllDept,JRSeleDept;
     
     JTextField     TSort,TFind,TFind1;
     
     JButton        BApply;
     
     MatTranPanel()
     {
          getDept();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public void createComponents()
     {
          DeptPanel      = new JPanel();
          ApplyPanel     = new JPanel();
          SortPanel      = new JPanel();
          
          JRAllDept      = new JRadioButton("All");
          JRSeleDept     = new JRadioButton("Selected",true);
          JCDept         = new JComboBox(VDept);
          TSort          = new JTextField();
          TFind          = new JTextField();
          TFind1         = new JTextField();
          BApply         = new JButton("Apply");
     }

     public void setLayouts()
     {
          setLayout(new GridLayout(1,3));
          DeptPanel      . setLayout(new GridLayout(3,1));   
          DeptPanel      . setBorder(new TitledBorder("Stock Group"));
          SortPanel      . setLayout(new GridLayout(3,2));
          SortPanel      . setBorder(new TitledBorder("Sort & Find"));
     }

     public void addComponents()
     {
          add(DeptPanel);
          add(ApplyPanel);
          add(SortPanel);
          
          DeptPanel . add(JRAllDept);
          DeptPanel . add(JRSeleDept);
          DeptPanel . add(JCDept);    
          
          SortPanel . add(new JLabel("Sort Order"));
          SortPanel . add(TSort);
          SortPanel . add(new JLabel("By Name"));
          SortPanel . add(TFind);
          SortPanel . add(new JLabel("By Code"));
          SortPanel . add(TFind1);
          
          TFind     . setEditable(false);
          ApplyPanel. add(BApply);
     }

     public void addListeners()
     {
          JRAllDept . addActionListener(new DeptList());
          JRSeleDept. addActionListener(new DeptList());
     }

     public class DeptList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept . setSelected(true);
                    JRSeleDept. setSelected(false);
                    JCDept    . setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {
                    JRAllDept . setSelected(false);
                    JRSeleDept. setSelected(true);
                    JCDept    . setEnabled(true);
               }
          }
     }

     public void getDept()
     {
          VDept     = new Vector();
          VDeptCode = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
               ResultSet      result         = stat.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result.next())
               {
                    VDept     . addElement(result.getString(1));
                    VDeptCode . addElement(result.getString(2));
               }
          }
          catch(Exception ex)
          {
               System.out.println("Dept :"+ex);
          }
     }
}
