package vital;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;
import javax.swing.border.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MaterialAbolishedFrame extends JInternalFrame
{
      JLayeredPane  Layer;
      Object        RowData[][];

      String        ColumnData[] = {"Code","Item Name","UoM","Stock Group","Stock","Value","Last TransDate","Days","Click for Modify"};
      String        ColumnType[] = {"S"   ,"S"        ,"S"  ,"S"          ,"N"    ,"N"    ,"S"             ,"N"   ,"B"               };

      Vector        VMCode,VMName,VMDept,VMUoM,VMStock,VMValue,VMDate,VMDays,VMDeptCode;

      JTabbedPane   JTP;
      JPanel        ListPanel;
      JButton       BOk;
      Common        common = new Common();
      JPanel        TopPanel;
      JPanel 	    DeptPanel,ApplyPanel,FindPanel;

      Vector 	    VDept,VDeptCode;
      JComboBox     JCDept;

      JRadioButton  JRAllDept,JRSeleDept;
      DateField     TDate;

      JTextField    TFind,TFind1;

      JButton       BApply;


      TabReport     tabreport;
      String        SFind="";
      int           iMillCode,iAuthCode;
      String        SItemTable;

      Connection theConnection = null;

      public MaterialAbolishedFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,String SItemTable)
      {
            this.Layer        = Layer;
            this.iMillCode    = iMillCode;
            this.iAuthCode    = iAuthCode;
            this.SItemTable   = SItemTable;

            getDept();     
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }
         
      public void createComponents()
      {
	   TopPanel	  = new JPanel();
           DeptPanel      = new JPanel();
           ApplyPanel     = new JPanel();
           FindPanel      = new JPanel();

           ListPanel      = new JPanel(true);

           JTP            = new JTabbedPane();

           TDate          = new DateField();

           JRAllDept      = new JRadioButton("All");
           JRSeleDept     = new JRadioButton("Selected",true);
           JCDept         = new JComboBox(VDept);
           TFind          = new JTextField();
           TFind1         = new JTextField();
           BApply         = new JButton("Apply");
           BOk            = new JButton("Update");

           TDate.setTodayDate();

           TFind.setEditable(false);
           TFind1.setEditable(false);
	   BOk.setEnabled(false);
      }

      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,850,500);
            setTitle("Material For Qty Allownace");

            TopPanel.setLayout(new GridLayout(1,3));
            DeptPanel.setLayout(new GridLayout(3,1));   
            DeptPanel.setBorder(new TitledBorder("Stock Group"));
            ApplyPanel.setLayout(new GridLayout(3,1));   
            ApplyPanel.setBorder(new TitledBorder("Control"));
            FindPanel.setLayout(new GridLayout(2,2));
            FindPanel.setBorder(new TitledBorder("Find"));

            ListPanel.setLayout(new BorderLayout());
      }

      public void addComponents()
      {
           DeptPanel.add(JRAllDept);
           DeptPanel.add(JRSeleDept);
           DeptPanel.add(JCDept);    

           ApplyPanel.add(new JLabel("As on"));
           ApplyPanel.add(TDate);
           ApplyPanel.add(BApply);

           FindPanel.add(new JLabel("By Name"));
           FindPanel.add(TFind);
           FindPanel.add(new JLabel("By Code"));
           FindPanel.add(TFind1);

           TopPanel.add(DeptPanel);
           TopPanel.add(ApplyPanel);
           TopPanel.add(FindPanel);

           ListPanel.add("North",TopPanel);

           if(iAuthCode>1)
           {
               ListPanel.add("South",BOk);
           }

           JTP.addTab("List of Materials",ListPanel);

           getContentPane().add("Center",JTP);
      }

      public void addListeners()
      {
          JRAllDept.addActionListener(new DeptList());
          JRSeleDept.addActionListener(new DeptList());

          BApply.addActionListener(new ActList());
          TFind.addKeyListener(new KeyList());
          TFind1.addKeyListener(new KeyList1());

          BOk.addActionListener(new UpdtList());
      }

      public class DeptList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAllDept)
               {
                    JRAllDept.setSelected(true);
                    JRSeleDept.setSelected(false);
                    JCDept.setEnabled(false);
               }
               if(ae.getSource()==JRSeleDept)
               {
                    JRAllDept.setSelected(false);
                    JRSeleDept.setSelected(true);
                    JCDept.setEnabled(true);
               }
          }
      }

      public class ActList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  TFind.setEditable(true);
                  TFind1.setEditable(true);

                  setVector();
                  setRowData();
                  try
                  {
                       ListPanel.remove(tabreport); 
                  }
                  catch(Exception ex)
                  {
                  }
                  try
                  {
                     tabreport = new TabReport(RowData,ColumnData,ColumnType);
                     ListPanel.add(tabreport,BorderLayout.CENTER);
		     tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		     if(VMCode.size()>0)
		     {
		          BOk.setEnabled(true);
		     }
                     Layer.repaint();
                     Layer.updateUI();
                  }
                  catch(Exception ex)
                  {
                     System.out.println("Err2"+ex);
                  }
            }
      }

      public class UpdtList implements ActionListener
      {
         public void actionPerformed(ActionEvent ae)
         {
             if(validData())
             {
                  BOk.setEnabled(false);
                  updateMaterials();
                  removeHelpFrame();
	     }
         }
      }

      public class KeyList extends KeyAdapter
      {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = TFind.getText();
               int i=indexOf(SFind);
               if (i>-1)
               {
                    tabreport.ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                    tabreport.ReportTable.scrollRectToVisible(cellRect);
               }
          }
      }

      public class KeyList1 extends KeyAdapter
      {
           public void keyReleased(KeyEvent ke)
           {
                   String SFind = TFind1.getText();
                   int i=codeIndexOf(SFind);
                   if (i>-1)
                   {
                           tabreport.ReportTable.setRowSelectionInterval(i,i);
                           Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                           tabreport.ReportTable.scrollRectToVisible(cellRect);
                   }
           }
      }

      public int indexOf(String SFind)
      {
           SFind = SFind.toUpperCase();
           for(int i=0;i<VMName.size();i++)
           {
                   String str = (String)VMName.elementAt(i);
                   str=str.toUpperCase();
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int codeIndexOf(String SFind)
      {
           for(int i=0;i<VMCode.size();i++)
           {
                   String str = (String)VMCode.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public void setVector()
      {
            VMCode       = new Vector();
            VMName       = new Vector();
            VMDept       = new Vector();
            VMUoM        = new Vector();
            VMStock      = new Vector();
            VMValue      = new Vector();
            VMDate       = new Vector();
            VMDays       = new Vector();
            VMDeptCode   = new Vector();

            try
            {
                  if(theConnection == null)
                  {
                       ORAConnection jdbc   = ORAConnection.getORAConnection();
                       theConnection        = jdbc.getConnection();
                  }

                  Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery(getQString());
                  while(res.next())
                  {
                        double dOpg   = common.toDouble(res.getString(5));
                        double dOVal  = common.toDouble(res.getString(6));
                        double dRec   = common.toDouble(res.getString(7));
                        double dRVal  = common.toDouble(res.getString(8));
                        double dIss   = common.toDouble(res.getString(9))+common.toDouble(res.getString(11));
                        double dIVal  = common.toDouble(res.getString(10))+common.toDouble(res.getString(12));

                        double dStock = dOpg+dRec-dIss;
                        double dValue = dOVal+dRVal-dIVal;
                        double dRate  = dValue / dStock;

                        if(common.getRound(dStock,3).equals("0.000") && common.getRound(dValue,2).equals("0.00"))
                            continue;

                        VMCode     .addElement(res.getString(2));
                        VMName     .addElement(res.getString(3));
                        VMDept     .addElement(res.getString(1));
                        VMUoM      .addElement(res.getString(4));
			VMStock    .addElement(common.getRound(dStock,3));
			VMValue    .addElement(common.getRound(dValue,2));
			VMDate     .addElement(common.parseNull(res.getString(16)));
			VMDays     .addElement(common.parseNull(res.getString(17)));
                        VMDeptCode .addElement(common.parseNull(res.getString(14)));
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }

      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {
                        RowData[i][0] = common.parseNull((String)VMCode.elementAt(i));      
                        RowData[i][1] = common.parseNull((String)VMName.elementAt(i));
                        RowData[i][2] = common.parseNull((String)VMUoM.elementAt(i));
                        RowData[i][3] = common.parseNull((String)VMDept.elementAt(i));
                        RowData[i][4] = common.parseNull((String)VMStock.elementAt(i));
                        RowData[i][5] = common.parseNull((String)VMValue.elementAt(i));
                        RowData[i][6] = common.parseDate((String)VMDate.elementAt(i));
                        RowData[i][7] = common.parseNull((String)VMDays.elementAt(i));
                        RowData[i][8] = new Boolean(false);
            }
      }

     public boolean validData()
     {
	  int iCount=0;

          for(int i=0;i<tabreport.ReportTable.getRowCount();i++)
          {
               Boolean bFlag     = (Boolean)tabreport.ReportTable.getValueAt(i,8);

               if(bFlag.booleanValue())
               {
                    iCount++;
               }
          }

          if(iCount<=0)
          {
               JOptionPane.showMessageDialog(null,"No Row Selected ","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }

          return true;
     }

      public void updateMaterials()
      {
           try
           {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();

	       String SAbolishedDate = common.getServerPureDate();

                for(int i=0;i<VMCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][8];

                     if(Bselected.booleanValue())
                     {
                     	 String SItemCode  = (String)VMCode.elementAt(i);

			 String QS = " Insert into AbolishedItems (Id,Item_Code,MillCode,AbolishedDate) Values (AbolishedItems_Seq.nextval,"+
			             "'"+SItemCode+"',"+iMillCode+","+SAbolishedDate+")";

                         theStatement.execute(QS);
                    }
                }
                theStatement.close();
           }
           catch(Exception ex)
           {
                 System.out.println("Err4"+ ex);
           }
      }

      public String getQString()
      {
           String SGrnFilter="",SIssueFilter="";
           String SPyGrnFilter="",SPyIssueFilter="";

           String SDate   = TDate.toNormal();

           if(iMillCode==0)
           {
                SGrnFilter     = " And (Grn.MillCode is Null OR GRN.MillCode = "+iMillCode+") ";
                SIssueFilter   = " And (Issue.MillCode is Null OR Issue.MillCode = "+iMillCode+")";
                SPyIssueFilter = " And (PYissue.MillCode is Null OR  PYIssue.MillCode ="+iMillCode+")";
                SPyGrnFilter   = " And (PYGrn.MillCode is Null OR  PYGrn.MillCode ="+iMillCode+")";
           }
           else
           {
                SGrnFilter     = " And  GRN.MillCode = "+iMillCode;
                SIssueFilter   = " And  Issue.MillCode = "+iMillCode;
                SPyIssueFilter = " And  PYIssue.MillCode ="+iMillCode;
                SPyGrnFilter   = " And  PYGrn.MillCode ="+iMillCode;
           }

           String QS1 = " SELECT InvItems.Item_Code as Code, InvItems.Item_Name, " ;

                        if(iMillCode==0)
                             QS1 = QS1+ " Sum(nvl(InvItems.OpgQty,0)) as OpgQty, Sum(nvl(InvItems.OpgVal,0)) as OpgVal, ";
                        else
                             QS1 = QS1+ " Sum(nvl("+SItemTable+".OpgQty,0)) as OpgQty, Sum(nvl("+SItemTable+".OpgVal,0)) as OpgVal, ";

 		        if(iMillCode==0)
		        {
		             QS1 = QS1+" Uom.UomName, StockGroup.GroupName,InvItems.LocName, "+
		                   " InvItems.StkGroupCode,InvItems.ExClass ";
		        }
		        else
		        {
		             QS1 = QS1+" Uom.UomName, StockGroup.GroupName,"+SItemTable+".LocName, "+
		                   " "+SItemTable+".StkGroupCode,InvItems.ExClass ";
		        }

		        if(iMillCode==0)
		        {     QS1 = QS1 + " FROM ((InvItems Inner Join StockGroup on "+
		                    " InvItems.StkGroupCode=StockGroup.GroupCode )"+
		                    " Inner Join Uom on InvItems.UomCode=Uom.UomCode )";
		        }
		        else
		        {
		             QS1 = QS1 + " FROM (("+SItemTable+" Inner Join StockGroup on "+
		                   " "+SItemTable+".StkGroupCode=StockGroup.GroupCode) ";
		        }     

		        if(iMillCode!=0)
		             QS1 = QS1+  " inner join InvItems on "+SItemTable+".Item_Code = InvItems.Item_Code) "+
		                         " inner join Uom on InvItems.UomCode=Uom.UomCode ";

		        if(iMillCode==0)
		        {
		             QS1 = QS1+" Group by InvItems.Item_Code, InvItems.Item_Name, "+
		             " Uom.UomName, StockGroup.GroupName, "+
		             " invitems.locname,InvItems.StkGroupCode,InvItems.ExClass ";
		        }
		        else
		        {
		             QS1 = QS1+" Group by InvItems.Item_Code, InvItems.Item_Name, "+
		             " Uom.UomName, StockGroup.GroupName, "+
		             " "+SItemTable+".locname,"+SItemTable+".StkGroupCode,InvItems.ExClass ";
		        }
  
           String QS2 = " Select Code,sum(GrnQty) as GrnQty,sum(GrnVal) as GrnVal from "+
                        " (SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, "+
                        " Sum(GRN.GrnValue) as GrnVal "+
                        " FROM GRN "+
                        " WHERE Grn.RejFlag=0 and GRN.GrnDate <='"+SDate+"' "+SGrnFilter+" "+
                        " GROUP BY GRN.Code "+
                        " Union All "+
                        " SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, "+
                        " Sum(GRN.GrnValue) as GrnVal "+
                        " FROM GRN "+
                        " WHERE Grn.RejFlag=1 and GRN.RejDate <='"+SDate+"' "+SGrnFilter+" "+
                        " GROUP BY GRN.Code) "+
                        " Group By Code ";

           String QS2a = " SELECT Code, Sum(GrnQty) AS GrnQty, Sum(GrnVal) as GrnVal from "+
                         " (SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=0 and (GrnType<>2 and GrnBlock>1) And GRN.GrnDate <='"+SDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code "+
                         " Union All "+
                         " SELECT GRN.Code, Sum(GRN.GrnQty) AS GrnQty, Sum(GRN.GrnValue) as GrnVal "+
                         " FROM GRN "+
                         " WHERE Grn.RejFlag=1 and (GrnType<>2 and GrnBlock>1) And GRN.RejDate <='"+SDate+"' "+SGrnFilter+" "+
                         " GROUP BY GRN.Code) "+
                         " Group By Code ";

           String QS3  = " SELECT Issue.Code, Sum(Issue.Qty) AS IssQty, Sum(Issue.IssueValue) as IssVal "+
                         " FROM Issue "+
                         " WHERE Issue.IssueDate <='"+SDate+"' "+SIssueFilter+" "+
                         " GROUP BY Issue.Code";

           String QS3a = " SELECT CODE,MAX(MAXDATE) AS LASTDATE FROM "+
                         " (SELECT ISSUE.CODE AS CODE, MAX(ISSUE.ISSUEDATE) AS MAXDATE FROM ISSUE "+
                         " WHERE ISSUE.ISSUEDATE <='"+SDate+"' "+SIssueFilter+" GROUP BY ISSUE.CODE "+
                         " UNION ALL "+
                         " SELECT PYISSUE.CODE AS CODE, MAX(PYISSUE.ISSUEDATE) AS MAXDATE FROM PYISSUE "+
                         " WHERE PYISSUE.ISSUEDATE <='"+SDate+"' "+SPyIssueFilter+" GROUP BY PYISSUE.CODE) "+
                         " GROUP BY CODE "+
                         " ORDER BY CODE ";

           String QS4  = " SELECT CODE,MAX(MAXDATE) AS LASTDATE FROM "+
                         " (SELECT GRN.CODE AS CODE,MAX(GRN.GRNDATE) AS MAXDATE FROM GRN "+
                         " WHERE GRN.GRNDATE <='"+SDate+"' "+SGrnFilter+" GROUP BY GRN.CODE "+
                         " UNION ALL "+
                         " SELECT PYGRN.CODE AS CODE,MAX(PYGRN.GRNDATE) AS MAXDATE FROM PYGRN "+
                         " WHERE PYGRN.GRNDATE <='"+SDate+"' "+SPyGrnFilter+" GROUP BY PYGRN.CODE) "+
                         " GROUP BY CODE ";

           String QS5  = " SELECT CODE,MAX(LASTDATE) AS LASTTRANSDATE FROM "+  
                         " (SELECT CODE,LASTDATE FROM ("+QS3a+") C10) "+
                         " GROUP BY CODE ";

           String QS6  = " SELECT c1.GroupName, c1.code, c1.Item_Name, "+
                         " c1.UomName, c1.OpgQty, c1.OpgVal, "+
                         " c2.GrnQty, c2.GrnVal, c3.IssQty, "+
                         " c3.IssVal,c2a.GrnQty,c2a.GrnVal, "+
                         " c1.LocName,c1.StkGroupCode,c1.ExClass, "+
                         " c12.LastTransDate,to_Date("+SDate+",'yyyymmdd')-to_Date(c12.LastTransDate,'yyyymmdd') as Days"+
                         " FROM ("+QS1+") c1 "+
                         " Left JOIN ("+QS2+") c2 ON c1.code = c2.Code "+
                         " Left Join ("+QS5+") C12 On C1.code = c12.code "+
                         " Left JOIN ("+QS2a+") c2a ON c1.code = c2a.code "+
                         " Left JOIN ("+QS3+") c3 ON c1.code = c3.code "+
                         " Where (c12.LastTransDate <='"+SDate+"' or c12.LastTransDate is Null) "+
			 " And c1.code not in (select Item_Code from AbolishedItems Where MillCode="+iMillCode+")";

		         if (JRSeleDept.isSelected())
		         {
		              QS6 = QS6+" And c1.StkGroupCode = '"+(String)VDeptCode.elementAt(JCDept.getSelectedIndex())+"'";
		         }

                         QS6 = QS6 + " Order by 1,3 ";

           return QS6;
      }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("Err5"+ex);
          }
     }

     public void getDept()
     {
          VDept     = new Vector();
          VDeptCode = new Vector();

          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
               ResultSet result1= theStatement.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result1.next())
               {
                    VDept     .addElement(result1.getString(1));
                    VDeptCode .addElement(result1.getString(2));
               }
               result1.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("Err3 :"+ex);
          }
     }

}
