package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialFrame extends JInternalFrame
{
     JLayeredPane        Layer;

     private Object              RowData[][];
     private String              ColumnData[] = {"Code","Item Name","UoM","Stock Group","Drawing","Catl","Opg Qty","Opg Val","ROQ","Min Qty","Max Qty","Location","Regular","Group","Hod","Click for Modify"};
     private String              ColumnType[] = {"S"   ,"B"        ,"B"  ,"E"          ,"E"      ,"E"   ,"S"      ,"S"      ,"E"  ,"E"      ,"E"      ,"E"      ,"B"        ,"E"    ,"E"  ,"B"               };

     private Vector              VHodCode,VHodName,VGroupCode,VGroupName,VStockGroupCode,VStockGroupName;
     private Vector              VUOMCode,VUOMName;

     private JTabbedPane         JTP;
     private JPanel              ListPanel,CreatePanel;
     private JButton             BOk;
     private Common              common;
     private MatPanel            matpanel;
     private TabReportNew        tabreport;
     private String              SFind     = "";

     Runtime                     rt        = null;

     private int                 iMillCode,iAuthCode,iUserCode;
     private String              SItemTable;

     public MaterialFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;
          this.SItemTable     = SItemTable;

          common              = new Common();
          matpanel            = new MatPanel(iMillCode);
          JTP                 = new JTabbedPane();
          CreatePanel         = new JPanel(true);
          ListPanel           = new JPanel(true);

          BOk                 = new JButton("Update");

          JTP                 . addTab("New Materials",CreatePanel);
          JTP                 . addTab("List of Materials",ListPanel);

          matpanel  . TFind.setEditable(true);
          matpanel  . TFind1.setEditable(true);
          matpanel  . TCatl.setEditable(true);
          matpanel  . TDraw.setEditable(true);

          rt = Runtime.getRuntime();
          setVectors();
          setLayouts();
     }
         
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Materials");

          CreatePanel         . setLayout(new BorderLayout());
          try
          {
               //CreatePanel    . add("North",new MatCreatePanel(iMillCode,iAuthCode,iUserCode,SItemTable));
			   CreatePanel    . add("North",new MatCreatePanelGST(iMillCode,iAuthCode,iUserCode,SItemTable));
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          ListPanel . setLayout(new BorderLayout());
          ListPanel . add("North",matpanel);
             
          if(iAuthCode>1)
          {
               ListPanel.add("South",BOk);
          }

          matpanel            . BApply.addActionListener(new ActList());
          BOk                 . addActionListener(new UpdtList());
          getContentPane()    . add("Center",JTP);
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               updateMaterials();
               System.runFinalization();
               System.gc();
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               System.runFinalization();
               System.gc();

               BOk       . setEnabled(true);
               setVector();

               try
               {
                    ListPanel . remove(tabreport);
               }
               catch(Exception ex)
               {
               }
               try
               {
                                   tabreport      = new TabReportNew(RowData,ColumnData,ColumnType);
                                   tabreport      . ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                                   ListPanel      . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);

                                   Layer          . repaint();
                                   Layer          . updateUI();
                                                  
                    TableColumn    UomColumn      = tabreport.ReportTable.getColumn("UoM");
                    TableColumn    HodColumn      = tabreport.ReportTable.getColumn("Hod");
                    TableColumn    GroupColumn    = tabreport.ReportTable.getColumn("Group");
                    TableColumn    StockColumn    = tabreport.ReportTable.getColumn("Stock Group");
                                   UomColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VUOMName)));
                                   HodColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VHodName)));
                                   GroupColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VGroupName)));
                                   StockColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VStockGroupName)));

                                   matpanel       . TFind   . addKeyListener(new KeyList());
                                   matpanel       . TFind1  . addKeyListener(new KeyList1());
                                   matpanel       . TCatl   . addKeyListener(new KeyList2());
                                   matpanel       . TDraw   . addKeyListener(new KeyList3());
                                   tabreport      . ReportTable.addKeyListener(new KeyList4());
                    System.runFinalization();
                    System.gc();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setVectors()
     {
          ResultSet  result   = null;

          VHodCode            = new Vector();
          VHodName            = new Vector();

          VGroupCode          = new Vector();
          VGroupName          = new Vector();

          VStockGroupCode     = new Vector();
          VStockGroupName     = new Vector();

          VUOMCode            = new Vector();
          VUOMName            = new Vector();

          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();

               result = stat.executeQuery("Select HodCode,HodName From Hod Order By 2");

               while(result.next())
               {
                    VHodCode            . addElement(result.getString(1));
                    VHodName            . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery("Select GroupCode,GroupName From MatGroup Order By 2");
               while(result.next())
               {
                    VGroupCode          . addElement(result.getString(1));
                    VGroupName          . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery("Select GroupCode,GroupName From StockGroup Order By 2");
               while(result.next())
               {
                    VStockGroupCode     . addElement(result.getString(1));
                    VStockGroupName     . addElement(result.getString(2));
               }

               result    . close();

               result = stat.executeQuery("Select uomcode,uomName From Uom Order By 2");
               while(result.next())
               {
                    VUOMCode            . addElement(result.getString(1));
                    VUOMName            . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setVector()
     {
          Vector    theVector = new Vector();
          String[]  aRow      = null;
          ResultSet result    = null;

          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(getQString());
               int            inumColumns   = result.getMetaData().getColumnCount();

               while(result.next())
               {
                    aRow = new String[inumColumns];

                    for (int i = 0;i<inumColumns;i++)
                         aRow[i]   = common.parseNull(result.getString(i+1));

                    theVector      . addElement(aRow);
               }

               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          RowData = new Object[theVector.size()][ColumnData.length];

          for(int i=0;i<theVector.size();i++)
          {
               String[] sMaterial = (String[])theVector . elementAt(i);

               RowData[i][0]  = sMaterial[0];
               RowData[i][1]  = sMaterial[1];
               RowData[i][2]  = sMaterial[2];
               RowData[i][3]  = sMaterial[3];
               RowData[i][4]  = sMaterial[4];
               RowData[i][5]  = sMaterial[5];
               RowData[i][6]  = sMaterial[6];
               RowData[i][7]  = sMaterial[7];
               RowData[i][8]  = sMaterial[8];
               RowData[i][9]  = sMaterial[9];
               RowData[i][10] = sMaterial[10];
               RowData[i][11] = sMaterial[11];
               RowData[i][12] = sMaterial[12];
               RowData[i][13] = sMaterial[13];
               RowData[i][14] = sMaterial[14];
               RowData[i][15] = new Boolean(false);
          }
          theVector      . removeAllElements();
          theVector      = null;
     }

     public String getQString()
     {
          if(iMillCode == 0)
          {
               String QString =    " select invitems.ITEM_CODE,invitems.item_name,Uom.uomName,stockgroup.groupname,"+
                                   " invitems.draw,invitems.catl,invitems.opgqty,invitems.opgval,"+
                                   " invitems.roq,invitems.minqty,invitems.maxqty,"+
                                   " invitems.LocName,invitems.regular,matgroup.groupname,hod.hodname from invitems"+
                                   " inner join hod on hod.hodcode = invitems.hodcode"+
                                   " inner join UOM on UOM.UOMcode = invitems.UOMcode"+
                                   " inner join matgroup on matgroup.groupcode = invitems.matgroupcode"+
                                   " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode";
               String WS      = "";

               int iSig = 0;
               if (matpanel.JRSeleDept.isSelected())
               {
                    WS = (iSig==0 ?" Where ":" ");
                    iSig = 1;
                    WS = WS+"InvItems.StkGroupCode = '"+(String)matpanel.VDeptCode.elementAt(matpanel.JCDept.getSelectedIndex())+"'";
               }
               if(((matpanel.TSort.getText()).trim()).length()==0)
                    QString = QString+WS+" Order By 2";
               else
                    QString = QString+WS+" Order By "+matpanel.TSort.getText();

               return QString;
          }
          else
          {
               String QString =    " select invitems.ITEM_CODE,invitems.item_name,Uom.uomName,stockgroup.groupname,"+
                                   " invitems.draw,invitems.catl,"+SItemTable+".opgqty,"+SItemTable+".opgval,"+
                                   " "+SItemTable+".roq,"+SItemTable+".minqty,"+SItemTable+".maxqty,"+
                                   " "+SItemTable+".LocName,"+SItemTable+".regular,"+
                                   " matgroup.groupname,hod.hodname"+
                                   " from  "+SItemTable+""+
                                   " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                                   " left join hod on hod.hodcode = "+SItemTable+".hodcode"+
                                   " inner join UOM on UOM.UOMcode = invitems.UOMcode"+
                                   " left join matgroup on matgroup.groupcode = "+SItemTable+".matgroupcode"+
                                   " left join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode";

               String WS      = "";
               int iSig = 0;                  
               if (matpanel.JRSeleDept.isSelected())
               {
                    iSig = 1;
                    WS   = WS+" Where "+SItemTable+".StkGroupCode = '"+(String)matpanel.VDeptCode.elementAt(matpanel.JCDept.getSelectedIndex())+"'";
               }
               if(((matpanel.TSort.getText()).trim()).length()==0)
                    QString = QString+WS+" Order By 2";
               else
                    QString = QString+WS+" Order By "+matpanel.TSort.getText();


               return QString;
          }
     }

     private String getGroupCode(String str)
     {
          int  index     = -1;

          if((str.trim()).equals(""))
               return "0";

          if(VGroupName.size()>1)
          {
               index = VGroupName.indexOf(str);
               return (String)VGroupCode.elementAt(index);
          }
          return "0";
     }

     private String getUserCode(String str)
     {
          int  index     = -1;

          if((str.trim()).equals(""))
               return "0";

          if(VHodName.size()>0)
          {
               index     = VHodName.indexOf(str);
               return (String)VHodCode.elementAt(index);
          }
          return "0";
     }

     private String getStockGroupCode(String str)
     {
          int  index     = -1;

          if(VStockGroupName.size()>0)
          {
               index     = VStockGroupName.indexOf(str);
               return (String)VStockGroupCode.elementAt(index);
          }
          return "0";
     }

     private String getUomCode(String str)
     {
          int  index     = -1;

          if(VUOMName.size()>0)
          {
               index     = VUOMName.indexOf(str);
               return (String)VUOMCode.elementAt(index);
          }

          return "0";
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TFind.getText();
               int       i         = indexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TFind1.getText();
               int       i         = codeIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TCatl.getText();
               int       i         = catlIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList3 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TDraw.getText();
               int       i         = drawIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList4 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String SPColour  = "";
               String SPSet     = "";
               String SPSize    = "";
               String SPSide    = "";

               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    String    SStkGroupCode  = "";
                    int       i              = tabreport.ReportTable.getSelectedRow();

                    String    SItemCode = ((String)RowData[i][0]).trim();
                    String    SItemName = ((String)RowData[i][1]).trim();
                    String    QS        = "";
     
                    if(iMillCode==0)
                    {
                         QS =    " select Invitems.stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from invitems"+
                                 " where invitems.item_code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS =    " select "+SItemTable+".stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from "+SItemTable+""+
                                 " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                                 " where "+SItemTable+".item_code = '"+SItemCode+"'";
                    }

                    try
                    {
                         ORAConnection  oraConnection = ORAConnection.getORAConnection();
                         Connection     theConnection = oraConnection.getConnection();

                                        if(!theConnection.getAutoCommit())
                                             theConnection . setAutoCommit(true);

                         Statement      stat          = theConnection.createStatement();
                         ResultSet      result        = stat.executeQuery(QS);
                         result         . next();
                         SStkGroupCode  = (String)result.getString(1);
                         SPColour       = (String)result.getString(2);
                         SPSet          = (String)result.getString(3);
                         SPSize         = (String)result.getString(4);
                         SPSide         = (String)result.getString(5);
                         result         . close();
                         stat           . close();

                    }catch(Exception ex)
                    {
                         System.out.println(ex);
                    }

                    if(SStkGroupCode.equals("B01"))
                    {
                         StationaryFrame SF   =  new StationaryFrame(Layer,SItemCode,SItemName,SPColour,SPSet,SPSize,SPSide);
                         try
                         {
                              Layer     . add(SF);
                              Layer     . repaint();
                              SF        . setSelected(true);
                              Layer     . updateUI();
                              SF        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
               }
          }
     }

     public int codeIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][0];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int indexOf(String SFind)
     {
          SFind = SFind.toUpperCase();
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][1];
                         str       =              str       . toUpperCase();

               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int catlIndexOf(String SFind)
     {
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][5];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int drawIndexOf(String SFind)
     {
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][4];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public void updateMaterials()
     {
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
                              if(!theConnection.getAutoCommit())
                                   theConnection . setAutoCommit(true);
               Statement      stat          = theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][15];
                    if(Bselected.booleanValue())
                    {
                         if(iMillCode == 0)
                         {
                              String    QString = " Update InvItems Set ";
                                        QString = QString+" ITEM_NAME       = '"+((String)RowData[i][1]).toUpperCase()+"',";
                                        QString = QString+" UOMCODE         = '"+getUomCode(((String)RowData[i][2]).toUpperCase())+"',";
                                        QString = QString+" STKGROUPCODE    = '"+getStockGroupCode((String)RowData[i][3])+"',";
                                        QString = QString+" DRAW            = '"+((String)RowData[i][4]).toUpperCase()+"',";
                                        QString = QString+" CATL            = '"+((String)RowData[i][5]).toUpperCase()+"',";
                                        QString = QString+" OPGQTY          = 0"+(String)RowData[i][6]+",";
                                        QString = QString+" OPGVAL          = 0"+(String)RowData[i][7]+",";
                                        QString = QString+" ROQ             = 0"+(String)RowData[i][8]+",";
                                        QString = QString+" MINQTY          = 0"+(String)RowData[i][9]+",";
                                        QString = QString+" MAXQTY          = 0"+(String)RowData[i][10]+",";
                                        QString = QString+" LOCNAME         = '"+((String)RowData[i][11]).toUpperCase()+"', ";
                                        QString = QString+" REGULAR         = 0"+common.toInt(((String)RowData[i][12]))+", ";
                                        QString = QString+" MATGROUPCODE    = 0"+getGroupCode((String)RowData[i][13])+", ";
                                        QString = QString+" CREATIONDATE    = '"+common.getServerDate()+"',";
                                        QString = QString+" USERCODE        = "+iUserCode+",";
                                        QString = QString+" HODCODE         = 0"+getUserCode((String)RowData[i][14])+" ";
                                        QString = QString+" Where ITEM_CODE = '"+(String)RowData[i][0]+"'";

                              stat.execute(QString);
                         }
                         else
                         {
                              String    QString = "Update InvItems Set ";
                                        QString = QString+" ITEM_NAME           = '"+((String)RowData[i][1]).toUpperCase()+"',";
                                        QString = QString+" UOMCODE             = '"+getUomCode(((String)RowData[i][2]).toUpperCase())+"',";
                                        QString = QString+" DRAW                = '"+((String)RowData[i][4]).toUpperCase()+"',";
                                        QString = QString+" CREATIONDATE        = '"+common.getServerDate()+"',";
                                        QString = QString+" USERCODE            = "+iUserCode+",";
                                        QString = QString+" CATL                = '"+((String)RowData[i][5]).toUpperCase()+"' ";
                                        QString = QString+" Where ITEM_CODE     = '"+(String)RowData[i][0]+"'";

                              stat . execute(QString);
                              
                              String    QString1 = "Update "+SItemTable+" Set ";
                                        QString1 = QString1+" OPGQTY              = 0"+(String)RowData[i][6]+",";
                                        QString1 = QString1+" OPGVAL              = 0"+(String)RowData[i][7]+",";
                                        QString1 = QString1+" ROQ                 = 0"+(String)RowData[i][8]+",";
                                        QString1 = QString1+" REGULAR             = 0"+common.toInt(((String)RowData[i][12]))+", ";
                                        QString1 = QString1+" STKGROUPCODE        = '"+getStockGroupCode((String)RowData[i][3])+"',";
                                        QString1 = QString1+" MINQTY              = 0"+(String)RowData[i][9]+",";
                                        QString1 = QString1+" MAXQTY              = 0"+(String)RowData[i][10]+",";
                                        QString1 = QString1+" LOCNAME             = '"+((String)RowData[i][11]).toUpperCase()+"', ";
                                        QString1 = QString1+" HODCODE             = 0"+getUserCode((String)RowData[i][14])+",";
                                        QString1 = QString1+" CREATIONDATE        = '"+common.getServerDate()+"',";
                                        QString1 = QString1+" USERCODE            = "+iUserCode+",";
                                        QString1 = QString1+" MATGROUPCODE        = 0"+getGroupCode((String)RowData[i][13])+" ";
                                        QString1 = QString1+" Where ITEM_CODE     = '"+(String)RowData[i][0]+"'";

                              stat . execute(QString1);
                         }
                    }
               }
               stat           . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     protected void finalize() throws Throwable
     {
          RowData             = null;
          ColumnData          = null;
          ColumnType          = null;
          this.Layer          = null;

          JTP                 = null;
          ListPanel           = null;
          CreatePanel         = null;
          BOk                 = null;
          matpanel            = null;
          tabreport           = null;
          SFind               = null;

          VHodCode            . removeAllElements();
          VHodCode            = null;
          VHodName            . removeAllElements();
          VHodName            = null;
          VGroupCode          . removeAllElements();
          VGroupCode          = null;
          VGroupName          . removeAllElements();
          VGroupName          = null;
          VStockGroupCode     . removeAllElements();
          VStockGroupCode     = null;
          VStockGroupName     . removeAllElements();
          VStockGroupName     = null;
          VUOMCode            . removeAllElements();
          VUOMCode            = null;
          VUOMName            . removeAllElements();
          VUOMName            = null;

          super               . finalize();

         // System.out.println("finalizeMaterialFrame");
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }
}

