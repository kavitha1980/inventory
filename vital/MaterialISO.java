package vital;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MaterialISO extends JInternalFrame
{
      JLayeredPane  Layer;
      Object        RowData[][];

      String        ColumnData[] = {"Code","Item Name","UoM","Stock Group","Drawing","Catl","Iso Rating","Click for Modify"};
      String        ColumnType[] = {"S"   ,"S"        ,"S"  ,"S"          ,"S"      ,"S"   , "E"        , "B"              };

      Vector        VMCode,VMName,VMDept,VMDraw,VMCatl,VMUoM,VIsoRating;

      JTabbedPane   JTP;
      JPanel        ListPanel;
      JButton       BOk;
      Common        common = new Common();
      FindPanel     findPanel;
      TabReport     tabreport;
      String        SFind="";
      int           iMillCode,iAuthCode;
      String        SItemTable;

      Connection theConnection = null;

      public MaterialISO(JLayeredPane Layer,int iMillCode,int iAuthCode,String SItemTable)
      {
            this.Layer        = Layer;
            this.iMillCode    = iMillCode;
            this.iAuthCode    = iAuthCode;
            this.SItemTable   = SItemTable;

            findPanel         = new FindPanel();
            JTP               = new JTabbedPane();
            ListPanel         = new JPanel(true);
            BOk               = new JButton("Update");
            JTP.addTab("List of Materials",ListPanel);
            setLayouts();
      }
         
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
            setTitle("Material For Iso Rating");

            ListPanel.setLayout(new BorderLayout());
            ListPanel.add("North",findPanel);

            if(iAuthCode>1)
            {
               ListPanel.add("South",BOk);
            }

            findPanel.BApply.addActionListener(new ActList());
            findPanel.TFind.addKeyListener(new KeyList());
            findPanel.TFind1.addKeyListener(new KeyList1());

            BOk.addActionListener(new UpdtList());

            getContentPane().add("Center",JTP);
      }

      public class ActList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  findPanel.TFind.setEditable(true);
                  findPanel.TFind1.setEditable(true);
                  BOk.setEnabled(true);

                  setVector();
                  setRowData();
                  try
                  {
                       ListPanel.remove(tabreport); 
                  }
                  catch(Exception ex)
                  {
                  }
                  try
                  {
                     tabreport = new TabReport(RowData,ColumnData,ColumnType);
                     ListPanel.add(tabreport,BorderLayout.CENTER);
                     Layer.repaint();
                     Layer.updateUI();
                  }
                  catch(Exception ex)
                  {
                     System.out.println("Err2"+ex);
                  }
            }
      }

      public class UpdtList implements ActionListener
      {
         public void actionPerformed(ActionEvent ae)
         {
             BOk.setEnabled(false);
             updateMaterials();
             removeHelpFrame();
         }
      }

      public class KeyList extends KeyAdapter
      {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = findPanel.TFind.getText();
               int i=indexOf(SFind);
               if (i>-1)
               {
                    tabreport.ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                    tabreport.ReportTable.scrollRectToVisible(cellRect);
               }
          }
      }

      public class KeyList1 extends KeyAdapter
      {
           public void keyReleased(KeyEvent ke)
           {
                   String SFind = findPanel.TFind1.getText();
                   int i=codeIndexOf(SFind);
                   if (i>-1)
                   {
                           tabreport.ReportTable.setRowSelectionInterval(i,i);
                           Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                           tabreport.ReportTable.scrollRectToVisible(cellRect);
                   }
           }
      }

      public int indexOf(String SFind)
      {
           SFind = SFind.toUpperCase();
           for(int i=0;i<VMName.size();i++)
           {
                   String str = (String)VMName.elementAt(i);
                   str=str.toUpperCase();
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int codeIndexOf(String SFind)
      {
           for(int i=0;i<VMCode.size();i++)
           {
                   String str = (String)VMCode.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int catlIndexOf(String SFind)
      {
           for(int i=0;i<VMCatl.size();i++)
           {
                   String str = (String)VMCatl.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public int drawIndexOf(String SFind)
      {
           for(int i=0;i<VMDraw.size();i++)
           {
                   String str = (String)VMDraw.elementAt(i);
                   if(str.startsWith(SFind))
                           return i;
           }
           return -1;
      }

      public void setVector()
      {
            VMCode       = new Vector();
            VMName       = new Vector();
            VMDept       = new Vector();
            VMDraw       = new Vector();
            VMCatl       = new Vector();
            VMUoM        = new Vector();
            VIsoRating   = new Vector();

            try
            {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
                  ResultSet res  = theStatement.executeQuery(getQString());
                  while(res.next())
                  {
                        VMCode     .addElement(res.getString(1));
                        VMName     .addElement(res.getString(2));
                        VMDept     .addElement(res.getString(3));
                        VMDraw     .addElement(common.parseNull(res.getString(4)));
                        VMCatl     .addElement(common.parseNull(res.getString(5)));
                        VMUoM      .addElement(res.getString(6));
                        VIsoRating .addElement(common.parseNull(res.getString(7)));
                  }
                  res.close();
                  theStatement.close();
            }
            catch(Exception ex)
            {
                  System.out.println("Err3"+ex);
            }
      }

      public void setRowData()
      {
            RowData = new Object[VMCode.size()][ColumnData.length];
            for(int i=0;i<VMCode.size();i++)
            {
                        RowData[i][0] = common.parseNull((String)VMCode.elementAt(i));      
                        RowData[i][1] = common.parseNull((String)VMName.elementAt(i));
                        RowData[i][2] = common.parseNull((String)VMUoM.elementAt(i));
                        RowData[i][3] = common.parseNull((String)VMDept.elementAt(i));
                        RowData[i][4] = common.parseNull((String)VMDraw.elementAt(i));
                        RowData[i][5] = common.parseNull((String)VMCatl.elementAt(i));

                        if(common.toInt((String)VIsoRating.elementAt(i)) == 1)
                             RowData[i][6] = common.parseNull((String)VIsoRating.elementAt(i));
                        RowData[i][7] = new Boolean(false);
            }
      }
      public void updateMaterials()
      {
           try
           {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();

                for(int i=0;i<VMCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][7];
                     int iStatus       = common.toInt((String)RowData[i][6]);

                     if(Bselected.booleanValue())
                     {

                         String QString = " Update "+SItemTable+" Set ";
                         QString = QString+" ISoRating = "+iStatus;
                         QString = QString+" Where Item_Code = '"+(String)RowData[i][0]+"'";
                         theStatement.execute(QString);
                    }
                }
                theStatement.close();
           }
           catch(Exception ex)
           {
                 System.out.println("Err4"+ ex);
           }
      }

      public String getQString()
      {
          if(iMillCode == 0)
          {
               String QString = " Select InvItems.Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
                                " UoM.UomName,InvItems.IsoRating"+
                                " From (InvItems Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode) "+
                                " Inner Join Uom On Uom.UomCode = InvItems.UomCode ";
     
               String WS      = "";
               int iSig = 0;                  
               if (findPanel.JRSeleDept.isSelected())
               {
                     WS = (iSig==0 ?"Where ":" ");
                     iSig = 1;
                     WS = WS+"InvItems.StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
               }
               if(((findPanel.TSort.getText()).trim()).length()==0)
                       QString = QString+WS+" Order By 2";
               else
                       QString = QString+WS+" Order By "+findPanel.TSort.getText();
               return QString;
          }

          else
          {
               String QString = " Select "+SItemTable+".Item_Code,InvItems.Item_Name,"+
                                " StockGroup.GroupName,InvItems.Draw,"+
                                " InvItems.Catl,"+
                                " UoM.UomName,"+SItemTable+".IsoRating"+
                                " From (("+SItemTable+" Inner Join InvItems On "+
                                " "+SItemTable+".Item_Code = InvItems.Item_Code) "+
                                " Left Join StockGroup On StockGroup.GroupCode = "+SItemTable+".StkGroupCode) "+
                                " Inner Join Uom On Uom.UomCode = InvItems.UomCode ";
     

               String WS      = "";
               int iSig = 0;                  
               if (findPanel.JRSeleDept.isSelected())
               {
                     WS = (iSig==0 ?"Where ":" ");
                     iSig = 1;
                     WS = WS+""+SItemTable+".StkGroupCode = '"+(String)findPanel.VDeptCode.elementAt(findPanel.JCDept.getSelectedIndex())+"'";
               }

               if(((findPanel.TSort.getText()).trim()).length()==0)
                       QString = QString+WS+" Order By 2";
               else
                       QString = QString+WS+" Order By "+findPanel.TSort.getText();

               return QString;
          }
      }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println("Err5"+ex);
          }
     }
}
