package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialModiFrame extends JInternalFrame
{
     JLayeredPane        Layer;

     Object              RowData[][];
     String              ColumnData[] = {"Code","Item Name","Short Name","UoM","Stock Group","Drawing","Catl","PartNo","Industry Sector","Sub Sector","Department","MacModel","MatGroup","Brand","ST Group","InnerDia","OuterDia","Length","Thickness","Width","Viscosity","Height","Temperature","ChapterNo","Commodity","Opg Qty","Opg Val","ROQ","Min Qty","Max Qty","Location","Regular","Hod","RoomNo","RackNo","RowNo","ColumnNo","TrayNo","BoxNo","Obsolete","AgingPeriod","Check Freq","QualityCheck","Sales if not used","CostingType","RateRevision","Click for Modify"};
     String              ColumnType[] = {"S"   ,"B"        ,"B"         ,"B"  ,"E"          ,"E"      ,"E"   ,"E"     ,"S"              ,"S"         ,"E"         ,"E"       ,"E"       ,"E"    ,"E"       ,"E"       ,"E"       ,"E"     ,"E"        ,"E"    ,"E"        ,"E"     ,"E"          ,"E"        ,"E"        ,"S"      ,"S"      ,"E"  ,"E"      ,"E"      ,"E"       ,"B"      ,"E"  ,"B"     ,"B"     ,"B"    ,"B"       ,"B"     ,"B"    ,"B"       ,"B"          ,"B"         ,"B"           ,"B"                ,"B"          ,"B"           ,"B"               };

     private Vector              VHodCode,VHodName,VGroupCode,VGroupName,VStockGroupCode,VStockGroupName;
     private Vector              VUOMCode,VUOMName;
     private Vector              VDeptCode,VDeptName,VModelCode,VModelName,VBrandCode,VBrandName,VSTGroupCode,VSTGroupName;
     private Vector              VTempCode,VTempName,VRoomCode,VRoomName,VRackCode,VRackName,VRowCode,VRowName,VColumnCode,VColumnName;
     private Vector              VTrayCode,VTrayName,VBoxCode,VBoxName,VObsCode,VObsName,VQCheckCode,VQCheckName;
     private Vector              VSalesCode,VSalesName,VCostCode,VCostName,VRateCode,VRateName;

     private JTabbedPane         JTP;
     private JPanel              ListPanel,BottomPanel;
     private JButton             BOk;
     private Common              common;
     private MatPanel            matpanel;
     private TabReportNew        tabreport;
     private String              SFind     = "";

     Runtime                     rt        = null;

     private int                 iMillCode,iAuthCode,iUserCode;
     private String              SItemTable;

     ArrayList theVector = null;
     String[]  aRow      = null;

     public MaterialModiFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable)
     {
          this.Layer          = Layer;
          this.iMillCode      = iMillCode;
          this.iAuthCode      = iAuthCode;
          this.iUserCode      = iUserCode;
          this.SItemTable     = SItemTable;

          initialize();

               //long heapsize = Runtime.getRuntime().totalMemory();
               //System.out.println("StartHeapsize:"+heapsize);
               //System.out.println("StartTotal:"+Runtime.getRuntime().totalMemory());
               //System.out.println("StartFree:"+Runtime.getRuntime().freeMemory());

               /*try
               {
               theVector      . clear();
               theVector      = null;
               aRow           = null;
               }
               catch(Exception ex){}*/


               Runtime rt = Runtime.getRuntime();
               System.out.println("TotalMem:"+rt.totalMemory());
               System.out.println("InitFreeMem:"+rt.freeMemory());
               rt.gc();
               System.out.println("AfterGC:"+rt.freeMemory());



          common              = new Common();
          matpanel            = new MatPanel(iMillCode);
          JTP                 = new JTabbedPane();
          ListPanel           = new JPanel(true);
          BottomPanel         = new JPanel(true);

          BOk                 = new JButton("Update");

          JTP                 . addTab("List of Materials",ListPanel);

          matpanel  . TFind.setEditable(true);
          matpanel  . TFind1.setEditable(true);
          matpanel  . TCatl.setEditable(true);
          matpanel  . TDraw.setEditable(true);

          rt = Runtime.getRuntime();
          setVectors();
          setLayouts();
     }
         
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Materials");

          ListPanel . setLayout(new BorderLayout());
          ListPanel . add("North",matpanel);

          BottomPanel.add(BOk);
          BottomPanel.add(new JLabel(""));
          BottomPanel.add(new JLabel("F3 - To Change Sector & Photo"));

          if(iAuthCode>1)
          {
               //ListPanel.add("South",BOk);
               ListPanel.add("South",BottomPanel);
          }
             
          matpanel            . BApply.addActionListener(new ActList(this));
          BOk                 . addActionListener(new UpdtList());
          getContentPane()    . add("Center",JTP);
     }

     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               updateMaterials();
               System.runFinalization();
               System.gc();
               removeHelpFrame();
          }
     }

     public class ActList implements ActionListener
     {
          MaterialModiFrame MMF;

          public ActList(MaterialModiFrame MMF)
          {
               this.MMF = MMF;
          }

          public void actionPerformed(ActionEvent ae)
          {
               clearObjects();
               try
               {
               //theVector      . clear();
               //theVector      = null;
               aRow      = null;
               }
               catch(Exception ex){}

               System.runFinalization();
               System.gc();

               long heapsize1 = Runtime.getRuntime().totalMemory();
               System.out.println("Heapsize1:"+heapsize1+":"+Runtime.getRuntime().freeMemory());

               BOk       . setEnabled(true);
               setVector();

               try
               {
                    ListPanel . remove(tabreport);
               }
               catch(Exception ex)
               {
               }
               try
               {
                                   tabreport      = new TabReportNew(RowData,ColumnData,ColumnType);
                                   tabreport      . ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                                   ListPanel      . add(tabreport,BorderLayout.CENTER);



                    setSelected(true);

                                   Layer          . repaint();
                                   Layer          . updateUI();
                                                  
                    TableColumn    UomColumn      = tabreport.ReportTable.getColumn("UoM");
                    TableColumn    HodColumn      = tabreport.ReportTable.getColumn("Hod");
                    TableColumn    GroupColumn    = tabreport.ReportTable.getColumn("MatGroup");
                    TableColumn    StockColumn    = tabreport.ReportTable.getColumn("Stock Group");
                    TableColumn    DeptColumn     = tabreport.ReportTable.getColumn("Department");
                    TableColumn    MacColumn      = tabreport.ReportTable.getColumn("MacModel");
                    TableColumn    BrandColumn    = tabreport.ReportTable.getColumn("Brand");
                    TableColumn    STGroupColumn  = tabreport.ReportTable.getColumn("ST Group");
                    TableColumn    TempColumn     = tabreport.ReportTable.getColumn("Temperature");
                    TableColumn    RoomColumn     = tabreport.ReportTable.getColumn("RoomNo");
                    TableColumn    RackColumn     = tabreport.ReportTable.getColumn("RackNo");
                    TableColumn    RowColumn      = tabreport.ReportTable.getColumn("RowNo");
                    TableColumn    ColColumn      = tabreport.ReportTable.getColumn("ColumnNo");
                    TableColumn    TrayColumn     = tabreport.ReportTable.getColumn("TrayNo");
                    TableColumn    BoxColumn      = tabreport.ReportTable.getColumn("BoxNo");
                    TableColumn    ObsoleteColumn = tabreport.ReportTable.getColumn("Obsolete");
                    TableColumn    QualityColumn  = tabreport.ReportTable.getColumn("QualityCheck");
                    TableColumn    SalesColumn    = tabreport.ReportTable.getColumn("Sales if not used");
                    TableColumn    CostColumn     = tabreport.ReportTable.getColumn("CostingType");
                    TableColumn    RateColumn     = tabreport.ReportTable.getColumn("RateRevision");

                                   UomColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VUOMName)));
                                   HodColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VHodName)));
                                   GroupColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VGroupName)));
                                   StockColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VStockGroupName)));
                                   DeptColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VDeptName)));
                                   MacColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VModelName)));
                                   BrandColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VBrandName)));
                                   STGroupColumn  . setCellEditor(new DefaultCellEditor(new JComboBox(VSTGroupName)));
                                   TempColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VTempName)));
                                   RoomColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VRoomName)));
                                   RackColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VRackName)));
                                   RowColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VRowName)));
                                   ColColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VColumnName)));
                                   TrayColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VTrayName)));
                                   BoxColumn      . setCellEditor(new DefaultCellEditor(new JComboBox(VBoxName)));
                                   ObsoleteColumn . setCellEditor(new DefaultCellEditor(new JComboBox(VObsName)));
                                   QualityColumn  . setCellEditor(new DefaultCellEditor(new JComboBox(VQCheckName)));
                                   SalesColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VSalesName)));
                                   CostColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VCostName)));
                                   RateColumn     . setCellEditor(new DefaultCellEditor(new JComboBox(VRateName)));

                                   matpanel       . TFind   . addKeyListener(new KeyList());
                                   matpanel       . TFind1  . addKeyListener(new KeyList1());
                                   matpanel       . TCatl   . addKeyListener(new KeyList2());
                                   matpanel       . TDraw   . addKeyListener(new KeyList3());
                                   tabreport      . ReportTable.addKeyListener(new KeyList4(MMF));
                    System.runFinalization();
                    System.gc();
               long heapsize2 = Runtime.getRuntime().totalMemory();
               System.out.println("Heapsize2:"+heapsize2+":"+Runtime.getRuntime().freeMemory());

               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     public void setVectors()
     {
          ResultSet  result   = null;

          VHodCode            = new Vector();
          VHodName            = new Vector();

          VGroupCode          = new Vector();
          VGroupName          = new Vector();

          VStockGroupCode     = new Vector();
          VStockGroupName     = new Vector();

          VUOMCode            = new Vector();
          VUOMName            = new Vector();

          VDeptCode           = new Vector();
          VDeptName           = new Vector();

          VModelCode          = new Vector();
          VModelName          = new Vector();

          VBrandCode          = new Vector();
          VBrandName          = new Vector();

          VSTGroupCode        = new Vector();
          VSTGroupName        = new Vector();

          VTempCode           = new Vector();
          VTempName           = new Vector();

          VRoomCode           = new Vector();
          VRoomName           = new Vector();

          VRackCode           = new Vector();
          VRackName           = new Vector();

          VRowCode            = new Vector();
          VRowName            = new Vector();

          VColumnCode         = new Vector();
          VColumnName         = new Vector();

          VTrayCode           = new Vector();
          VTrayName           = new Vector();

          VBoxCode            = new Vector();
          VBoxName            = new Vector();

          VObsCode            = new Vector();
          VObsName            = new Vector();

          VQCheckCode         = new Vector();
          VQCheckName         = new Vector();

          VSalesCode          = new Vector();
          VSalesName          = new Vector();

          VCostCode           = new Vector();
          VCostName           = new Vector();

          VRateCode           = new Vector();
          VRateName           = new Vector();


          VObsCode.addElement("0");
          VObsCode.addElement("1");
          VObsName.addElement("No");
          VObsName.addElement("Yes");

          VSalesCode.addElement("0");
          VSalesCode.addElement("1");
          VSalesName.addElement("No");
          VSalesName.addElement("Yes");


          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();

               result = stat.executeQuery("Select HodCode,HodName From Hod Order By 2");

               while(result.next())
               {
                    VHodCode            . addElement(result.getString(1));
                    VHodName            . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery("Select GroupCode,GroupName From MatGroup Order By 2");
               while(result.next())
               {
                    VGroupCode          . addElement(result.getString(1));
                    VGroupName          . addElement(result.getString(2));
               }
               result.close();

               result = stat.executeQuery("Select GroupCode,GroupName From StockGroup Order By 2");
               while(result.next())
               {
                    VStockGroupCode     . addElement(result.getString(1));
                    VStockGroupName     . addElement(result.getString(2));
               }

               result    . close();

               result = stat.executeQuery("Select uomcode,uomName From Uom Order By 2");
               while(result.next())
               {
                    VUOMCode            . addElement(result.getString(1));
                    VUOMName            . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Dept_Code,Dept_Name From Dept Where MillCode=2 or MillCode="+iMillCode+" Order By Dept_Name");
               while(result.next())
               {
                    VDeptCode           . addElement(result.getString(1));
                    VDeptName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select ModelCode,ModelName From scm.SqcModelMaster Order By ModelName");
               while(result.next())
               {
                    VModelCode          . addElement(result.getString(1));
                    VModelName          . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From Brand Order By Name");
               while(result.next())
               {
                    VBrandCode          . addElement(result.getString(1));
                    VBrandName          . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From STGroup Order By Name");
               while(result.next())
               {
                    VSTGroupCode        . addElement(result.getString(1));
                    VSTGroupName        . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From Temperature Order By Name");
               while(result.next())
               {
                    VTempCode           . addElement(result.getString(1));
                    VTempName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatRoom Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VRoomCode           . addElement(result.getString(1));
                    VRoomName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatRack Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VRackCode           . addElement(result.getString(1));
                    VRackName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatRow Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VRowCode            . addElement(result.getString(1));
                    VRowName            . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatColumn Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VColumnCode         . addElement(result.getString(1));
                    VColumnName         . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatTray Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VTrayCode           . addElement(result.getString(1));
                    VTrayName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From MatBox Where MillCode=2 or MillCode="+iMillCode+" Order By Name");
               while(result.next())
               {
                    VBoxCode            . addElement(result.getString(1));
                    VBoxName            . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From QualityPeriodical Order By Name");
               while(result.next())
               {
                    VQCheckCode         . addElement(result.getString(1));
                    VQCheckName         . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From CostingType Order By Name");
               while(result.next())
               {
                    VCostCode           . addElement(result.getString(1));
                    VCostName           . addElement(result.getString(2));
               }
               result    . close();

               result = stat.executeQuery("Select Code,Name From RatePeriodical Order By Name");
               while(result.next())
               {
                    VRateCode           . addElement(result.getString(1));
                    VRateName           . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     public void setVector()
     {
          theVector           = new ArrayList();
          aRow                = null;
          ResultSet result    = null;

          try
          {
               long heapsize1a = Runtime.getRuntime().totalMemory();
               System.out.println("Heapsize1a:"+heapsize1a+":"+Runtime.getRuntime().freeMemory());

               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();
                              result        = stat.executeQuery(getQString());
               int            inumColumns   = result.getMetaData().getColumnCount();

               while(result.next())
               {
                    aRow = new String[inumColumns];

                    for (int i = 0;i<inumColumns;i++)
                         aRow[i]   = common.parseNull(result.getString(i+1));

                    //System.runFinalization();
                    //System.gc();

                    theVector      . add(aRow);
               }

                    Runtime RT = Runtime.getRuntime();
                    RT.gc();

               long heapsize1b = Runtime.getRuntime().totalMemory();
               System.out.println("Heapsize1b:"+heapsize1b+":"+Runtime.getRuntime().freeMemory());


               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

          RowData = new Object[theVector.size()][ColumnData.length];

          for(int i=0;i<theVector.size();i++)
          {
               String[] sMaterial = (String[])theVector . get(i);

               RowData[i][0]  = sMaterial[0];
               RowData[i][1]  = sMaterial[1];
               RowData[i][2]  = sMaterial[2];
               RowData[i][3]  = sMaterial[3];
               RowData[i][4]  = sMaterial[4];
               RowData[i][5]  = sMaterial[5];
               RowData[i][6]  = sMaterial[6];
               RowData[i][7]  = sMaterial[7];
               RowData[i][8]  = sMaterial[8];
               RowData[i][9]  = sMaterial[9];
               RowData[i][10] = sMaterial[10];
               RowData[i][11] = sMaterial[11];
               RowData[i][12] = sMaterial[12];
               RowData[i][13] = sMaterial[13];
               RowData[i][14] = sMaterial[14];
               RowData[i][15] = sMaterial[15];
               RowData[i][16] = sMaterial[16];
               RowData[i][17] = sMaterial[17];
               RowData[i][18] = sMaterial[18];
               RowData[i][19] = sMaterial[19];
               RowData[i][20] = sMaterial[20];
               RowData[i][21] = sMaterial[21];
               RowData[i][22] = sMaterial[22];
               RowData[i][23] = sMaterial[23];
               RowData[i][24] = sMaterial[24];
               RowData[i][25] = sMaterial[25];
               RowData[i][26] = sMaterial[26];
               RowData[i][27] = sMaterial[27];
               RowData[i][28] = sMaterial[28];
               RowData[i][29] = sMaterial[29];
               RowData[i][30] = sMaterial[30];
               RowData[i][31] = sMaterial[31];
               RowData[i][32] = sMaterial[32];
               RowData[i][33] = sMaterial[33];
               RowData[i][34] = sMaterial[34];
               RowData[i][35] = sMaterial[35];
               RowData[i][36] = sMaterial[36];
               RowData[i][37] = sMaterial[37];
               RowData[i][38] = sMaterial[38];
               RowData[i][39] = sMaterial[39];
               RowData[i][40] = sMaterial[40];
               RowData[i][41] = sMaterial[41];
               RowData[i][42] = sMaterial[42];
               RowData[i][43] = sMaterial[43];
               RowData[i][44] = sMaterial[44];
               RowData[i][45] = sMaterial[45];
               RowData[i][46] = new Boolean(false);
          }
     }

     public String getQString()
     {
          if(iMillCode == 0)
          {
               String QString =    " select invitems.ITEM_CODE,invitems.item_name,invitems.ShortName,Uom.uomName,stockgroup.groupname as StockGroup,"+
                                   " invitems.draw,invitems.catl,invitems.PartNumber,IndustrySector.Name as Sector,"+
                                   " IndustrySubSector.Name as SubSector,Dept.Dept_Name,SqcModelMaster.ModelName,MatGroup.GroupName as MatGroup,"+
                                   " Brand.Name as Brand,STGroup.Name as STGroup,InvItems.InnerDia,InvItems.OuterDia,"+
                                   " InvItems.MatLength,InvItems.Thickness,InvItems.Width,InvItems.Viscosity,InvItems.Height,"+
                                   " Temperature.Name as Temperature,InvItems.ChapterNo,InvItems.CommodityCode,invitems.opgqty,invitems.opgval,"+
                                   " invitems.roq,invitems.minqty,invitems.maxqty,invitems.LocName,invitems.regular,hod.hodname,"+
                                   " MatRoom.Name as Room,MatRack.Name as Rack,MatRow.Name as RowNo,MatColumn.Name as ColumnNo,"+
                                   " MatTray.Name as TrayNo,MatBox.Name as BoxNo,decode(InvItems.Obsolete,0,'No','Yes') as obsolete,InvItems.AgingPeriod,"+
                                   " InvItems.CheckFreq,QualityPeriodical.Name as QualityCheck,decode(InvItems.SalesStatus,0,'No','Yes') as Sales,"+
                                   " CostingType.Name as CostingType,RatePeriodical.Name as RateRevision "+
                                   " from invitems"+
                                   " inner join hod on hod.hodcode = invitems.hodcode"+
                                   " inner join UOM on UOM.UOMcode = invitems.UOMcode"+
                                   " inner join IndustrySector on invitems.sectorcode = industrysector.code "+
                                   " inner join IndustrySubSector on invitems.subsectorcode = industrysubsector.code "+
                                   " inner join Dept on invitems.deptcode = dept.dept_code "+
                                   " inner join scm.SqcModelMaster on invitems.macmodelcode = scm.sqcmodelmaster.modelcode "+
                                   " inner join Brand on invitems.brandcode = Brand.code "+
                                   " inner join STGroup on invitems.STGroup = STGroup.code "+
                                   " inner join Temperature on invitems.tempcode = temperature.code "+
                                   " inner join MatRoom on invitems.roomcode = matroom.code "+
                                   " inner join MatRack on invitems.rackcode = matrack.code "+
                                   " inner join MatRow on invitems.rowcode = matrow.code "+
                                   " inner join MatColumn on invitems.columncode = matcolumn.code "+
                                   " inner join MatTray on invitems.traycode = mattray.code "+
                                   " inner join MatBox on invitems.boxcode = matbox.code "+
                                   " inner join QualityPeriodical on invitems.qualitycheck = qualityperiodical.code "+
                                   " inner join CostingType on invitems.costingtype = costingtype.code "+
                                   " inner join RatePeriodical on invitems.raterevision = rateperiodical.code "+
                                   " inner join matgroup on matgroup.groupcode = invitems.matgroupcode"+
                                   " inner join stockgroup on stockgroup.groupcode = invitems.stkgroupcode";
               String WS      = "";

               int iSig = 0;
               if (matpanel.JRSeleDept.isSelected())
               {
                    WS = (iSig==0 ?" Where ":" ");
                    iSig = 1;
                    WS = WS+"InvItems.StkGroupCode = '"+(String)matpanel.VDeptCode.elementAt(matpanel.JCDept.getSelectedIndex())+"'";
               }
               if(((matpanel.TSort.getText()).trim()).length()==0)
                    QString = QString+WS+" Order By 2";
               else
                    QString = QString+WS+" Order By "+matpanel.TSort.getText();

               return QString;
          }
          else
          {
               String QString =    " select invitems.ITEM_CODE,invitems.item_name,invitems.ShortName,Uom.uomName,stockgroup.groupname as StockGroup,"+
                                   " invitems.draw,invitems.catl,invitems.PartNumber,IndustrySector.Name as Sector,"+
                                   " IndustrySubSector.Name as SubSector,Dept.Dept_Name,SqcModelMaster.ModelName,MatGroup.GroupName as MatGroup,"+
                                   " Brand.Name as Brand,STGroup.Name as STGroup,InvItems.InnerDia,InvItems.OuterDia,"+
                                   " InvItems.MatLength,InvItems.Thickness,InvItems.Width,InvItems.Viscosity,InvItems.Height,"+
                                   " Temperature.Name as Temperature,InvItems.ChapterNo,InvItems.CommodityCode,"+SItemTable+".opgqty,"+SItemTable+".opgval,"+
                                   " "+SItemTable+".roq,"+SItemTable+".minqty,"+SItemTable+".maxqty,"+
                                   " "+SItemTable+".LocName,"+SItemTable+".regular,hod.hodname,"+
                                   " MatRoom.Name as Room,MatRack.Name as Rack,MatRow.Name as RowNo,MatColumn.Name as ColumnNo,"+
                                   " MatTray.Name as TrayNo,MatBox.Name as BoxNo,decode("+SItemTable+".Obsolete,0,'No','Yes') as obsolete,"+SItemTable+".AgingPeriod,"+
                                   " "+SItemTable+".CheckFreq,QualityPeriodical.Name as QualityCheck,decode("+SItemTable+".SalesStatus,0,'No','Yes') as Sales,"+
                                   " CostingType.Name as CostingType,RatePeriodical.Name as RateRevision "+
                                   " from  "+SItemTable+""+
                                   " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                                   " left join hod on hod.hodcode = "+SItemTable+".hodcode"+
                                   " inner join UOM on UOM.UOMcode = invitems.UOMcode"+
                                   " inner join IndustrySector on invitems.sectorcode = industrysector.code "+
                                   " inner join IndustrySubSector on invitems.subsectorcode = industrysubsector.code "+
                                   " inner join Dept on invitems.deptcode = dept.dept_code "+
                                   " inner join scm.SqcModelMaster on invitems.macmodelcode = scm.sqcmodelmaster.modelcode "+
                                   " inner join Brand on invitems.brandcode = Brand.code "+
                                   " inner join STGroup on invitems.STGroup = STGroup.code "+
                                   " inner join Temperature on invitems.tempcode = temperature.code "+
                                   " inner join MatRoom on "+SItemTable+".roomcode = matroom.code "+
                                   " inner join MatRack on "+SItemTable+".rackcode = matrack.code "+
                                   " inner join MatRow on "+SItemTable+".rowcode = matrow.code "+
                                   " inner join MatColumn on "+SItemTable+".columncode = matcolumn.code "+
                                   " inner join MatTray on "+SItemTable+".traycode = mattray.code "+
                                   " inner join MatBox on "+SItemTable+".boxcode = matbox.code "+
                                   " inner join QualityPeriodical on "+SItemTable+".qualitycheck = qualityperiodical.code "+
                                   " inner join CostingType on "+SItemTable+".costingtype = costingtype.code "+
                                   " inner join RatePeriodical on invitems.raterevision = rateperiodical.code "+
                                   " left join matgroup on matgroup.groupcode = "+SItemTable+".matgroupcode"+
                                   " left join stockgroup on stockgroup.groupcode = "+SItemTable+".stkgroupcode";


               String WS      = "";
               int iSig = 0;                  
               if (matpanel.JRSeleDept.isSelected())
               {
                    iSig = 1;
                    WS   = WS+" Where "+SItemTable+".StkGroupCode = '"+(String)matpanel.VDeptCode.elementAt(matpanel.JCDept.getSelectedIndex())+"'";
               }
               if(((matpanel.TSort.getText()).trim()).length()==0)
                    QString = QString+WS+" Order By 2";
               else
                    QString = QString+WS+" Order By "+matpanel.TSort.getText();


               return QString;
          }
     }

     private String getGroupCode(String str)
     {
          int  index     = -1;

          if((str.trim()).equals(""))
               return "0";

          if(VGroupName.size()>1)
          {
               index = VGroupName.indexOf(str);
               return (String)VGroupCode.elementAt(index);
          }
          return "0";
     }

     private String getUserCode(String str)
     {
          int  index     = -1;

          if((str.trim()).equals(""))
               return "0";

          if(VHodName.size()>0)
          {
               index     = VHodName.indexOf(str);
               return (String)VHodCode.elementAt(index);
          }
          return "0";
     }

     private String getStockGroupCode(String str)
     {
          int  index     = -1;

          if(VStockGroupName.size()>0)
          {
               index     = VStockGroupName.indexOf(str);
               return (String)VStockGroupCode.elementAt(index);
          }
          return "0";
     }

     private String getUomCode(String str)
     {
          int  index     = -1;

          if(VUOMName.size()>0)
          {
               index     = VUOMName.indexOf(str);
               return (String)VUOMCode.elementAt(index);
          }

          return "0";
     }

     private String getDeptCode(String str)
     {
          int  index     = -1;

          if(VDeptName.size()>0)
          {
               index     = VDeptName.indexOf(str);
               return (String)VDeptCode.elementAt(index);
          }

          return "0";
     }

     private String getModelCode(String str)
     {
          int  index     = -1;

          if(VModelName.size()>0)
          {
               index     = VModelName.indexOf(str);
               return (String)VModelCode.elementAt(index);
          }

          return "0";
     }

     private String getBrandCode(String str)
     {
          int  index     = -1;

          if(VBrandName.size()>0)
          {
               index     = VBrandName.indexOf(str);
               return (String)VBrandCode.elementAt(index);
          }

          return "0";
     }

     private String getSTGroupCode(String str)
     {
          int  index     = -1;

          if(VSTGroupName.size()>0)
          {
               index     = VSTGroupName.indexOf(str);
               return (String)VSTGroupCode.elementAt(index);
          }

          return "0";
     }

     private String getTempCode(String str)
     {
          int  index     = -1;

          if(VTempName.size()>0)
          {
               index     = VTempName.indexOf(str);
               return (String)VTempCode.elementAt(index);
          }

          return "0";
     }

     private String getRoomCode(String str)
     {
          int  index     = -1;

          if(VRoomName.size()>0)
          {
               index     = VRoomName.indexOf(str);
               return (String)VRoomCode.elementAt(index);
          }

          return "0";
     }

     private String getRackCode(String str)
     {
          int  index     = -1;

          if(VRackName.size()>0)
          {
               index     = VRackName.indexOf(str);
               return (String)VRackCode.elementAt(index);
          }

          return "0";
     }

     private String getRowCode(String str)
     {
          int  index     = -1;

          if(VRowName.size()>0)
          {
               index     = VRowName.indexOf(str);
               return (String)VRowCode.elementAt(index);
          }

          return "0";
     }

     private String getColumnCode(String str)
     {
          int  index     = -1;

          if(VColumnName.size()>0)
          {
               index     = VColumnName.indexOf(str);
               return (String)VColumnCode.elementAt(index);
          }

          return "0";
     }

     private String getTrayCode(String str)
     {
          int  index     = -1;

          if(VTrayName.size()>0)
          {
               index     = VTrayName.indexOf(str);
               return (String)VTrayCode.elementAt(index);
          }

          return "0";
     }

     private String getBoxCode(String str)
     {
          int  index     = -1;

          if(VBoxName.size()>0)
          {
               index     = VBoxName.indexOf(str);
               return (String)VBoxCode.elementAt(index);
          }

          return "0";
     }

     private String getObsoleteCode(String str)
     {
          int  index     = -1;

          if(VObsName.size()>0)
          {
               index     = VObsName.indexOf(str);
               return (String)VObsCode.elementAt(index);
          }

          return "0";
     }

     private String getQualityCheckCode(String str)
     {
          int  index     = -1;

          if(VQCheckName.size()>0)
          {
               index     = VQCheckName.indexOf(str);
               return (String)VQCheckCode.elementAt(index);
          }

          return "0";
     }

     private String getSalesCode(String str)
     {
          int  index     = -1;

          if(VSalesName.size()>0)
          {
               index     = VSalesName.indexOf(str);
               return (String)VSalesCode.elementAt(index);
          }

          return "0";
     }

     private String getCostCode(String str)
     {
          int  index     = -1;

          if(VCostName.size()>0)
          {
               index     = VCostName.indexOf(str);
               return (String)VCostCode.elementAt(index);
          }

          return "0";
     }

     private String getRateCode(String str)
     {
          int  index     = -1;

          if(VRateName.size()>0)
          {
               index     = VRateName.indexOf(str);
               return (String)VRateCode.elementAt(index);
          }

          return "0";
     }

     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TFind.getText();
               int       i         = indexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TFind1.getText();
               int       i         = codeIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList2 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TCatl.getText();
               int       i         = catlIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList3 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String    SFind     = matpanel.TDraw.getText();
               int       i         = drawIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }

     public class KeyList4 extends KeyAdapter
     {
          MaterialModiFrame MMF;

          public KeyList4(MaterialModiFrame MMF)
          {
               this.MMF = MMF;
          }

          public void keyReleased(KeyEvent ke)
          {
               String SPColour  = "";
               String SPSet     = "";
               String SPSize    = "";
               String SPSide    = "";

               /*if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    String    SStkGroupCode  = "";
                    int       i              = tabreport.ReportTable.getSelectedRow();

                    String    SItemCode = ((String)RowData[i][0]).trim();
                    String    SItemName = ((String)RowData[i][1]).trim();
                    String    QS        = "";
     
                    if(iMillCode==0)
                    {
                         QS =    " select Invitems.stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from invitems"+
                                 " where invitems.item_code = '"+SItemCode+"'";
                    }
                    else
                    {
                         QS =    " select "+SItemTable+".stkgroupcode,INVITEMS.PAPERCOLOR,INVITEMS.PAPERSETS,INVITEMS.PAPERSIZE,INVITEMS.PAPERSIDE from "+SItemTable+""+
                                 " inner join invitems on invitems.item_code = "+SItemTable+".item_code"+
                                 " where "+SItemTable+".item_code = '"+SItemCode+"'";
                    }

                    try
                    {
                         ORAConnection  oraConnection = ORAConnection.getORAConnection();
                         Connection     theConnection = oraConnection.getConnection();

                                        if(!theConnection.getAutoCommit())
                                             theConnection . setAutoCommit(true);

                         Statement      stat          = theConnection.createStatement();
                         ResultSet      result        = stat.executeQuery(QS);
                         result         . next();
                         SStkGroupCode  = (String)result.getString(1);
                         SPColour       = (String)result.getString(2);
                         SPSet          = (String)result.getString(3);
                         SPSize         = (String)result.getString(4);
                         SPSide         = (String)result.getString(5);
                         result         . close();
                         stat           . close();

                    }catch(Exception ex)
                    {
                         System.out.println(ex);
                    }

                    if(SStkGroupCode.equals("B01"))
                    {
                         StationaryFrame SF   =  new StationaryFrame(Layer,SItemCode,SItemName,SPColour,SPSet,SPSize,SPSide);
                         try
                         {
                              Layer     . add(SF);
                              Layer     . repaint();
                              SF        . setSelected(true);
                              Layer     . updateUI();
                              SF        . show();
                         }
                         catch(java.beans.PropertyVetoException ex){}
                    }
               }*/

               if(ke.getKeyCode()==KeyEvent.VK_F3)
               {
                    int       i         = tabreport.ReportTable.getSelectedRow();

                    String    SItemCode  = ((String)RowData[i][0]).trim();
                    String    SItemName  = ((String)RowData[i][1]).trim();
                    String    SStkGroup  = ((String)RowData[i][4]).trim();
                    String    SSector    = ((String)RowData[i][8]).trim();
                    String    SSubSector = ((String)RowData[i][9]).trim();

                    File f1 = new File("d:\\material.jpg");

                    boolean bDelete = f1.delete();

                    String SFileName    = getPictureFile(SItemCode);

                    MaterialSectorModiFrame MF = new MaterialSectorModiFrame(Layer,iMillCode,iAuthCode,iUserCode,SItemTable,SItemCode,SItemName,SStkGroup,SSector,SSubSector,SFileName,MMF,i);
                    try
                    {
                         Layer     . add(MF);
                         Layer     . repaint();
                         MF        . setSelected(true);
                         Layer     . updateUI();
                         MF        . show();
                    }
                    catch(java.beans.PropertyVetoException ex){}
               }
          }
     }

     public String getPictureFile(String SItemCode)
     {
          String SFile="";

          String QS1 = " Select length(Photo) from InvItems where Item_Code='"+SItemCode+"'";
          String QS2 = " Select Photo from InvItems where Item_Code='"+SItemCode+"'";

          double dLen=0;

          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
               Statement      stat          = theConnection.createStatement();
               ResultSet      result        = stat.executeQuery(QS1);

               while(result.next())
               {
                    dLen = result.getDouble(1);
               }
               result    . close();

               if(dLen>0)
               {
                    Blob b = null;

                    result        = stat.executeQuery(QS2);
                    while(result.next())
                    {
                         b = result.getBlob(1);
                    }
                    result    . close();

                    byte barr[] = b.getBytes(1,(int)b.length());

                    FileOutputStream fout = new FileOutputStream("D:\\material.jpg");
                    fout.write(barr);
                    fout.close();
                    SFile = "D:\\material.jpg";
               }
               else
               {
                    SFile = "Attach Photo";
               }
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return SFile;
     }


     public int codeIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][0];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int indexOf(String SFind)
     {
          SFind = SFind.toUpperCase();
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][1];
                         str       =              str       . toUpperCase();

               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int catlIndexOf(String SFind)
     {
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][6];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public int drawIndexOf(String SFind)
     {
          for(int i=0;i<RowData .length;i++)
          {
               String    str       = (String)RowData[i][5];
               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }

     public void updateMaterials()
     {
          try
          {
               ORAConnection  oraConnection = ORAConnection.getORAConnection();
               Connection     theConnection = oraConnection.getConnection();
                              if(!theConnection.getAutoCommit())
                                   theConnection . setAutoCommit(true);
               Statement      stat          = theConnection.createStatement();

               for(int i=0;i<RowData.length;i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][46];
                    if(Bselected.booleanValue())
                    {
                         if(iMillCode == 0)
                         {
                              String    QString = " Update InvItems Set ";
                                        QString = QString+" ITEM_NAME       = '"+((String)RowData[i][1]).toUpperCase()+"',";
                                        QString = QString+" SHORTNAME       = '"+((String)RowData[i][2]).toUpperCase()+"',";
                                        QString = QString+" UOMCODE         = '"+getUomCode(((String)RowData[i][3]).toUpperCase())+"',";
                                        QString = QString+" STKGROUPCODE    = '"+getStockGroupCode((String)RowData[i][4])+"',";
                                        QString = QString+" DRAW            = '"+((String)RowData[i][5]).toUpperCase()+"',";
                                        QString = QString+" CATL            = '"+((String)RowData[i][6]).toUpperCase()+"',";
                                        QString = QString+" PARTNUMBER      = '"+((String)RowData[i][7]).toUpperCase()+"',";
                                        QString = QString+" DEPTCODE        = 0"+getDeptCode((String)RowData[i][10])+", ";
                                        QString = QString+" MACMODELCODE    = 0"+getModelCode((String)RowData[i][11])+", ";
                                        QString = QString+" MATGROUPCODE    = 0"+getGroupCode((String)RowData[i][12])+", ";
                                        QString = QString+" BRANDCODE       = 0"+getBrandCode((String)RowData[i][13])+", ";
                                        QString = QString+" STGROUP         = 0"+getSTGroupCode((String)RowData[i][14])+", ";
                                        QString = QString+" INNERDIA        = '"+(String)RowData[i][15]+"',";
                                        QString = QString+" OUTERDIA        = '"+(String)RowData[i][16]+"',";
                                        QString = QString+" MATLENGTH       = '"+(String)RowData[i][17]+"',";
                                        QString = QString+" THICKNESS       = '"+(String)RowData[i][18]+"',";
                                        QString = QString+" WIDTH           = '"+(String)RowData[i][19]+"',";
                                        QString = QString+" VISCOSITY       = '"+(String)RowData[i][20]+"',";
                                        QString = QString+" HEIGHT          = '"+(String)RowData[i][21]+"',";
                                        QString = QString+" TEMPCODE        = 0"+getTempCode((String)RowData[i][22])+", ";
                                        QString = QString+" CHAPTERNO       = '"+(String)RowData[i][23]+"',";
                                        QString = QString+" COMMODITYCODE   = '"+(String)RowData[i][24]+"',";
                                        QString = QString+" OPGQTY          = 0"+(String)RowData[i][25]+",";
                                        QString = QString+" OPGVAL          = 0"+(String)RowData[i][26]+",";
                                        QString = QString+" ROQ             = 0"+(String)RowData[i][27]+",";
                                        QString = QString+" MINQTY          = 0"+(String)RowData[i][28]+",";
                                        QString = QString+" MAXQTY          = 0"+(String)RowData[i][29]+",";
                                        QString = QString+" LOCNAME         = '"+((String)RowData[i][30]).toUpperCase()+"', ";
                                        QString = QString+" REGULAR         = 0"+common.toInt(((String)RowData[i][31]))+", ";
                                        QString = QString+" HODCODE         = 0"+getUserCode((String)RowData[i][32])+",";
                                        QString = QString+" ROOMCODE        = 0"+getRoomCode((String)RowData[i][33])+",";
                                        QString = QString+" RACKCODE        = 0"+getRackCode((String)RowData[i][34])+",";
                                        QString = QString+" ROWCODE         = 0"+getRowCode((String)RowData[i][35])+",";
                                        QString = QString+" COLUMNCODE      = 0"+getColumnCode((String)RowData[i][36])+",";
                                        QString = QString+" TRAYCODE        = 0"+getTrayCode((String)RowData[i][37])+",";
                                        QString = QString+" BOXCODE         = 0"+getBoxCode((String)RowData[i][38])+",";
                                        QString = QString+" OBSOLETE        = 0"+getObsoleteCode((String)RowData[i][39])+",";
                                        QString = QString+" AGINGPERIOD     = 0"+(String)RowData[i][40]+",";
                                        QString = QString+" CHECKFREQ       = 0"+(String)RowData[i][41]+",";
                                        QString = QString+" QUALITYCHECK    = 0"+getQualityCheckCode((String)RowData[i][42])+",";
                                        QString = QString+" SALESSTATUS     = 0"+getSalesCode((String)RowData[i][43])+",";
                                        QString = QString+" COSTINGTYPE     = 0"+getCostCode((String)RowData[i][44])+",";
                                        QString = QString+" RATEREVISION    = 0"+getRateCode((String)RowData[i][45])+",";
                                        QString = QString+" CREATIONDATE    = '"+common.getServerDate()+"',";
                                        QString = QString+" USERCODE        = "+iUserCode+" ";
                                        QString = QString+" Where ITEM_CODE = '"+(String)RowData[i][0]+"'";

                              stat.execute(QString);
                         }
                         else
                         {
                              String    QString = "Update InvItems Set ";
                                        QString = QString+" ITEM_NAME           = '"+((String)RowData[i][1]).toUpperCase()+"',";
                                        QString = QString+" SHORTNAME           = '"+((String)RowData[i][2]).toUpperCase()+"',";
                                        QString = QString+" UOMCODE             = '"+getUomCode(((String)RowData[i][3]).toUpperCase())+"',";
                                        QString = QString+" DRAW                = '"+((String)RowData[i][5]).toUpperCase()+"',";
                                        QString = QString+" CATL                = '"+((String)RowData[i][6]).toUpperCase()+"',";
                                        QString = QString+" PARTNUMBER          = '"+((String)RowData[i][7]).toUpperCase()+"',";
                                        QString = QString+" DEPTCODE            = 0"+getDeptCode((String)RowData[i][10])+", ";
                                        QString = QString+" MACMODELCODE        = 0"+getModelCode((String)RowData[i][11])+", ";
                                        QString = QString+" BRANDCODE           = 0"+getBrandCode((String)RowData[i][13])+", ";
                                        QString = QString+" STGROUP             = 0"+getSTGroupCode((String)RowData[i][14])+", ";
                                        QString = QString+" INNERDIA            = '"+(String)RowData[i][15]+"',";
                                        QString = QString+" OUTERDIA            = '"+(String)RowData[i][16]+"',";
                                        QString = QString+" MATLENGTH           = '"+(String)RowData[i][17]+"',";
                                        QString = QString+" THICKNESS           = '"+(String)RowData[i][18]+"',";
                                        QString = QString+" WIDTH               = '"+(String)RowData[i][19]+"',";
                                        QString = QString+" VISCOSITY           = '"+(String)RowData[i][20]+"',";
                                        QString = QString+" HEIGHT              = '"+(String)RowData[i][21]+"',";
                                        QString = QString+" TEMPCODE            = 0"+getTempCode((String)RowData[i][22])+", ";
                                        QString = QString+" CHAPTERNO           = '"+(String)RowData[i][23]+"',";
                                        QString = QString+" COMMODITYCODE       = '"+(String)RowData[i][24]+"',";
                                        QString = QString+" RATEREVISION        = 0"+getRateCode((String)RowData[i][45])+",";
                                        QString = QString+" CREATIONDATE        = '"+common.getServerDate()+"',";
                                        QString = QString+" USERCODE            = "+iUserCode+" ";
                                        QString = QString+" Where ITEM_CODE     = '"+(String)RowData[i][0]+"'";

                              stat . execute(QString);
                              
                              String    QString1 = "Update "+SItemTable+" Set ";
                                        QString1 = QString1+" STKGROUPCODE        = '"+getStockGroupCode((String)RowData[i][4])+"',";
                                        QString1 = QString1+" MATGROUPCODE        = 0"+getGroupCode((String)RowData[i][12])+",";
                                        QString1 = QString1+" OPGQTY              = 0"+(String)RowData[i][25]+",";
                                        QString1 = QString1+" OPGVAL              = 0"+(String)RowData[i][26]+",";
                                        QString1 = QString1+" ROQ                 = 0"+(String)RowData[i][27]+",";
                                        QString1 = QString1+" MINQTY              = 0"+(String)RowData[i][28]+",";
                                        QString1 = QString1+" MAXQTY              = 0"+(String)RowData[i][29]+",";
                                        QString1 = QString1+" LOCNAME             = '"+((String)RowData[i][30]).toUpperCase()+"', ";
                                        QString1 = QString1+" REGULAR             = 0"+common.toInt(((String)RowData[i][31]))+", ";
                                        QString1 = QString1+" HODCODE             = 0"+getUserCode((String)RowData[i][32])+",";
                                        QString1 = QString1+" ROOMCODE            = 0"+getRoomCode((String)RowData[i][33])+",";
                                        QString1 = QString1+" RACKCODE            = 0"+getRackCode((String)RowData[i][34])+",";
                                        QString1 = QString1+" ROWCODE             = 0"+getRowCode((String)RowData[i][35])+",";
                                        QString1 = QString1+" COLUMNCODE          = 0"+getColumnCode((String)RowData[i][36])+",";
                                        QString1 = QString1+" TRAYCODE            = 0"+getTrayCode((String)RowData[i][37])+",";
                                        QString1 = QString1+" BOXCODE             = 0"+getBoxCode((String)RowData[i][38])+",";
                                        QString1 = QString1+" OBSOLETE            = 0"+getObsoleteCode((String)RowData[i][39])+",";
                                        QString1 = QString1+" AGINGPERIOD         = 0"+(String)RowData[i][40]+",";
                                        QString1 = QString1+" CHECKFREQ           = 0"+(String)RowData[i][41]+",";
                                        QString1 = QString1+" QUALITYCHECK        = 0"+getQualityCheckCode((String)RowData[i][42])+",";
                                        QString1 = QString1+" SALESSTATUS         = 0"+getSalesCode((String)RowData[i][43])+",";
                                        QString1 = QString1+" COSTINGTYPE         = 0"+getCostCode((String)RowData[i][44])+",";
                                        QString1 = QString1+" CREATIONDATE        = '"+common.getServerDate()+"',";
                                        QString1 = QString1+" USERCODE            = "+iUserCode+" ";
                                        QString1 = QString1+" Where ITEM_CODE     = '"+(String)RowData[i][0]+"'";

                              stat . execute(QString1);
                         }
                    }
               }
               stat           . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void initialize()
     {
          System.out.println("Inside initialize");

          VHodCode            = null;
          VHodName            = null;
          VGroupCode          = null;
          VGroupName          = null;
          VStockGroupCode     = null;
          VStockGroupName     = null;
          VUOMCode            = null;
          VUOMName            = null;

          VDeptCode           = null;
          VDeptName           = null;
          VModelCode          = null;
          VModelName          = null;
          VBrandCode          = null;
          VBrandName          = null;
          VSTGroupCode        = null;
          VSTGroupName        = null;
          VTempCode           = null;
          VTempName           = null;
          VRoomCode           = null;
          VRoomName           = null;
          VRackCode           = null;
          VRackName           = null;
          VRowCode            = null;
          VRowName            = null;
          VColumnCode         = null;
          VColumnName         = null;
          VTrayCode           = null;
          VTrayName           = null;
          VBoxCode            = null;
          VBoxName            = null;
          VObsCode            = null;
          VObsName            = null;
          VQCheckCode         = null;
          VQCheckName         = null;
          VSalesCode          = null;
          VSalesName          = null;
          VCostCode           = null;
          VCostName           = null;
          VRateCode           = null;
          VRateName           = null;

          theVector      = null;
          aRow           = null;
     }

     public void clearObjects()
     {
          if(theVector!=null)
          {
               int iLen = theVector.size();
               System.out.println("Len:"+iLen);
     
               if(iLen>0)
               {
                    theVector.clear();
                    System.gc();
               }

               iLen = theVector.size();
               System.out.println("AftLen:"+iLen);

          }
     }

     protected void finalize() throws Throwable
     {
          System.out.println("Inside Finalize");

          RowData             = null;
          ColumnData          = null;
          ColumnType          = null;
          this.Layer          = null;

          JTP                 = null;
          ListPanel           = null;
          BOk                 = null;
          matpanel            = null;
          tabreport           = null;
          SFind               = null;

          VHodCode            . removeAllElements();
          VHodCode            = null;
          VHodName            . removeAllElements();
          VHodName            = null;
          VGroupCode          . removeAllElements();
          VGroupCode          = null;
          VGroupName          . removeAllElements();
          VGroupName          = null;
          VStockGroupCode     . removeAllElements();
          VStockGroupCode     = null;
          VStockGroupName     . removeAllElements();
          VStockGroupName     = null;
          VUOMCode            . removeAllElements();
          VUOMCode            = null;
          VUOMName            . removeAllElements();
          VUOMName            = null;

          VDeptCode           . removeAllElements();
          VDeptCode           = null;
          VDeptName           . removeAllElements();
          VDeptName           = null;
          VModelCode          . removeAllElements();
          VModelCode          = null;
          VModelName          . removeAllElements();
          VModelName          = null;
          VBrandCode          . removeAllElements();
          VBrandCode          = null;
          VBrandName          . removeAllElements();
          VBrandName          = null;
          VSTGroupCode        . removeAllElements();
          VSTGroupCode        = null;
          VSTGroupName        . removeAllElements();
          VSTGroupName        = null;
          VTempCode           . removeAllElements();
          VTempCode           = null;
          VTempName           . removeAllElements();
          VTempName           = null;
          VRoomCode           . removeAllElements();
          VRoomCode           = null;
          VRoomName           . removeAllElements();
          VRoomName           = null;
          VRackCode           . removeAllElements();
          VRackCode           = null;
          VRackName           . removeAllElements();
          VRackName           = null;
          VRowCode            . removeAllElements();
          VRowCode            = null;
          VRowName            . removeAllElements();
          VRowName            = null;
          VColumnCode         . removeAllElements();
          VColumnCode         = null;
          VColumnName         . removeAllElements();
          VColumnName         = null;
          VTrayCode           . removeAllElements();
          VTrayCode           = null;
          VTrayName           . removeAllElements();
          VTrayName           = null;
          VBoxCode            . removeAllElements();
          VBoxCode            = null;
          VBoxName            . removeAllElements();
          VBoxName            = null;
          VObsCode            . removeAllElements();
          VObsCode            = null;
          VObsName            . removeAllElements();
          VObsName            = null;
          VQCheckCode         . removeAllElements();
          VQCheckCode         = null;
          VQCheckName         . removeAllElements();
          VQCheckName         = null;
          VSalesCode          . removeAllElements();
          VSalesCode          = null;
          VSalesName          . removeAllElements();
          VSalesName          = null;
          VCostCode           . removeAllElements();
          VCostCode           = null;
          VCostName           . removeAllElements();
          VCostName           = null;
          VRateCode           . removeAllElements();
          VRateCode           = null;
          VRateName           . removeAllElements();
          VRateName           = null;

          theVector      . clear();
          theVector      = null;
          aRow           = null;

          super               . finalize();
     }

     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }
}

