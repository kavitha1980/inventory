package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import guiutil.*;
import jdbc.*;

public class MaterialSectorModiFrame extends JInternalFrame
{
     JLayeredPane   Layer;

     JTextField     TMatCode,TMatPaperColour,TMatPaperSize;
     MyTextField    TMatName;
     JComboBox      JCStkGroup,JMatPaperSets,JMatPaperSide;
     MyComboBox     JCSector,JCSubSector;

     JButton        BOk,BCancel,BPhoto;
     JPanel         NamePanel,StatPanel,PicPanel;
     JPanel         Name1Panel,Stat1Panel,BottomPanel;

     JTabbedPane    JTP;

     Vector         VStkGroup,VStkGroupCode;
     Vector         VPaperSet,VSetCode,VSideName,VSideCode;
     Vector         VSector,VSectorCode,VSubSector,VSubSectorCode;

     Common         common    = new Common();
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable,SItemCode,SItemName,SPhotoName;
     int            iRow;

     MaterialModiFrame MMF;

     String         SStkGroup,SSector,SSubSector;

     boolean        bStatFlag = false;
     boolean        bAutoCommitFlag = true;

     Connection     theConnection  = null;
     Connection     theDConnection = null;

     public MaterialSectorModiFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable,String SItemCode,String SItemName,String SStkGroup,String SSector,String SSubSector,String SPhotoName,MaterialModiFrame MMF,int iRow)
     {
          this.Layer      = Layer;
          this.iMillCode  = iMillCode;
          this.iAuthCode  = iAuthCode;
          this.iUserCode  = iUserCode;
          this.SItemTable = SItemTable;
          this.SItemCode  = SItemCode;
          this.SItemName  = SItemName;
          this.SStkGroup  = SStkGroup;
          this.SSector    = SSector;
          this.SSubSector = SSubSector;
          this.SPhotoName = SPhotoName;
          this.MMF        = MMF;
          this.iRow       = iRow;

          getMasterData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setPreset();
     }

     public void createComponents()
     {
          TMatCode       = new JTextField();
          TMatName       = new MyTextField(50);
          TMatPaperColour= new JTextField();
          TMatPaperSize  = new JTextField();

          JCSector       = new MyComboBox(VSector);
          JCSubSector    = new MyComboBox();

          JMatPaperSide  = new JComboBox(VSideName);
          JMatPaperSets  = new JComboBox(VPaperSet);
          JCStkGroup     = new JComboBox(VStkGroup);

          JTP            = new JTabbedPane(JTabbedPane.RIGHT);

          BOk            = new JButton("Okay");
          BCancel        = new JButton("Cancel");
          BPhoto         = new JButton("Attach Photo");

          NamePanel      = new JPanel();
          StatPanel      = new JPanel();
          PicPanel       = new JPanel();
          BottomPanel    = new JPanel();

          Name1Panel     = new JPanel();
          Stat1Panel     = new JPanel();
          
          TMatCode       . setEditable(false);
     }

     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Materials");

          NamePanel.setLayout(new BorderLayout());
          StatPanel.setLayout(new BorderLayout());
          PicPanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          Name1Panel.setLayout(new GridLayout(6,2,10,10));
          Stat1Panel.setLayout(new GridLayout(4,2,10,10));
     }

     public void addComponents()
     {
          Name1Panel.add(new JLabel("Stock Group"));
          Name1Panel.add(JCStkGroup);
          Name1Panel.add(new JLabel("Material Code"));
          Name1Panel.add(TMatCode);
          Name1Panel.add(new JLabel("Material Name"));
          Name1Panel.add(TMatName);
          Name1Panel.add(new JLabel("Industry Sector"));
          Name1Panel.add(JCSector);
          Name1Panel.add(new JLabel("Industry Sector SubGroup"));
          Name1Panel.add(JCSubSector);
          Name1Panel.add(new JLabel("Select Image of the Material"));
          Name1Panel.add(BPhoto);

          Stat1Panel.add(new JLabel("Paper Colour"));
          Stat1Panel.add(TMatPaperColour);
          Stat1Panel.add(new JLabel("Paper Sets"));
          Stat1Panel.add(JMatPaperSets);
          Stat1Panel.add(new JLabel("Paper Size"));
          Stat1Panel.add(TMatPaperSize);
          Stat1Panel.add(new JLabel("Paper Side"));
          Stat1Panel.add(JMatPaperSide);

          if(iAuthCode>0)
          {
               BottomPanel.add(BOk);
          }
          BottomPanel.add(BCancel);

          NamePanel.add("North",Name1Panel);
          StatPanel.add("North",Stat1Panel);

          JTP.addTab("Naming & Identification",NamePanel);
          JTP.addTab("Photo",PicPanel);
          JTP.addTab("Stationary",StatPanel);

          getContentPane().add("Center",JTP);
          getContentPane().add("South",BottomPanel);
     }

     public void addListeners()
     {
          BPhoto.addActionListener(new PhotoList());
          BOk.addActionListener(new ActList());
          JCStkGroup.addActionListener(new SelectList());
          JCSector.addActionListener(new SectorList());
     }

     public void setPreset()
     {
          TMatCode.setText(SItemCode);
          TMatName.setText(SItemName);
          TMatName.setEditable(false);
          JCStkGroup.setSelectedItem(SStkGroup);
          JCStkGroup.setEnabled(false);
          setStationary();
          JCSector.setSelectedItem(SSector);
          setSubSector();
          JCSubSector.setSelectedItem(SSubSector);
          BPhoto.setText(SPhotoName);
          setPhoto(BPhoto.getText());
     }


     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);

               updateMaterial();

               try
               {
                    if(bAutoCommitFlag)
                    {
                         theConnection  . commit();
                         System         . out.println("Commit");
                         theConnection  . setAutoCommit(true);
                         removeHelpFrame();
                    }
                    else
                    {
                         theConnection  . rollback();
                         System         . out.println("RollBack");
                         theConnection  . setAutoCommit(true);
                         BOk.setEnabled(true);
                    }
               }catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
     }

     private class PhotoList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               JFileChooser fc = new JFileChooser();

               int rVal = fc.showOpenDialog(MaterialSectorModiFrame.this);

               if (rVal == JFileChooser.APPROVE_OPTION)
               {
                    if((fc.getSelectedFile().getName()).endsWith(".jpg") || (fc.getSelectedFile().getName()).endsWith(".JPG"))
                    {
                         String sCurDirectory = fc.getCurrentDirectory().toString();
     
                         if(sCurDirectory.startsWith("/"))
                         {
                              BPhoto.setText((fc.getCurrentDirectory().toString())+"/"+(fc.getSelectedFile().getName()));
                         }
                         else
                         {
                              BPhoto.setText((fc.getCurrentDirectory().toString())+"\\"+(fc.getSelectedFile().getName()));
                         }
                         setPhoto(BPhoto.getText());
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Only jpg format allowed","Information",JOptionPane.INFORMATION_MESSAGE);
                         BPhoto.setText(SPhotoName);
                         setPhoto(BPhoto.getText());
                    }
               }

               if (rVal == JFileChooser.CANCEL_OPTION)
               {
                    BPhoto.setText(SPhotoName);
                    setPhoto(BPhoto.getText());
               }
          }
     }

     public class SelectList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setStationary();
          }
     }

     public class SectorList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setSubSector();
          }
     }

     public void setStationary()
     {
          JMatPaperSets  . setSelectedIndex(0);
          JMatPaperSide  . setSelectedIndex(0);
          TMatPaperColour. setText("");
          TMatPaperSize  . setText("");

          if(((String)JCStkGroup.getSelectedItem()).equals("STATIONARY"))
          {

               String SPColour = "";
               String SPSize   = "";
               int    iPSet    = 0;
               int    iPSide   = 0;

               String QS =  " select PAPERCOLOR,PAPERSETS,PAPERSIZE,PAPERSIDE from invitems"+
                            " where item_code = '"+SItemCode+"'";

               try
               {
                    ORAConnection  oraConnection = ORAConnection.getORAConnection();
                    Connection     theConnection = oraConnection.getConnection();

                                   if(!theConnection.getAutoCommit())
                                        theConnection . setAutoCommit(true);

                    Statement      stat          = theConnection.createStatement();
                    ResultSet      result        = stat.executeQuery(QS);
                    result         . next();
                    SPColour       = common.parseNull((String)result.getString(1));
                    iPSet          = result.getInt(2);
                    SPSize         = common.parseNull((String)result.getString(3));
                    iPSide         = result.getInt(4);
                    result         . close();
                    stat           . close();

               }catch(Exception ex)
               {
                    System.out.println(ex);
               }

               TMatPaperColour.setText(SPColour);
               TMatPaperSize  .setText(SPSize);
               JMatPaperSets  .setSelectedIndex(iPSet);
               JMatPaperSide  .setSelectedIndex(iPSide);

               bStatFlag = true;
               JTP.setEnabledAt(2,true);
               StatPanel.updateUI();
          }
          else
          {
               bStatFlag = false;
               JTP.setEnabledAt(2,false);
               StatPanel.updateUI();
          }
     }

     public void setPhoto(String SFileName)
     {
          try
          {
               PicPanel.removeAll();
               PicPanel.updateUI();
          }
          catch(Exception ex){}

          if(!SFileName.equals("Attach Photo"))
          {
               ImageIcon img = new ImageIcon(SFileName);
               img.getImage().flush();
               JLabel jlabel = new JLabel("");
               jlabel.setIcon(img);
               PicPanel.add("Center",new JScrollPane(jlabel));
               PicPanel.updateUI();
          }
     }

     public void setSubSector()
     {
          int iIndex = JCSector.getSelectedIndex();

          ResultSet result    = null;

          VSubSector     = new Vector();
          VSubSectorCode = new Vector();

          JCSubSector.removeAllItems();

          String SSectorCode = (String)VSectorCode.elementAt(iIndex);

          String QS = " Select Name,Code from IndustrySubSector Where SectorCode=0 or SectorCode="+SSectorCode+" Order by Name ";

          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                               theConnection .  setAutoCommit(true);
               Statement       stat          =  theConnection.createStatement();

               result    = stat.executeQuery(QS);

               while(result.next())
               {
                    VSubSector.addElement(result.getString(1));
                    VSubSectorCode.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();

               for(int i=0;i<VSubSector.size();i++)
               {
                    JCSubSector.addItem((String)VSubSector.elementAt(i));
               }

               JCSubSector.setSelectedItem("N.A.");
          }
          catch(Exception ex)
          {
               System.out.println("setSubSector :"+ex);
          }
     }

     public void updateMaterial()
     {
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
                              theConnection  = oraConnection.getConnection();
                              theConnection  . setAutoCommit(true);
               PreparedStatement ps          = theConnection.prepareStatement(getUpdateQS());


               if(theConnection.getAutoCommit())
                         theConnection  . setAutoCommit(false);


               if((BPhoto.getText()).equals("Attach Photo") || (BPhoto.getText()).equals(SPhotoName))
               {
                    ps.setString(1,(String)VSectorCode.elementAt(JCSector.getSelectedIndex()));
                    ps.setString(2,(String)VSubSectorCode.elementAt(JCSubSector.getSelectedIndex()));
                    ps.setString(3,(((String)TMatPaperColour.getText()).trim()).toUpperCase());
                    ps.setString(4,(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex()));
                    ps.setString(5,(((String)TMatPaperSize.getText()).trim()).toUpperCase());
                    ps.setString(6,(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex()));
               }
               else
               {
                    ps.setString(1,(String)VSectorCode.elementAt(JCSector.getSelectedIndex()));
                    ps.setString(2,(String)VSubSectorCode.elementAt(JCSubSector.getSelectedIndex()));
                    ps.setString(3,(((String)TMatPaperColour.getText()).trim()).toUpperCase());
                    ps.setString(4,(String)VSetCode.elementAt(JMatPaperSets.getSelectedIndex()));
                    ps.setString(5,(((String)TMatPaperSize.getText()).trim()).toUpperCase());
                    ps.setString(6,(String)VSideCode.elementAt(JMatPaperSide.getSelectedIndex()));

                    FileInputStream fin = new FileInputStream(BPhoto.getText());
                    ps.setBinaryStream(7,fin,fin.available());
                    ps.setString(8,"jpg");
               }

               ps.executeUpdate();

               ps.close();

               MMF.RowData[iRow][8] = (String)JCSector.getSelectedItem();
               MMF.RowData[iRow][9] = (String)JCSubSector.getSelectedItem();

          }catch(Exception e)
          {
               System.out.println(e);
               bAutoCommitFlag     = false;
               e.printStackTrace();
          }
     }

     public String getUpdateQS()
     {
          String QS = "";

          if((BPhoto.getText()).equals("Attach Photo") || (BPhoto.getText()).equals(SPhotoName))
          {
               QS = " Update InvItems set SectorCode=?,SubSectorCode=?,PaperColor=?,PaperSets=?,PaperSize=?,PaperSide=? "+
                    " Where Item_Code='"+SItemCode+"'";
          }
          else
          {
               QS = " Update InvItems set SectorCode=?,SubSectorCode=?,PaperColor=?,PaperSets=?,PaperSize=?,PaperSide=?,Photo=?,PhotoFormat=? "+
                    " Where Item_Code='"+SItemCode+"'";
          }

          return QS;
     }

     public void removeHelpFrame()
     {
          Layer.remove(this);
          Layer.repaint();
          Layer.updateUI();
     }

     public void getMasterData()
     {
          ResultSet result    = null;

          VStkGroup      = new Vector();
          VStkGroupCode  = new Vector();
          VPaperSet      = new Vector();
          VSetCode       = new Vector();
          VSideName      = new Vector();
          VSideCode      = new Vector();

          VSector        = new Vector();
          VSectorCode    = new Vector();


          try
          {
               ORAConnection   oraConnection =  ORAConnection.getORAConnection();
               Connection      theConnection =  oraConnection.getConnection();
                               theConnection .  setAutoCommit(true);
               Statement       stat          =  theConnection.createStatement();

               result = stat.executeQuery("Select GroupName,GroupCode From StockGroup Order By 1");

               while(result.next())
               {
                    VStkGroup.addElement(result.getString(1));
                    VStkGroupCode.addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SetName,SetCode From PaperSet Order By SetCode");

               while(result.next())
               {
                    VPaperSet . addElement(result.getString(1));
                    VSetCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SideName,SideCode From PaperSide Order By SideCode");

               while(result.next())
               {
                    VSideName . addElement(result.getString(1));
                    VSideCode . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select Name,Code From IndustrySector Order By Name");

               while(result.next())
               {
                    VSector.addElement(result.getString(1));
                    VSectorCode.addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("getMasterData :"+ex);
          }
     }

}
