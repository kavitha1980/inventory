package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MaterialTransferFrame extends JInternalFrame
{
     JLayeredPane     Layer;
     
     Object         RowData[][];
     String         ColumnData[] = {"Code","Item Name","Stock Group","Click for Transfer"};
     String         ColumnType[] = {"S"   ,"S"        ,"E"          ,"B"               };
     
     Vector         VGroupCode,VGroupName,VStockGroupCode,VStockGroupName;

     Vector         theVector;
     Vector         VUomCode;
     
     JTabbedPane    JTP;
     JPanel         ListPanel;
     JButton        BOk;
     Common         common = new Common();
     MatTranPanel   matpanel;
     TabReport      tabreport;
     String         SFind="";
     int            iMillCode,iAuthCode,iUserCode;
     String         SItemTable;
     
     public MaterialTransferFrame(JLayeredPane Layer,int iMillCode,int iAuthCode,int iUserCode,String SItemTable)
     {
          this.Layer         = Layer;
          this.iMillCode     = iMillCode;
          this.iAuthCode     = iAuthCode;
          this.iUserCode     = iUserCode;
          this.SItemTable    = SItemTable;
          
          matpanel           = new MatTranPanel();
          JTP                = new JTabbedPane();
          ListPanel          = new JPanel(true);
          BOk                = new JButton("Update");
          JTP.addTab("List of Materials Not in "+SItemTable,ListPanel);
          setLayouts();
     }
     
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,500);
          setTitle("Administration Utility for Material Master Transfer");
          
          ListPanel . setLayout(new BorderLayout());
          ListPanel . add("North",matpanel);
          if(iAuthCode>0)
          {
               ListPanel.add("South",BOk);
          }
          matpanel  . BApply.addActionListener(new ActList());
          BOk       . addActionListener(new UpdtList());
          
          getContentPane().add("Center",JTP);
     }
          
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               matpanel  . TFind.setEditable(true);
               matpanel  . TFind1.setEditable(true);
               BOk       . setEnabled(true);
               
               setVector();
               setRowData();
               try
               {
                    ListPanel.remove(tabreport); 
               }
               catch(Exception ex)
               {
               }
               try
               {
                    tabreport      = new TabReport(RowData,ColumnData,ColumnType);
                    tabreport      . ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                    ListPanel      . add(tabreport,BorderLayout.CENTER);
                    setSelected(true);
                    Layer          . repaint();
                    Layer          . updateUI();
                    TableColumn    StockColumn    = tabreport.ReportTable.getColumn("Stock Group");  
                                   StockColumn    . setCellEditor(new DefaultCellEditor(new JComboBox(VStockGroupName)));
                    
                                   matpanel       . TFind.addKeyListener(new KeyList());
                                   matpanel       . TFind1.addKeyListener(new KeyList1());
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }
     
     public class UpdtList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               BOk  . setEnabled(false);
               updateMaterials();
               removeHelpFrame();
          }
     }
     
     public class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = matpanel.TFind.getText();
               int i=indexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport.ReportTable.getCellRect(i,0,true);
                              tabreport . ReportTable.scrollRectToVisible(cellRect);
               }
          }
     }
     
     public class KeyList1 extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               String SFind = matpanel.TFind1.getText();
               int i=codeIndexOf(SFind);
               if (i>-1)
               {
                              tabreport . ReportTable  . setRowSelectionInterval(i,i);
                    Rectangle cellRect  = tabreport    . ReportTable  . getCellRect(i,0,true);
                              tabreport . ReportTable  . scrollRectToVisible(cellRect);
               }
          }
     }
     
     public int indexOf(String SFind)
     {
          SFind = SFind.toUpperCase();
          
          for(int i=0;i<theVector . size();i++)
          {
               String[] SMaterial  = (String[])theVector . elementAt(i);
               String   str        =           SMaterial[1];
                        str        =           str       . toUpperCase();

               if(str.startsWith(SFind))
                    return i;
          }
          return -1;
     }
     
     public int codeIndexOf(String SFind)
     {
          SFind = SFind.toUpperCase();

          for(int i=0;i<theVector . size();i++)
          {
               String[] SMaterial  = (String[])theVector . elementAt(i);
               String    str       =           SMaterial[0];

               if(str.startsWith(SFind))
               return i;
          }
          return -1;
     }
     
     public void setVector()
     {
          String[]  aRow      = null;
          ResultSet result    = null;

          theVector           = new Vector();
          VUomCode            = new Vector();
          VStockGroupCode     = new Vector();
          VStockGroupName     = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();
                              result         = stat.executeQuery(getQString());

               int            inumColumns   = result.getMetaData().getColumnCount();

               while(result.next())
               {
                    aRow = new String[inumColumns];
                    for (int i = 0;i<inumColumns;i++)
                         aRow[i]   = common.parseNull(result.getString(i+1));
                    theVector.addElement(aRow);                         
               }
               result.close();
               
               result = stat.executeQuery("Select GroupCode,GroupName From StockGroup Order By 2");

               while(result.next())
               {
                    VStockGroupCode.addElement(result.getString(1));
                    VStockGroupName.addElement(result.getString(2));
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void setRowData()
     {
          RowData = new Object[theVector.size()][ColumnData.length];
          for(int i=0;i<theVector.size();i++)
          {
               String[] sMaterial = (String[])theVector . elementAt(i);

               RowData[i][0] = sMaterial[0];
               RowData[i][1] = sMaterial[1];
               RowData[i][2] = sMaterial[2];
               RowData[i][3] = new Boolean(false);
               VUomCode.addElement(sMaterial[3]);
          }
     }

     public void updateMaterials()
     {
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();               
               Statement      stat           = theConnection.createStatement();

               /*DORAConnection DoraConnection = DORAConnection.getORAConnection();
                              theDConnection = DoraConnection.getConnection();
                              theDConnection . setAutoCommit(true);
                              Statement dyestat =  theDConnection.createStatement();
               */

                             
               for(int i=0;i<theVector.size();i++)
               {
                    Boolean Bselected = (Boolean)RowData[i][3];
                    if(Bselected.booleanValue())
                    {
                         String SStkGroupCode = getStockGroupCode((String)RowData[i][2]);

                         String QS1 = "Insert Into "+SItemTable+" (Item_Code,StkGroupCode,USERCODE,ID,CREATIONDATE) Values (";
                         QS1 = QS1+"'"+(String)RowData[i][0]+"',";
                         QS1 = QS1+"'"+SStkGroupCode+"',";
                         QS1 = QS1+iUserCode+",";
                         QS1 = QS1+getNextVal(""+SItemTable+"_SEQ")+",";
                         QS1 = QS1+"'"+common.getServerDate()+"')";
                         stat.execute(QS1);

                         /*if(iMillCode==1)
                         {
                              if((SStkGroupCode.equals("C01")) || (SStkGroupCode.equals("C02")))
                              {
                                   String QS2 = " Insert Into InvItems (Item_Code,Item_Name,UOM,StkGroupCode) Values (";
                                   QS2 = QS2+"'"+TMatCode.getText()+"',";
                                   QS2 = QS2+"'"+SName.toUpperCase()+"',";
                                   QS2 = QS2+"'"+(String)VUomCode.elementAt(i)+"',";
                                   QS2 = QS2+"'"+SStkGroupCode+"')";

                                   dyestat.execute(QS2);
                              }
                         }*/
                    }
               }
               stat.close();
               //dyestat.close();
          }
          catch(Exception ex)
          {
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int iValue = -1;
          String QS = "Select "+SSequence+".NextVal from dual";
          try
          {
               ORAConnection  oraConnection  = ORAConnection. getORAConnection();
               Connection     theConnection  = oraConnection. getConnection();               
               Statement      stat           = theConnection. createStatement();
               ResultSet      result         = stat         . executeQuery(QS);

               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }
     
     private String getStockGroupCode(String str)
     {
          int index=-1;
          str = str.trim();
          for(int i=0;i<VStockGroupName.size();i++)
          {
               String ystr = (String)VStockGroupName.elementAt(i);
               if(ystr.startsWith(str))
               {
                    index = i;
                    break;
               }
          }
          if(index == -1)
               return "0";
          return (String)VStockGroupCode.elementAt(index);
     }
     
     public String getQString()
     {
          String QString =    " Select InvItems.Item_Code,InvItems.Item_Name,"+
                              " StockGroup.GroupName,InvItems.UomCode "+
                              " From InvItems Inner Join StockGroup On StockGroup.GroupCode = InvItems.StkGroupCode "+
                              " Where Item_Code Not In (Select Item_Code from "+SItemTable+") ";
          
          String WS      = "";
          if (matpanel.JRSeleDept.isSelected())
          {
               WS = WS+" And InvItems.StkGroupCode = '"+(String)matpanel.VDeptCode.elementAt(matpanel.JCDept.getSelectedIndex())+"'";
          }
          if(((matpanel.TSort.getText()).trim()).length()==0)
               QString = QString+WS+" Order By 2";
          else
               QString = QString+WS+" Order By "+matpanel.TSort.getText();

          return QString;
     }

     protected void finalize() throws Throwable
     {
          theVector           . removeAllElements();
          theVector           = null;

          VUomCode            . removeAllElements();
          VUomCode            = null;

          VGroupCode          . removeAllElements();
          VGroupCode          = null;
          VGroupName          . removeAllElements();
          VGroupName          = null;
          VStockGroupCode     . removeAllElements();
          VStockGroupCode     = null;
          VStockGroupName     . removeAllElements();
          VStockGroupName     = null;

          super          . finalize();

          System.out.println("finalize");
     }


     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}
