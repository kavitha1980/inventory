package vital;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

import jdbc.*;
import util.*;
import guiutil.*;

public class MergeAccept extends JFrame
{
     JButton        BAccept,BDeny;
     
     JPasswordField TA;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
     Common common = new Common();

     MergeAccept()
     {
          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          BottomPanel = new JPanel(true);

          BAccept  = new JButton("Accept");
          BDeny    = new JButton("Deny");

          TA       = new JPasswordField();

          MiddlePanel.setLayout(new GridLayout(1,1));

          BottomPanel.add(BAccept);
          BottomPanel.add(BDeny);

          MiddlePanel.add(TA);
          MiddlePanel.setBorder(new TitledBorder("Password"));

          BAccept.setBackground(Color.cyan);
          BAccept.setForeground(Color.blue);
          BDeny.setBackground(Color.red);
          BDeny.setForeground(Color.white);
          TA.setFont(new Font("monospaced",Font.PLAIN,12));
          TA.setForeground(new Color(130,30,60));
          TA.setBackground(Color.pink);

          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          double x             = screenSize.getWidth();
          double y             = screenSize.getHeight();

          x=x/2;
          y=y/2;
          setSize(280,120);
          setLocation(new Double(x-150).intValue(),new Double(y-90).intValue());
     }

     public boolean isPassValid()
     {
          boolean bFlag = false;

          String SPass = "aaa";

          if(SPass.equals(TA.getText()))
               return true;
          return false;
     }


}
