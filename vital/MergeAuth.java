package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MergeAuth
{
     MergeAccept AD;
     protected        JLayeredPane Layer;
     ItemMergingFrame itemMergingFrame;

     public MergeAuth()
     {
          try
          {
               AD.setVisible(false);
          }
          catch(Exception ex){}

          try
          {
               AD = new MergeAccept();
               AD.show();
               AD.BAccept.addActionListener(new ADList());
               AD.BDeny.addActionListener(new ADList());
          }          
          catch(Exception ex)
          {
               System.out.println("991 "+ex);
               ex.printStackTrace();
          }
     }

     public MergeAuth(JLayeredPane Layer,ItemMergingFrame itemMergingFrame)
     {
          this.Layer            = Layer;
          this.itemMergingFrame = itemMergingFrame;

          try
          {
               AD.setVisible(false);
          }
          catch(Exception ex){}

          try
          {
               AD = new MergeAccept();
               AD.show();
               AD.BAccept.addActionListener(new ADList());
               AD.BDeny.addActionListener(new ADList());
          }          
          catch(Exception ex)
          {
               System.out.println("991 "+ex);
               ex.printStackTrace();
          }
     }

     public static void main(String arg[])
     {
        new MergeAuth();
     }

     public class ADList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(AD.BDeny==ae.getSource())
               {
                    AD.setVisible(false);
                    System.exit(0);
               }
               if(ae.getSource()==AD.BAccept)
               {
                    startMerge();
               }
          }
     }
     public void startMerge()
     {
          if(AD.isPassValid())
          {
               AD.setVisible(false);
               itemMergingFrame.BApply.setEnabled(false);
               itemMergingFrame.startMerging();
          }
          else
          {
               JOptionPane.showMessageDialog(null, "Password is Invalid", "Warning", 
               JOptionPane.ERROR_MESSAGE,new ImageIcon("Warning.gif"));
               AD.TA.setText("");
          }
     }
}
