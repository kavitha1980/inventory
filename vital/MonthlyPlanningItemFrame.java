package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MonthlyPlanningItemFrame extends JInternalFrame
{
     // Global Declarations...

     protected      JLayeredPane             Layer;
     private        int                      iUserCode, iMillCode, iAuthCode;
     private        String                   SItemTable;
     
     private        JPanel                   TopPanel, MiddlePanel, BottomPanel;
     private        JButton                  BSave, BExit;

     private        MyComboBox               JCUsers, JCOrderConversion, JCMatGroup, JCDept;

     private        MonthlyPlanningItemModel theModel;
     public         JTable                   theTable;

     private        Common                   common;

     private        Connection               theConnection;
     private        ArrayList                AUserCode, AUserName;
     private        ArrayList                AOrderTypeCode, AOrderTypeName;
     private        ArrayList                AMatGroupCode, AMatGroupName;
     private        ArrayList                ADeptCode, ADeptName;

     // create Construtors..

     public MonthlyPlanningItemFrame(JLayeredPane Layer, int iMillCode, int iAuthCode, int iUserCode, String SItemTable)
     {
          this      . Layer        = Layer;
          this      . iMillCode    = iMillCode;
          this      . iAuthCode    = iAuthCode;
          this      . iUserCode    = iUserCode;
          this      . SItemTable   = SItemTable;

          common                   = new Common();

          try
          {
               createConnections();
               setArrayList();

               createComponents();
               setLayouts();
               addComponents();

               ExitFrame(this);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
          
     // Create Components, set Layouts and Add Components..

     private void createComponents()
     {
          TopPanel                 = new JPanel();
          MiddlePanel              = new JPanel();
          BottomPanel              = new JPanel();

          BSave                    = new JButton("Save");
          BExit                    = new JButton("Exit");

          JCUsers                  = new MyComboBox(new Vector(AUserName));
          JCOrderConversion        = new MyComboBox(new Vector(AOrderTypeName));
          JCMatGroup               = new MyComboBox(new Vector(AMatGroupName));
          JCDept		           = new MyComboBox(new Vector(ADeptName));

          theModel                 = new MonthlyPlanningItemModel();
          theTable                 = new JTable(theModel);

          TableColumnModel TC      = theTable.getColumnModel();

          for(int i=0; i<theModel.ColumnName.length; i++)
          {
               TC  . getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }

          // set Combo Box in Table Model..

          TableColumn theColumn    = theTable.getColumn("User");
          theColumn                . setCellEditor(new DefaultCellEditor(JCUsers));

          theColumn                = null;

          theColumn                = theTable.getColumn("Order Conversion Type");
          theColumn                . setCellEditor(new DefaultCellEditor(JCOrderConversion));

          theColumn                = null;

          theColumn                = theTable.getColumn("Mat Group");
          theColumn                . setCellEditor(new DefaultCellEditor(JCMatGroup));

          theColumn                = null;

          theColumn                = theTable.getColumn("Department");
          theColumn                . setCellEditor(new DefaultCellEditor(JCDept));


          // AddListeners..

          BSave                    . addActionListener(new ActionList());
          BExit                    . addActionListener(new ActionList());

          theTable                 . addKeyListener(new KeyList(this));
     }
     
     private void setLayouts()
     {
          setTitle("Monthly Planning Item");
          setBounds(10, 20, 800, 470);
          setResizable(true);
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);
                                             
          TopPanel                 . setLayout(new FlowLayout(FlowLayout.LEFT));
          MiddlePanel              . setLayout(new BorderLayout());
          BottomPanel              . setLayout(new FlowLayout());

          TopPanel                 . setBorder(new TitledBorder("Note"));
          MiddlePanel              . setBorder(new TitledBorder("List Of Items"));
          BottomPanel              . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {
          TopPanel                 . add(new JLabel("<html><body><font color='Green'><b>F3 - To Open Inventory Items</b></font></body></html>"));

          MiddlePanel              . add(new JScrollPane(theTable));

          BottomPanel              . add(BSave);
          BottomPanel              . add(BExit);

          getContentPane()         . add("North" , TopPanel);
          getContentPane()         . add("Center", MiddlePanel);
          getContentPane()         . add("South" , BottomPanel);
     }

     // Listener Classes
     
     private class ActionList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BSave)
               {
                    if(isValidInput() && isAlreadySaved())
                    {
                         if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
                         {
                              SaveDetails();
                         }
                    }
               }
               
               if(ae.getSource() == BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private class KeyList extends KeyAdapter
     {
          MonthlyPlanningItemFrame theFrame;

          public KeyList(MonthlyPlanningItemFrame theFrame)
          {
               this.theFrame  = theFrame;
          }

          public void keyReleased(KeyEvent ke)
          {
               int iRow       = theTable.getSelectedRow();

               if(ke.getKeyCode() == KeyEvent.VK_F3)
               {
                    MonthlyPlanningItemSelectionDialog theDialog = new MonthlyPlanningItemSelectionDialog(theFrame, iMillCode, iAuthCode, iUserCode, SItemTable, iRow);

                    theDialog . TItemName.requestFocus();
               }
          }

          public void keyPressed(KeyEvent ke)
          {
               int iRow       = theTable.getSelectedRow();

               if(ke.getKeyCode() == KeyEvent.VK_INSERT)
               {
                    theModel.addRow();
               }

               if(ke.getKeyCode() == KeyEvent.VK_DELETE)
               {
                    theModel.deleteRow(iRow);
               }
          }
     }

     // General Methods And Validations..
     
     private void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . updateUI();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // General Methods..

     private boolean isValidInput()
     {
          // Check if it is Empty Item Code..

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               String SItemCode         = common.parseNull((String)theModel.getValueAt(i, theModel.ITEMCODE));
               String SMatGroup         = common.parseNull((String)theModel.getValueAt(i, theModel.MATGROUP));
               String SUser             = common.parseNull((String)theModel.getValueAt(i, theModel.USER));
               String SOrderConversion  = common.parseNull((String)theModel.getValueAt(i, theModel.CONVTYPE));
               double dTaxPer           = common.toDouble((String)theModel.getValueAt(i, theModel.TAXPER));
               int    iDepCode     	    = getDeptCode((String)theModel.getValueAt(i, theModel.DEPT));
               
               //common.toInt((String)ADeptCode.get(ADeptName.indexOf((String)theModel.getValueAt(i, theModel.DEPT))));

               if(SItemCode.length() == 0 || SMatGroup.length() == 0 || SUser.length() == 0 || SOrderConversion.length() == 0 || dTaxPer == 0 || iDepCode==0)
               {
                    JOptionPane.showMessageDialog(null, "Error in Input.. Pls Correct it.. Row No. "+(i+1), "Information", JOptionPane.ERROR_MESSAGE);
                    setTableSelection(i);
                    return false;
               }
          }

          // Boolean Validation...

          for(int i=0; i<theModel.getRowCount(); i++)
          {
               Boolean bValue           = (Boolean)theModel.getValueAt(i, theModel.SELECT);

               if(bValue.booleanValue())
               {
                    return true;
               }
          }

          JOptionPane.showMessageDialog(null, "No Data Selected", "Information", JOptionPane.ERROR_MESSAGE);
          setTableSelection(0);
          return false;
     }

     private boolean isAlreadySaved()
     {
          boolean bFlag                      = true;
          String SSlNos                      = "";
          
          String SErrorMsg                   = "";
		  	
          for(int i=0; i<theModel.getRowCount(); i++)
          {
               Boolean bValue                = (Boolean)theModel.getValueAt(i, theModel.SELECT);

               if(bValue.booleanValue())
               {
                    String SItemCode         = common.parseNull((String)theModel.getValueAt(i, theModel.ITEMCODE));
                    int    iModelUserCode    = common.toInt((String)AUserCode.get(AUserName.indexOf((String)theModel.getValueAt(i, theModel.USER))));
					int    iDepCode     	 = getDeptCode((String)theModel.getValueAt(i, theModel.DEPT));
					
                    int iCount               = getCount(SItemCode, iModelUserCode);
					double dBudget           = isBudgetAvailable(iDepCode, iModelUserCode);
					
					if(dBudget<=0) {
						bFlag   = false;
						SErrorMsg += String.valueOf(i+1)+",";
						setTableSelection(i);
					}
					
                    if(iCount > 0 || iCount == -1)
                    {
                         bFlag               = false;
                         SSlNos             += String.valueOf(i+1)+",";

                         setTableSelection(i);
                    }
               }
          }

          if(SSlNos.length() > 0)
          {
               JOptionPane.showMessageDialog(null, "Some Data already Stored.. Pls Delete them..\n Row No(s) : "+SSlNos.substring(0, SSlNos.length()-1), "Information", JOptionPane.ERROR_MESSAGE);
          }
          
          if(SErrorMsg.length()>0) {
				JOptionPane.showMessageDialog(null, " Budget not available..\n  Row No(s) : "+SErrorMsg.substring(0, SErrorMsg.length()-1), "Information", JOptionPane.ERROR_MESSAGE);
          }

          return bFlag;
     }

     private void setTableSelection(int i)
     {
          try
          {
               theTable            . setRowSelectionInterval(i, i);
               Rectangle theRect   = theTable.getCellRect(i, 0, true);
               theTable            . scrollRectToVisible(theRect);
          }catch(Exception ex){}
     }

     // New Methods...

     private void ExitFrame(final MonthlyPlanningItemFrame theFrame)
     {
          KeyStroke escapeKeyStroke     = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);

          Action escapeAction           = new AbstractAction()
          {
              // close the frame when the user presses escape

              public void actionPerformed(ActionEvent e)
              {
                  theFrame              . dispose();
              }
          };

          theFrame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "ESCAPE");
          theFrame.getRootPane().getActionMap().put("ESCAPE", escapeAction);
     }

     // Connecting With DB....

     private void createConnections()
     {
          theConnection                 = null;

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "inventory", "stores");
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // set ArrayList Values...

     private void  setArrayList()
     {
          AUserCode                     = null;
          AUserName                     = null;
          AOrderTypeCode                = null;
          AOrderTypeName                = null;
          AMatGroupCode                 = null;
          AMatGroupName                 = null;
		  ADeptCode		                = null;
		  ADeptName                     = null;
		  
          AUserCode                     = new ArrayList();
          AUserName                     = new ArrayList();
          AOrderTypeCode                = new ArrayList();
          AOrderTypeName                = new ArrayList();
          AMatGroupCode                 = new ArrayList();
          AMatGroupName                 = new ArrayList();
		  ADeptCode		                = new ArrayList();
		  ADeptName                     = new ArrayList();

          AUserCode                     . clear();
          AUserName                     . clear();
          AOrderTypeCode                . clear();
          AOrderTypeName                . clear();
          AMatGroupCode                 . clear();
          AMatGroupName                 . clear();
		  ADeptCode		                . clear();
		  ADeptName                     . clear();


          try
          {
               PreparedStatement thePre = theConnection.prepareCall("Select TypeCode, TypeName from OrderConversionType Order by 2");
               ResultSet rs             = thePre.executeQuery();
               while(rs.next())
               {
                    AOrderTypeCode      . add(common.parseNull(rs.getString(1)));
                    AOrderTypeName      . add(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               thePre                   . close();

               thePre                   = theConnection.prepareCall("Select UserCode, UserName from RawUser where UserCode in (Select distinct AuthUserCode from MRSUserAuthentication) Order by 2");
               rs                       = thePre.executeQuery();     
               while(rs.next())
               {
                    AUserCode           . add(common.parseNull(rs.getString(1)));
                    AUserName           . add(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               thePre                   . close();

               thePre                   = theConnection.prepareCall("Select GroupCode, GroupName from MatGroup Order by 2");
               rs                       = thePre.executeQuery();
               while(rs.next())
               {
                    AMatGroupCode       . add(common.parseNull(rs.getString(1)));
                    AMatGroupName       . add(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               thePre                   . close();
               
               thePre                   = theConnection.prepareCall("Select Dept_Code, Dept_Name from Dept Order by 2 ");
               rs                       = thePre.executeQuery();
               while(rs.next())
               {
                    ADeptCode       . add(common.parseNull(rs.getString(1)));
                    ADeptName       . add(common.parseNull(rs.getString(2)));
               }
               rs                       . close();
               thePre                   . close();
               
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // get Count i.e. if it is already Saved..
     
     private int getCount(String SItemCode, int iModelUserCode)
     {
          try
          {
               int iCount               = 0;
			
			StringBuffer SB = null;
			SB = new StringBuffer();
			
			SB.append(" Select Count(*) from MonthlyPlanningItem "+
                                                                    " where MillCode = "+iMillCode+" and UserCode = "+iModelUserCode+" "+
                                                                    " and Item_Code = '"+SItemCode+"' ");
													   
			System.out.println(SB);										   

               PreparedStatement thePre = theConnection.prepareStatement(SB.toString());
               ResultSet rs             = thePre.executeQuery();

               if(rs.next())
               {
                    iCount              = rs.getInt(1);
               }

               rs                       . close();
               thePre                   . close();

               return iCount;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return -1;
          }
     }

     private double isBudgetAvailable(int iDeptCode, int iModelUserCode)
     {
     	double dBudgetVal  = 0;
          try
          {
               

               PreparedStatement thePre = theConnection.prepareCall(" Select nvl(sum(budget),0) from Budget "+
                                                                    " where MillCode = "+iMillCode+" and UserCode = "+iModelUserCode+" "+
                                                                    " and DeptCode = '"+iDeptCode+"' And WITHEFFECTTO = '99999999' ");
               ResultSet rs             = thePre.executeQuery();

               if(rs.next())
               {
                    dBudgetVal          = common.toDouble(rs.getString(1));
               }
               rs                       . close();
               thePre                   . close();
               
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return -1;
          }
          
          return dBudgetVal;
     }

     // Save Details...

     private void SaveDetails()
     {
          try
          {
               theConnection                      . setAutoCommit(false);

               for(int i=0; i<theModel.getRowCount(); i++)
               {
                    Boolean bValue                = (Boolean)theModel.getValueAt(i, theModel.SELECT);
     
                    if(bValue.booleanValue())
                    {
                         String SItemCode         = common.parseNull((String)theModel.getValueAt(i, theModel.ITEMCODE));
                         String SItemName         = common.parseNull((String)theModel.getValueAt(i, theModel.ITEMNAME));
                         int iDepCode     	      = getDeptCode((String)theModel.getValueAt(i, theModel.DEPT));
                         int iMatGroupCode        = common.toInt((String)AMatGroupCode.get(AMatGroupName.indexOf((String)theModel.getValueAt(i, theModel.MATGROUP))));
                         int iModelUserCode       = common.toInt((String)AUserCode.get(AUserName.indexOf((String)theModel.getValueAt(i, theModel.USER))));
                         double dPlusOrMinusPer   = common.toDouble((String)theModel.getValueAt(i, theModel.PERC));
                         int iOrderConTypeCode    = common.toInt((String)AOrderTypeCode.get(AOrderTypeName.indexOf((String)theModel.getValueAt(i, theModel.CONVTYPE))));
                         double dTaxPer           = common.toDouble((String)theModel.getValueAt(i, theModel.TAXPER));
                         double dCenVatPer        = common.toDouble((String)theModel.getValueAt(i, theModel.CENVATPER));
                         double dSurPer           = common.toDouble((String)theModel.getValueAt(i, theModel.SURPER));

                         // executing Query..

                         PreparedStatement thePre = theConnection.prepareCall(getInsertQS(SItemCode, SItemName, iMatGroupCode, iModelUserCode, dPlusOrMinusPer, iOrderConTypeCode, dTaxPer, dCenVatPer, dSurPer, iDepCode));

                         thePre                   . executeQuery();
                         thePre                   . close();
                    }
               }

               theConnection                      . commit();
               theConnection                      . setAutoCommit(true);

               JOptionPane.showMessageDialog(null, "Data Saved Sucessfully..", "Information", JOptionPane.INFORMATION_MESSAGE);
               theModel.setNumRows(0);
          }
          catch(Exception ex)
          {
               try
               {
                    theConnection                 . rollback();
                    theConnection                 . setAutoCommit(true);
               }catch(Exception e){}

               ex.printStackTrace();

               JOptionPane.showMessageDialog(null, "Data Not Saved.. Try Again.."+ex.getMessage().toString(), "Error", JOptionPane.ERROR_MESSAGE);
          }
     }

     private String getInsertQS(String SItemCode, String SItemName, int iMatGroupCode,
                                int iModelUserCode, double dPlusOrMinusPer, int iOrderConTypeCode,
                                double dTaxPer, double dCenVatPer, double dSurPer, int iDeptCode)
     {
          StringBuffer SB          = null;
          SB                       = new StringBuffer();

          SB.append(" Insert Into MonthlyPlanningItem ");
          SB.append(" ( ");
          SB.append(" Item_Code, Item_Name, MatGroupCode, UserCode, MillCode, ");
          SB.append(" PlusOrMinusPer, OrderConversionTypeCode, TaxPer, ");
          SB.append(" CenVatPer, SurPer, EntryDateTime, EntryUserCode, ITEMDEPTCODE ");
          SB.append(" ) ");
          SB.append(" Values ");
          SB.append(" ( ");
          SB.append(" '"+SItemCode+"', '"+SItemName+"', "+iMatGroupCode+", "+iModelUserCode+", "+iMillCode+", ");
          SB.append(" "+dPlusOrMinusPer+", "+iOrderConTypeCode+", "+dTaxPer+", ");
          SB.append(" "+dCenVatPer+", "+dSurPer+", to_Char(SysDate, 'DD.MM.YYYY HH24:MI:SS'), "+iUserCode+", "+iDeptCode+" ");
          SB.append(" ) ");

          return SB.toString();
     }
     
     
     private int getDeptCode(String SDeptName) {
     
     	int ind = ADeptName.indexOf(SDeptName);
     	
     	if(ind!=-1){
     		return common.toInt((String)ADeptCode.get(ind));
     	}
     	
     	return 0;
     }
}
