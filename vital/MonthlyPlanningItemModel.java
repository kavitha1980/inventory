package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class MonthlyPlanningItemModel extends DefaultTableModel
{
     String ColumnName[] = {"Sl.No.", "Item Code", "Item Name", "UOM", "Department", "Mat Group", "User", "Plus/Minus Per", "Order Conversion Type", "TaxPer", "Cenvat Per", "Sur Per", "Click"};
     String ColumnType[] = {"S"     , "S"        , "S"        , "S"  , "E"        , "E"        , "E"   , "E"             , "E"                    , "E"     , "E"         , "E"      , "B"    };
     int  iColumnWidth[] = {10      , 40         , 120        , 30   , 130        , 130        , 150   , 50              , 60                     , 40      , 40          , 40       , 10     };


	 int 	SINO     = 0;
	 int 	ITEMCODE = 1;
	 int 	ITEMNAME = 2;
	 int 	UOM      = 3;
	 int 	DEPT     = 4;
	 int 	MATGROUP = 5;
	 int 	USER     = 6;
	 int 	PERC     = 7;
	 int 	CONVTYPE = 8;
	 int 	TAXPER   = 9;
	 int 	CENVATPER= 10;
	 int 	SURPER   = 11;
	 int 	SELECT   = 12;
	 
     public MonthlyPlanningItemModel()
     {
          setDataVector(getRowData(), ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0; i<ColumnName.length; i++)
          {
               if(i == SINO)
               {
                    RowData[0][i] = "1";
               }
               else if(i == CONVTYPE)
               {
                    RowData[0][i] = "MANUAL";
               }
               else if(i == TAXPER)
               {
                    RowData[0][i] = "5";
               }
               else if(i == SELECT)
               {
                    RowData[0][i] = new Boolean(true);
               }
               else
               {
                    RowData[0][i] = "";
               }
          }
          
          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(), theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }

     public void addRow()
     {
          Vector theVect      = null;
          theVect             = new Vector();
          theVect             . removeAllElements();

          for(int i=0; i<ColumnName.length; i++)
          {
               if(i == CONVTYPE)
               {
                    theVect   . addElement("MANUAL");
               }
               else if(i == TAXPER)
               {
                    theVect   . addElement("5");
               }
               else if(i == SELECT)
               {
                    theVect   . addElement(new Boolean(true));
               }
               else
               {
                    theVect   . addElement("");
               }
          }
          
          insertRow(getRows(), theVect);
          setSlNos();
     }

     public void deleteRow(int iRow)
     {
          if(getRowCount() == 1 || iRow == -1)
          {
               return;
          }

          removeRow(iRow);
          setSlNos();
     }

     private void setSlNos()
     {
          for(int i=0; i<getRowCount(); i++)
          {
               setValueAt(String.valueOf(i+1), i, SINO);
          }
     }
}
