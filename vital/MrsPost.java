package vital;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.Vector;
import java.sql.*;
import java.awt.event.*;

import guiutil.*;
import util.*;
import jdbc.*;


public class MrsPost extends JFrame
{
     JPanel TopPanel,MiddlePanel,BottomPanel;
     String mac      = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
     Common common   = new Common();

     AGNModel theModel;
     JTable   theTable;

     Object RowData[][];
     String ColumnData[] = {"Block","Order No","Date","Code","Name","MRS No","Supplier"};
     String ColumnType[] = {"S","N","S","N","S","E","S"};

     Vector    VNo,VBlock,VDate,VCode,VItem,VSup,VId,VMRSNo;
     DateField TStDate,TEnDate;
     JButton   BApply,BPost;
     String    SStDate,SEnDate;

     int iMillCode;
     String SSupTable;
     Connection theConnection = null;

     public MrsPost(int iMillCode)
     {
          this.iMillCode = iMillCode;

          createComponents();
     }

     public MrsPost(int iMillCode,String SSupTable)
     {
          this.iMillCode = iMillCode;
          this.SSupTable = SSupTable;

          createComponents();
     }

     private void createComponents()
     {
          updateLookAndFeel();
          setTitle("MRS match to Orders");
          setSize(500,500);

          TopPanel    = new JPanel(true);
          MiddlePanel = new JPanel(true);
          BottomPanel = new JPanel(true);

          TStDate     = new DateField();
          TEnDate     = new DateField();

          TStDate.setTodayDate();
          TEnDate.setTodayDate();

          BApply      = new JButton("Apply");
          BPost       = new JButton("Post MRS");

          BPost.setEnabled(false);

          TopPanel.add(new JLabel("From : "));
          TopPanel.add(TStDate);
          TopPanel.add(new JLabel("To : "));
          TopPanel.add(TEnDate);
          TopPanel.add(BApply);

          BottomPanel.add(BPost);

          BApply.addActionListener(new AppList());
          BPost.addActionListener(new PostList());

          MiddlePanel.setLayout(new BorderLayout());

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          addWindowListener(new WinList());
          setVisible(true);
     }
      private class PostList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               BPost.setEnabled(false);
               setDataIntoMRSTable();
               JOptionPane.showMessageDialog(null,"Posting Over","Information",JOptionPane.INFORMATION_MESSAGE);
          }
      }

      private class AppList implements ActionListener
      {
          public void actionPerformed(ActionEvent ae)
          {
               SStDate = TStDate.toNormal();
               SEnDate = TEnDate.toNormal();

               try
               {
                    MiddlePanel.removeAll();
               }catch(Exception ex){System.out.println(ex);}
               setTable();
               BPost.setEnabled(true);
          }
      }

     private void fetchData()
     {
          VNo    = new Vector();
          VBlock = new Vector();
          VDate  = new Vector();
          VCode  = new Vector();
          VItem  = new Vector();
          VSup   = new Vector();
          VId    = new Vector();
          VMRSNo = new Vector();

          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
                ResultSet theResult      = theStatement.executeQuery(getQS());
                while(theResult.next())
                {
                    VNo    .addElement(theResult.getString(1));
                    VBlock .addElement(getBlock(theResult.getInt(2)));
                    VDate  .addElement(common.parseDate(theResult.getString(3)));
                    VCode  .addElement(theResult.getString(4));
                    VItem  .addElement(theResult.getString(5));
                    VSup   .addElement(theResult.getString(6));
                    VId    .addElement(theResult.getString(7));
                    VMRSNo .addElement(theResult.getString(8));
                }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getBlock(int iBlock)
     {
          String SBlock = " ";

          if(iBlock==0)
               SBlock = "SST";
          else if(iBlock==1)
               SBlock = "SSP";
          else if(iBlock==2)
               SBlock = "NST";
          else if(iBlock==3)
               SBlock = "NSP";
          else if(iBlock==4)
               SBlock = "NPR";
          return SBlock;
     }
     private String getQS()
     {
          String QS = " ";

          QS = " SELECT PurchaseOrder.OrderNo, PurchaseOrder.OrderBlock, "+
               " PurchaseOrder.OrderDate, PurchaseOrder.Item_Code, InvItems.ITEM_NAME, "+
               " "+SSupTable+".NAME, PurchaseOrder.ID, PurchaseOrder.MrsNo "+
               " FROM (PurchaseOrder INNER JOIN "+SSupTable+" ON PurchaseOrder.Sup_Code = "+SSupTable+".AC_CODE) "+
               " INNER JOIN InvItems ON PurchaseOrder.Item_Code = InvItems.ITEM_CODE "+
               " WHERE purchaseorder.orderdate>='"+SStDate+"' and "+
               " purchaseorder.orderdate<='"+SEnDate+"' And "+
               " purchaseorder.millcode="+iMillCode+
               " and PurchaseOrder.MrsNo=0 and PurchaseOrder.InvQty=0 "+
               " ORDER BY 2, 1 ";

          return QS;
     }

     public void updateLookAndFeel()
     {
          String currentLookAndFeel = mac;
          try
          {
               UIManager.setLookAndFeel(currentLookAndFeel);
          }
          catch (Exception ex)
          {
               System.out.println("Failed loading L&F: " + currentLookAndFeel);
               System.out.println(ex);
          }
     }

     private void setTable()
     {
          fetchData();
          setRowData();

          if(VNo.size()<=0)
          {
               JOptionPane.showMessageDialog(null,"No Data Available ","Error Message",JOptionPane.INFORMATION_MESSAGE);
               setRowData();
//               theTable.updateUI();
               return;
          }
          RowData = new Object[VNo.size()][ColumnData.length];
          for(int i=0;i<VNo.size();i++)
          {
               RowData[i][0] = (String)VBlock.elementAt(i);
               RowData[i][1] = (String)VNo.elementAt(i);
               RowData[i][2] = (String)VDate.elementAt(i);
               RowData[i][3] = (String)VCode.elementAt(i);            
               RowData[i][4] = (String)VItem.elementAt(i);
               RowData[i][5] = (String)VMRSNo.elementAt(i);
               RowData[i][6] = (String)VSup.elementAt(i);
          }

          theModel  = new AGNModel(RowData,ColumnData,ColumnType);
          theTable  = new JTable(theModel);

          MiddlePanel.setLayout(new BorderLayout());
          MiddlePanel.add("North",theTable.getTableHeader());
          MiddlePanel.add("Center",new JScrollPane(theTable));
          repaint();
          MiddlePanel.updateUI();
     }
     private void setRowData()
     {
           RowData = new Object[1][ColumnData.length];
           for(int i=0;i<ColumnData.length-1;i++)
                 RowData[0][i]="";
     }
     public class WinList extends WindowAdapter
     {
         public void windowClosing(WindowEvent we)
         {
            System.exit(0);
         }
     }
     /*private void setDataIntoTable()
     {
          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
                for(int i=0;i<VNo.size();i++)
                {
                    int iMrs  = common.toInt((String)theModel.getValueAt(i,5));
                    int iOMrs = common.toInt((String)VMRSNo.elementAt(i));
                    if(iMrs<=0 || iMrs==iOMrs)
                         continue;

                    String QS = " UPDATE PurchaseOrder SET "+
                                " MrsNo="+theModel.getValueAt(i,5)+
                                " WHERE id="+(String)VId.elementAt(i);

                    theStatement.execute(QS);
                }
                theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }*/
     private void setDataIntoMRSTable()
     {
          String QS="";
          try
          {
                 if(theConnection == null)
                 {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
                  }
                  Statement theStatement    = theConnection.createStatement();
                for(int i=0;i<VNo.size();i++)
                {
                    int     iMrs  = common.toInt((String)theModel.getValueAt(i,5));
                    String SOrdNo = (String)VNo.elementAt(i);
                    String SDate  = (String)VDate.elementAt(i);
                    String SBlock = (String)VBlock.elementAt(i);
                    String SCode  = (String)VCode.elementAt(i);

                    if(iMrs<=0)
                         continue;

                    QS = " UPDATE MRS SET "+
                           " OrderNo="+SOrdNo+","+
                           " OrderBlock="+getOrdBlock(SBlock)+
                           " WHERE MrsNo="+iMrs+" AND MillCode="+iMillCode+
                           " And Item_Code='"+SCode+"'";

                    theStatement.execute(QS);
                }
                theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("99 : "+ex);
               System.out.println(QS);
          }
     }
     private int getOrdBlock(String SBlock)
     {
          int iBlock = 0;

          if((SBlock.trim()).startsWith("SST"))
               iBlock = 0;
          else if((SBlock.trim()).startsWith("SSP"))
               iBlock = 1;
          else if((SBlock.trim()).startsWith("NST"))
               iBlock = 2;
          else if((SBlock.trim()).startsWith("NSP"))
               iBlock = 3;
          else if((SBlock.trim()).startsWith("NPR"))
               iBlock = 4;

          return iBlock;
     }

     public static void main(String arg[])
     {
          int iMillCode=0;
          new MrsPost(iMillCode);
     }
}
