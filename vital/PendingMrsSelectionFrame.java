package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import util.*;
import jdbc.*;
import guiutil.*;

public class PendingMrsSelectionFrame extends JInternalFrame
{
     MyComboBox      JCMrsNo;
     JLabel          LOrderNo,LItemCode,LItemName;

     JButton         BOk,BCancel;
     JPanel          TopPanel,BottomPanel;

     JLayeredPane    Layer;
     JTable          ReportTable;
     int             iMillCode;
     Vector          VPMrsNo,VPMrsSlNo,VPMrsUserCode,VPMrsId;
     MRSPostingFrame mrspostingframe;

     Common common = new Common();

     PendingMrsSelectionFrame(JLayeredPane Layer,JTable ReportTable,int iMillCode,Vector VPMrsNo,Vector VPMrsSlNo,Vector VPMrsUserCode,Vector VPMrsId,MRSPostingFrame mrspostingframe)
     {
          this.Layer           = Layer;
          this.ReportTable     = ReportTable;
          this.iMillCode       = iMillCode;
          this.VPMrsNo         = VPMrsNo;
          this.VPMrsSlNo       = VPMrsSlNo;
          this.VPMrsUserCode   = VPMrsUserCode;
          this.VPMrsId         = VPMrsId;
          this.mrspostingframe = mrspostingframe;

          createComponents();
          setLayouts();
          addComponents();
          setPresets();
          addListeners();
     }
     public void createComponents()
     {
          JCMrsNo             = new MyComboBox(VPMrsNo);
          LOrderNo            = new JLabel("");
          LItemCode           = new JLabel("");
          LItemName           = new JLabel("");
          
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          TopPanel            = new JPanel(true);
          BottomPanel         = new JPanel(true);
     }
     public void setLayouts()
     {
          setBounds(0,0,400,250);
          setResizable(true);
          setClosable(true);
          setTitle("MRS Link Frame");

          TopPanel.setLayout(new GridLayout(4,2));
          BottomPanel.setLayout(new FlowLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new MyLabel("OrderNo"));
          TopPanel.add(LOrderNo);

          TopPanel.add(new MyLabel("ItemCode"));
          TopPanel.add(LItemCode);


          TopPanel.add(new MyLabel("ItemName"));
          TopPanel.add(LItemName);

          TopPanel.add(new MyLabel("MrsNo"));
          TopPanel.add(JCMrsNo);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk       .addActionListener(new ActList());
          BCancel   .addActionListener(new ActList());
     }
     public void setPresets()
     {
          int i = ReportTable.getSelectedRow();
          String SOrderNo   = (String)ReportTable.getModel().getValueAt(i,1);
          String SCode      = (String)ReportTable.getModel().getValueAt(i,3);
          String SName      = (String)ReportTable.getModel().getValueAt(i,4);
          String SMrsNo     = (String)ReportTable.getModel().getValueAt(i,5);
          
          LOrderNo.setText(SOrderNo);
          LItemCode.setText(SCode);
          LItemName.setText(SName);

          if(common.toInt(SMrsNo)>0)
               JCMrsNo.setSelectedItem(SMrsNo);
     }
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    int i = ReportTable.getSelectedRow();

                    int iIndex = JCMrsNo.getSelectedIndex();

                    String SMrsNo       = (String)VPMrsNo.elementAt(iIndex);
                    String SMrsSlNo     = (String)VPMrsSlNo.elementAt(iIndex);
                    String SMrsUserCode = (String)VPMrsUserCode.elementAt(iIndex);
                    String SMrsId       = (String)VPMrsId.elementAt(iIndex);
                    ReportTable.getModel().setValueAt(SMrsNo,i,5);
                    mrspostingframe.VMrsNo.setElementAt(SMrsNo,i);
                    mrspostingframe.VMrsSlNo.setElementAt(SMrsSlNo,i);
                    mrspostingframe.VMrsUserCode.setElementAt(SMrsUserCode,i);
                    mrspostingframe.VSeleId.addElement(SMrsId);
               }
               removeHelpFrame();
               ReportTable.requestFocus();
          }
     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex) { }
     }


}
