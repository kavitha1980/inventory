package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class StationaryFrame extends JInternalFrame
{
     JTextField     TMatCode,TMatName;
     JTextField     TStationaryColor,TStationarysize;
     JComboBox      JStationarySet,JStationaryside;

     JButton        BOk,BCancel;
     
     JPanel         TopPanel,BottomPanel;
     JLayeredPane   Layer;
     JTable         ReportTable;

     String         SItemCode = "",SItemName = "";
     String         SPColour  = "",SPSet     = "",SPSize    = "",SPSide    = "";

     Vector         VPaperSet,VSetCode,VSideName,VSideCode;

     Common         common         = new Common();
     Connection     theConnection  = null;

     StationaryFrame(JLayeredPane Layer,String SItemCode,String SItemName,String SPColour,String SPSet,String SPSize,String SPSide)
     {
          this.Layer          = Layer;
          this.SItemName      = SItemName;
          this.SItemCode      = SItemCode;
          this.SPColour       = SPColour;
          this.SPSet          = SPSet;
          this.SPSize         = SPSize;
          this.SPSide         = SPSide;

          getSetSide();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     
     public void createComponents()
     {
          BOk                 = new JButton("Okay");
          BCancel             = new JButton("Cancel");
          
          TopPanel            = new JPanel(true);
          BottomPanel         = new JPanel(true);
          
          TMatCode            = new JTextField(SItemCode);
          TMatName            = new JTextField(SItemName);
          
          TStationaryColor    = new JTextField(SPColour);
          TStationarysize     = new JTextField(SPSize);

          JStationarySet      = new JComboBox(VPaperSet);
          JStationaryside     = new JComboBox(VSideName);

          JStationarySet      . setSelectedIndex(common.toInt(SPSet));
          JStationaryside     . setSelectedIndex(common.toInt(SPSide));

          TMatCode            . setEditable(false);
          TMatName            . setEditable(false);
     }
     
     public void setLayouts()
     {
          setBounds(80,100,450,350);
          setResizable(true);
          setTitle("Stationary Details");
          TopPanel.setLayout(new GridLayout(6,2));
     }
     
     public void addComponents()
     {
          TopPanel            . add(new JLabel("Material Name"));
          TopPanel            . add(TMatName);
          TopPanel            . add(new JLabel("Material Code"));
          TopPanel            . add(TMatCode);

          TopPanel            . add(new JLabel("Paper Color"));
          TopPanel            . add(TStationaryColor);

          TopPanel            . add(new JLabel("Paper Set"));
          TopPanel            . add(JStationarySet);

          TopPanel            . add(new JLabel("Paper Size"));
          TopPanel            . add(TStationarysize);

          TopPanel            . add(new JLabel("Paper Side"));
          TopPanel            . add(JStationaryside);
          
          BottomPanel         . add(BOk);
          BottomPanel         . add(BCancel);
          
          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("South",BottomPanel);
     }
     
     public void addListeners()
     {
          BOk       . addActionListener(new ActList());
          BCancel   . addActionListener(new ActList());
     }
     
     public class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    UpdateData();
                    removeHelpFrame();
               }
               if(ae.getSource()==BCancel)
               {
                    removeHelpFrame();
               }
          }
     }
     
     public void UpdateData()
     {
          String SUpQuery     = "";

          String    SAPColour = ((String)TStationaryColor    . getText()).toUpperCase();
          String    SAPSize   = ((String)TStationarysize     . getText()).toUpperCase();

          String    SAPSide   = (String)VSideCode . elementAt(JStationaryside    . getSelectedIndex());
          String    SAPSet    = (String)VSetCode  . elementAt(JStationarySet     . getSelectedIndex());

                    SUpQuery  = " update invitems set PAPERCOLOR ='"+SAPColour+"',PAPERSETS ='"+SAPSet+"',PAPERSIZE ='"+SAPSize+"',PAPERSIDE = '"+SAPSide+"' where invitems.item_Code ='"+SItemCode+"'";
          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
                              theConnection  . setAutoCommit(true);
               Statement      stat           = theConnection.createStatement();
                              stat           . executeUpdate(SUpQuery);
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . repaint();
               Layer     . updateUI();
          }
          catch(Exception ex) { }
     }

     public void getSetSide()
     {
          ResultSet result    = null;

          VPaperSet           = new Vector();
          VSetCode            = new Vector();
          VSideName           = new Vector();
          VSideCode           = new Vector();

          try
          {
               ORAConnection  oraConnection  = ORAConnection.getORAConnection();
               Connection     theConnection  = oraConnection.getConnection();
                              theConnection  . setAutoCommit(true);
               Statement      stat           = theConnection.createStatement();
                              result         = stat.executeQuery("Select SetName,SetCode From PaperSet Order By SetCode");

               while(result.next())
               {
                    VPaperSet . addElement(result.getString(1));
                    VSetCode  . addElement(result.getString(2));
               }
               result    . close();

               result    = stat.executeQuery("Select SideName,SideCode From PaperSide Order By SideCode");

               while(result.next())
               {
                    VSideName . addElement(result.getString(1));
                    VSideCode . addElement(result.getString(2));
               }
               result    . close();
               stat      . close();
          }
          catch(Exception ex)
          {
               System.out.println("PaperSet & PaperSide :"+ex);
          }
     }

     protected void finalize() throws Throwable
     {
          this.Layer          = null;
          this.SItemName      = null;
          this.SItemCode      = null;
          this.SPColour       = null;
          this.SPSet          = null;
          this.SPSize         = null;
          this.SPSide         = null;

          VPaperSet           . removeAllElements();
          VPaperSet           = null;
          VSetCode            . removeAllElements();
          VSetCode            = null;
          VSideName           . removeAllElements();
          VSideName           = null;
          VSideCode           . removeAllElements();
          VSideCode           = null;

          super          . finalize();
         //System.out.println("finalizeStationaryframe");
     }
}
