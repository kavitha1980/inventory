package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class StockGroupMaster extends JInternalFrame 
{

     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TStockGroup;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VGroupName,VSGroupName;
     Common         common = new Common();

     //MyComboBox     JCBranch;

     Connection theConnection = null;

     int iUserCode=0;
     int iMillCode=0;

     public  StockGroupMaster(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;
          setVectorSample();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void createComponents()
     {
          thePane        = new JTabbedPane();
          TopPanel       = new JPanel();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          TStockGroup    = new AddressField(30);
          VGroupName      = new Vector();
          VSGroupName     = new Vector();
          //JCBranch       = new MyComboBox();

          //JCBranch.addItem("AmarjothiSpinningMill");
          //JCBranch.addItem("AmarjothiDyeingDivision");
          //JCBranch.addItem("Both");

          BOkay          = new MyButton("Save");
          BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("Stock Group Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }
     private void addComponents()
     {

          setVector();
          setTabReport();

          // TopPanel Controls
          TopPanel.add(new JLabel("Group Name"));
          TopPanel.add(TStockGroup);


          MiddlePanel.add("Center",thePane);
          thePane.addTab("Stock Group List",theReport);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');

     }
     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"GROUP NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VGroupName.size()][ColumnName.length];
               for(int i=0;i<VGroupName.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VGroupName.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("Group List ",theReport);
                    else
                         thePane.addTab("Group List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TStockGroup.setText("");
                    TStockGroup.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {

               String SCount = getExistName(TStockGroup.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Group Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    TStockGroup.requestFocus();
                    return false;
               }

               if(common.parseNull(TStockGroup.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Group Name Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TStockGroup.requestFocus();
                    return false;
               }

               if(TStockGroup.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid Group Name! ","Error",JOptionPane.ERROR_MESSAGE);
                    TStockGroup.requestFocus();
                    TStockGroup.setText("");
                    return false;
               }
               if(TStockGroup.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"Group Name must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TStockGroup.requestFocus();
                    TStockGroup.setText("");
                    return false;
               } 
               return true;
          }

          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          try
          {
               String SGroupCode        = "";
               int iGroupCode           = getMaxGroupCode();
               if(iGroupCode<10)   {
                    SGroupCode     =    "A0"+String.valueOf(iGroupCode);                    
               }
               else {
                    SGroupCode     =    "A"+String.valueOf(iGroupCode);                    
               }

               String SGroupName   =    TStockGroup.getText().trim().toUpperCase();

               inserTStockGroup(SGroupCode,SGroupName,iMillCode,iUserCode);
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Group Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     public void setVector()
     {
          VGroupName   = new Vector();

          String QS   = "Select GroupName from StockGroup ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VGroupName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void setVectorSample()
     {
          VSGroupName   = new Vector();

          String QS   = "Select GroupName from StockGroup ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VSGroupName.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     public void inserTStockGroup(String SGroupCode,String SGroupName,int iMillCode,int iUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInserTStockGroupQS(SGroupCode,SGroupName,iMillCode,iUserCode));
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getInserTStockGroupQS(String SGroupCode,String SGroupName,int iMillCode,int iUserCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into StockGroup(id,GroupCode,GroupName,MillCode,UserCode,CreationDate) Values (";
               QS = QS+"  StockGroup_seq.nextval ,";
               QS = QS+" '"+SGroupCode+"',";
               QS = QS+" '"+SGroupName+"',";
               QS = QS+" "+iMillCode+" ,";
               QS = QS+" "+iUserCode+" ,";
               QS = QS+" '"+common.getServerDate()+"' ) ";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Stock group "+e);
          }
          return QS;
     }
     public  int getMaxGroupCode()
     {
          int iMaxNo=0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getMaxGroupQS());
               while(theResult.next())
               {
                    iMaxNo =    theResult.getInt(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMaxNo;
     }
     private String getMaxGroupQS()
     {
          String QS = " Select Max(substr(GroupCode,2,length(groupcode)))+1 from StockGroup ";
          return QS;
     }
     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select GroupName from StockGroup Where GroupName='"+SName+"' ";
          return QS;
     }



}

