package vital;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class SupPanel extends JPanel
{
    JPanel GroupPanel;
    JPanel FinPanel;
    JPanel CityPanel;
    JPanel ApplyPanel;
    JPanel SortPanel;

    JTextField   TSort,TFind;

    Common common = new Common();

    Connection theConnection = null;

    Vector VGroup,VGroupCode,VFin,VFinCode,VCity,VCityCode;
    JComboBox JCGroup,JCFin,JCCity;

    JRadioButton JRAllGroup,JRSeleGroup;
    JRadioButton JRAllFin,JRSeleFin;
    JRadioButton JRAllCity,JRSeleCity;

    JButton      BApply;

    SupPanel()
    {
        getGroupFinCity();     
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }
    public void createComponents()
    {
        CityPanel   = new JPanel();
        ApplyPanel  = new JPanel();
        SortPanel   = new JPanel();


        JRAllCity   = new JRadioButton("All",true);
        JRSeleCity  = new JRadioButton("Selected");
        JCCity      = new JComboBox(VCity);

        JCCity.setEnabled(false);

        TSort      = new JTextField();
        TFind      = new JTextField();

        BApply      = new JButton("Apply");
          
    }
    public void setLayouts()
    {
        setLayout(new GridLayout(1,5));
        CityPanel.setLayout(new GridLayout(3,1));
        SortPanel.setLayout(new GridLayout(2,2));

        CityPanel.setBorder(new TitledBorder("City Group"));
        SortPanel.setBorder(new TitledBorder("Sort & Find"));
    }
    public void addComponents()
    {
        add(CityPanel);
        add(ApplyPanel);
        add(SortPanel);

        CityPanel.add(JRAllCity);
        CityPanel.add(JRSeleCity);
        CityPanel.add(JCCity);    

        SortPanel.add(new JLabel("Sort Order"));
        SortPanel.add(TSort);
        SortPanel.add(new JLabel("To Find"));
        SortPanel.add(TFind);
        TFind.setEditable(false);

        ApplyPanel.add(BApply);
    }
    public void addListeners()
    {

        JRAllCity.addActionListener(new CityList());
        JRSeleCity.addActionListener(new CityList());

    }
    public class CityList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==JRAllCity)
            {
                JRAllCity.setSelected(true);
                JRSeleCity.setSelected(false);
                JCCity.setEnabled(false);
            }
            if(ae.getSource()==JRSeleCity)
            {
                JRAllCity.setSelected(false);
                JRSeleCity.setSelected(true);
                JCCity.setEnabled(true);
            }
        }
    }

    public void getGroupFinCity()
    {
        VCity  = new Vector();
        VCityCode  = new Vector();

          String QS   = "Select PlaceName,PlaceCode from Place order by 1 ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VCity.addElement(result.getString(1));
                    VCityCode.addElement(result.getString(2));

               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }

    }
}
