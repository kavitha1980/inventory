package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class SupplierEntryFrame extends JInternalFrame
{
     protected           JLayeredPane Layer;
     JPanel              TopPanel  ,MiddlePanel   ,BottomPanel;
     JPanel              MiddleLeft,MiddleRight;
     AddressField        TPartyName,TAddress1     ,TAddress2     ,TAddress3;
     AddressField        TEmail    ,TWeb;
     AddressField        WContact  ,WPhone        ,WFax;
     AddressField        TTinNo    ,TCstNo        ,TPinCode;        
     JTabbedPane         thePane;
     TabReport           theReport;
     MyButton            BOkay,BExit,BExist;
     Vector              VExistParty;
     MyComboBox          JCState,JCCountry;

     Common              common = new Common();
     Connection          theConnection = null;
     int iUserCode       =    0;
     int iMillCode       =    0;
     boolean             bCheck;

     Vector              VStateName,VStateCode;
     Vector              VCountry,VCountryCode;

     public SupplierEntryFrame(JLayeredPane Layer,int iUserCode,int iMillCode)
     {
          this.Layer     = Layer;
          this.iUserCode = iUserCode;
          this.iMillCode = iMillCode;

          setMasterData();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          thePane        = new JTabbedPane();
          MiddlePanel    = new JPanel();
          BottomPanel    = new JPanel();
          MiddleLeft     = new JPanel();
          MiddleRight    = new JPanel();
          TPartyName     = new AddressField(30);
          TAddress1      = new AddressField(30);
          TAddress2      = new AddressField(30);
          TAddress3      = new AddressField(30);
          TEmail         = new AddressField(30);
          TWeb           = new AddressField(30);
          TTinNo         = new AddressField(30);
          TCstNo         = new AddressField(30);
          TPinCode       = new AddressField(30);

          WContact       = new AddressField(30);
          WPhone         = new AddressField(30);
          WFax           = new AddressField(30);

          JCState        = new MyComboBox(VStateName);
          JCCountry      = new MyComboBox(VCountry);
          JCState        . addItem("");
          JCCountry      . addItem("");

          JCState        . setSelectedItem("");
          JCCountry      . setSelectedItem("");

          BOkay          = new MyButton("Save");
          BExit          = new MyButton("Exit");
          BExist         = new MyButton("ExistParty");
     }

     private void setLayouts()
     {
          setTitle(" Supplier Master Frame ");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,650,450);

          MiddlePanel    .    setLayout(new GridLayout(1,2));
          MiddleLeft     .    setLayout(new GridLayout(15,2));
          MiddleRight    .    setLayout(new BorderLayout());
          BottomPanel    .    setLayout(new FlowLayout());

          MiddlePanel    .    setBorder(new TitledBorder("Party Details"));
          MiddleLeft     .    setBorder(new TitledBorder("Party Input"));
          MiddleRight    .    setBorder(new TitledBorder("Exist Details"));


     }
     private void addComponents()
     {

          MiddleLeft     .    add(new JLabel("Party"));
          MiddleLeft     .    add(TPartyName);
          MiddleLeft     .    add(new JLabel("Address1"));
          MiddleLeft     .    add(TAddress1);
          MiddleLeft     .    add(new JLabel("Address2"));
          MiddleLeft     .    add(TAddress2);
          MiddleLeft     .    add(new JLabel("Address3"));
          MiddleLeft     .    add(TAddress3);
          MiddleLeft     .    add(new JLabel("Tin No"));
          MiddleLeft     .    add(TTinNo);
          MiddleLeft     .    add(new JLabel("Cst No"));
          MiddleLeft     .    add(TCstNo);
          MiddleLeft     .    add(new JLabel("EMail"));
          MiddleLeft     .    add(TEmail);
          MiddleLeft     .    add(new JLabel("Contact"));
          MiddleLeft     .    add(WContact);
          MiddleLeft     .    add(new JLabel("Phone"));
          MiddleLeft     .    add(WPhone);
          MiddleLeft     .    add(new JLabel("Fax"));
          MiddleLeft     .    add(WFax);
          MiddleLeft     .    add(new JLabel("WebSite"));
          MiddleLeft     .    add(TWeb);
          MiddleLeft     .    add(new JLabel("State"));
          MiddleLeft     .    add(JCState);
          MiddleLeft     .    add(new JLabel("Pin code"));
          MiddleLeft     .    add(TPinCode);
          MiddleLeft     .    add(new JLabel("Country"));
          MiddleLeft     .    add(JCCountry);
          MiddleLeft     .    add(new JLabel(""));
          MiddleLeft     .    add(BExist);

          MiddleRight    .    add("Center",thePane);
          thePane        .    addTab("Party List",theReport);
          MiddlePanel    .    add(MiddleLeft);
          MiddlePanel    .    add(MiddleRight);

          // BottomPanel Controls
          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');
     }
     private void addListeners()
     {
          BOkay     .    addActionListener(new ActList());
          BExit     .    addActionListener(new ActList());
          BExist    .    addActionListener(new ActList()); 
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BExist)
               {
                    String SParty  =    common.parseNull(TPartyName.getText());

                    if(SParty.length()>0)    {
                         setExistVector(SParty);
                         setTabReport();
                         getContentPane().remove(MiddleRight);
                         MiddleRight.remove(thePane);
                         thePane = new JTabbedPane();
     
                         if(theReport != null)
                              thePane.addTab("Party List ",theReport);
                         else
                              thePane.addTab("Party List",new MyLabel("Not Available")); 
     
                         MiddleRight.add("Center",thePane); 
                         updateUI();
                         bCheck=true;
                    }
               }
               if(ae.getSource()==BOkay)
               {
                    saveData();
                    bCheck=false;
                    TPartyName.setText("");
                    TAddress1.setText("");
                    TAddress2.setText("");
                    TAddress3.setText("");
                    TTinNo.setText("");
                    TCstNo.setText("");
                    TEmail.setText("");
                    WContact.setText("");
                    WPhone.setText("");
                    WFax.setText("");
                    TWeb.setText("");
                    JCState        . setSelectedItem("");
                    JCCountry      . setSelectedItem("");
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }
     private void saveData()
     {
          if(!isValidData())
               return;


          if(bCheck==true)
          {
               persist();
          }
          else
          {
               JOptionPane.showMessageDialog(null, "Check Exists Party", "Error",JOptionPane.ERROR_MESSAGE);
               return;
          }
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TPartyName.getText());
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "Party Name already Exists", "Error",JOptionPane.ERROR_MESSAGE);
                    TPartyName.requestFocus();
                    return false;
               }
               if(common.parseNull(TAddress1.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"Address1 Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TAddress1.requestFocus();
                    return false;
               }
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
          return true;
     }

     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select Name from Supplier Where Name='"+SName+"' ";
          return QS;
     }


     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"PARTY NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {250};

               Object RowData[][] = new Object[VExistParty.size()][ColumnName.length];
               for(int i=0;i<VExistParty.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VExistParty.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }

     }

     private void setExistVector(String SParty)
     {
          VExistParty         = new Vector();

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(getExistQS(SParty));
               while(result.next())
               {
                    VExistParty         .  addElement(result.getString(1));
               }

               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private String getExistQS(String SPartyName)
     {
          StringBuffer SB          = new StringBuffer();

          String SFirstCheck       = "", SSecondCheck  = "";

          try
          {
               StringTokenizer ST  = new StringTokenizer(SPartyName," ");

               SFirstCheck         = ST.nextToken().substring(0,3);
               SSecondCheck        = ST.nextToken().substring(0,3);
          }
          catch(Exception ex)
          {
               SSecondCheck        = "";
          }

          SB.append(" Select Name from Supplier where Name like '%"+SFirstCheck+"% ");

          if(SSecondCheck.length() > 0)
               SB.append(SSecondCheck+"%");

          SB.append("'");

          System.out.println(SB.toString());

          return SB.toString();
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }

     private void persist()
     {
          try
          {
               String SPartyName        = TPartyName.getText().trim().toUpperCase();
               String SAddress1         = TAddress1.getText().trim().toUpperCase();
               String SAddress2         = TAddress2.getText().trim().toUpperCase();
               String SAddress3         = TAddress3.getText().trim().toUpperCase();
               String SEmail            = TEmail.getText().trim();
               String SContact          = WContact.getText().trim().toUpperCase();
               String SPhone            = WPhone.getText().trim().toUpperCase();
               String SFax              = WFax.getText().trim().toUpperCase();
               String SWeb              = TWeb.getText().trim();

               String STin              = TTinNo.getText().trim().toUpperCase();
               String SCst              = TCstNo.getText().trim().toUpperCase();
               String SPin              = TPinCode.getText().trim().toUpperCase();
               String SStateCode        = getStateCode((String)JCState.getSelectedItem());
               String SCountryCode      = getCountryCode((String)JCCountry.getSelectedItem());

               boolean bFlag            = insertSupplier(SPartyName,SAddress1,SAddress2,SAddress3,SEmail,SContact,SPhone,SFax,SWeb,STin,SCst,SPin,SStateCode,SCountryCode);

               if(bFlag==false)
               {
                    JOptionPane.showMessageDialog(null,"Date Not Saved ","Error",JOptionPane.ERROR_MESSAGE);
               }
               if(bFlag==true)
               {
                    showMessage();
               }
                    
          }
          catch(Exception ex){}
     }

     private void showMessage()
     {
          JOptionPane.showMessageDialog(null,getMessage(),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }

     private String getMessage()
     {
          String str = "<html><body>";
          str = str + "A New Supplier Name was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
     public boolean insertSupplier(String SPartyName,String SAddress1,String SAddress2,String SAddress3,String SEmail,String SContact,String SPhone,String SFax,String SWeb,String STin,String SCst,String SPin,String SStateCode,String SCountryCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc  = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertSupplierQS(SPartyName,SAddress1,SAddress2,SAddress3,SEmail,SContact,SPhone,SFax,SWeb,STin,SCst,SPin,SStateCode,SCountryCode));
               stat.close();
               return true;
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return false;
          }
    }
     private String getInsertSupplierQS(String SPartyName,String SAddress1,String SAddress2,String SAddress3,String SEmail,String SContact,String SPhone,String SFax,String SWeb,String STin,String SCst,String SPin,String SStateCode,String SCountryCode)
     {
               String QS = "";
          try
          {
               QS = "Insert Into Supplier(Ac_code,Name,Addr1,Addr2,addr3,email,contact,phone,fax,website,tinno,cstno,statecode,countrycode,pincode) Values (";
               QS = QS+"SupCode_seq.nextval ,";
               QS = QS+" '"+SPartyName+"' ,";
               QS = QS+" '"+SAddress1+"' ,";
               QS = QS+" '"+SAddress2+"' ,";
               QS = QS+" '"+SAddress3+"' ,";
               QS = QS+" '"+SEmail+"' ,";
               QS = QS+" '"+SContact+"' ,";
               QS = QS+" '"+SPhone+"' ,";
               QS = QS+" '"+SFax+"' ,";
               QS = QS+" '"+SWeb+"' ,";
               QS = QS+" '"+STin+"' ,";
               QS = QS+" '"+SCst+"' ,";
               QS = QS+" '"+SStateCode+"' ,";
               QS = QS+" '"+SCountryCode+"', ";
               QS = QS+" '"+SPin+"' )";
               
          }
          catch(Exception e)
          {
               System.out.println("Insert Hod "+e);
          }
          return QS;
     }
     private void setMasterData()
     {
          VStateName     =    new Vector();
          VStateCode     =    new Vector();
          VCountry       =    new Vector();
          VCountryCode   =    new Vector();

          try
          {
               String QS =" Select StateCode,StateName from State Order by 2 ";
               String QS1=" Select CountryCode,CountryName from Country Order by 2 ";

               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VStateName         .  addElement(result.getString(2));
                    VStateCode         .  addElement(result.getString(1));
               }
               result.close();

               result    =    theStatement.executeQuery(QS1);
               while(result.next())
               {
                    VCountryCode         .  addElement(result.getString(1));
                    VCountry             .  addElement(result.getString(2));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getStateCode(String SState)
     {
          int iIndex     =    VStateName.indexOf(SState);

          if(iIndex!=-1)
               return (String)VStateCode.elementAt(iIndex);
          return "";
     }

     private String getCountryCode(String SCountry)
     {
          int iIndex     =    VCountry.indexOf(SCountry);

          if(iIndex!=-1)
               return (String)VCountryCode.elementAt(iIndex);
          return "";
     }
}


