package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class SupplierFrame extends JInternalFrame
{
      JLayeredPane     Layer;
      int iAuthCode;
      
      Object RowData[][];
      String ColumnData[] = {"Code","Name","Address1","Address2","Address3","Pincode","Phone","Fax","e-Mail","Website","Contact Person","Click for Modify"};
      String ColumnType[] = {"S","S","E","E","E","E","E","E","E","E","E","B"};
      Vector VSCode,VSName,VAddr1,VAddr2,VAddr3,VPin,VMail,VPhone,VContact,VFax,VWeb;
      JTabbedPane JTP;
      JPanel ListPanel;
      JButton BOk;
      Common common = new Common();
      SupPanel suppanel;
      TabReport tabreport;
      String SFind="";

      Connection theConnection = null;

      public SupplierFrame(JLayeredPane Layer,int iAuthCode)
      {
            this.Layer         = Layer;
            this.iAuthCode     = iAuthCode;

            suppanel           = new SupPanel();
            JTP                = new JTabbedPane();
            ListPanel          = new JPanel(true);
            BOk                = new JButton("Update");
            JTP.addTab("List of Suppliers",ListPanel);
            setLayouts();
      }
         
      public void setLayouts()
      {
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);
            setTitle("Supplier View");

            ListPanel.setLayout(new BorderLayout());
            ListPanel.add("North",suppanel);
            if(iAuthCode>1)
            {
               ListPanel.add("South",BOk);
            }
            suppanel.BApply.addActionListener(new ActList());
            BOk.addActionListener(new UpdtList());

            getContentPane().add("Center",JTP);
      }
      public class ActList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  suppanel.TFind.setEditable(true);
                  setVector();
                  setRowData();
                  try
                  {
                       ListPanel.remove(tabreport); 
                  }
                  catch(Exception ex)
                  {
                  }
                  try
                  {
                     tabreport = new TabReport(RowData,ColumnData,ColumnType);
                     ListPanel.add(tabreport,BorderLayout.CENTER);
                     tabreport.ReportTable.setShowHorizontalLines(true);
                     tabreport.ReportTable.setShowVerticalLines(true);
                     setSelected(true);
                     Layer.repaint();
                     Layer.updateUI();
                     suppanel.TFind.addKeyListener(new KeyList());
                  }
                  catch(Exception ex)
                  {
                     System.out.println(ex);
                  }
            }
      }
      public class UpdtList implements ActionListener
      {
         public void actionPerformed(ActionEvent ae)
         {
             BOk.setEnabled(false);
           //  updateSuppliers();
         }
      }
      public class KeyList extends KeyAdapter
      {
                public void keyReleased(KeyEvent ke)
                {
                        String SFind = suppanel.TFind.getText();
                        int i=indexOf(SFind);
                        if (i>-1)
                        {
                                tabreport.ReportTable.setRowSelectionInterval(i,i);
                                Rectangle cellRect = tabreport.ReportTable.getCellRect(i,0,true);
                                tabreport.ReportTable.scrollRectToVisible(cellRect);
                        }
                }
      }
      public int indexOf(String SFind)
      {
                SFind = SFind.toUpperCase();
                for(int i=0;i<VSName.size();i++)
                {
                        String str = (String)VSName.elementAt(i);
                        str=str.toUpperCase();
                        if(str.startsWith(SFind))
                                return i;
                }
                return -1;
      }
      public void setVector()
      {
            VSCode      = new Vector();
            VSName      = new Vector();
            VAddr1      = new Vector();
            VAddr2      = new Vector();
            VAddr3      = new Vector();
            VPin        = new Vector();
            VMail       = new Vector();
            VPhone      = new Vector();
            VContact    = new Vector();
            VFax        = new Vector();
            VWeb        = new Vector();

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
                  Statement stat = theConnection.createStatement();
                  ResultSet res  = stat.executeQuery(getQString());
                  while(res.next())
                  {
                        VSCode      .addElement(res.getString(1));
                        VSName      .addElement(res.getString(2));
                        VAddr1      .addElement(res.getString(3));
                        VAddr2      .addElement(res.getString(4));
                        VAddr3      .addElement(res.getString(5));
                        VPin        .addElement(res.getString(6));
                        VPhone      .addElement(res.getString(7));
                        VFax        .addElement(res.getString(8));
                        VMail       .addElement(res.getString(9));
                        VWeb        .addElement(res.getString(10));
                        VContact    .addElement(res.getString(11));
                  }
                  stat.close();
                  res.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
      public void setRowData()
      {
            RowData = new Object[VSCode.size()][ColumnData.length];
            for(int i=0;i<VSCode.size();i++)
            {
                        RowData[i][0]  = common.parseNull((String)VSCode.elementAt(i));      
                        RowData[i][1]  = common.parseNull((String)VSName.elementAt(i));      
                        RowData[i][2]  = common.parseNull((String)VAddr1.elementAt(i));      
                        RowData[i][3]  = common.parseNull((String)VAddr2.elementAt(i));      
                        RowData[i][4]  = common.parseNull((String)VAddr3.elementAt(i));      
                        RowData[i][5]  = common.parseNull((String)VPin.elementAt(i));
                        RowData[i][6]  = common.parseNull((String)VPhone.elementAt(i));
                        RowData[i][7] = common.parseNull((String)VFax.elementAt(i));
                        RowData[i][8] = common.parseNull((String)VMail.elementAt(i));
                        RowData[i][9] = common.parseNull((String)VWeb.elementAt(i));
                        RowData[i][10] = common.parseNull((String)VContact.elementAt(i));
                        RowData[i][11] = new Boolean(false);       
            }
      }
      /*public void updateSuppliers()
      {
           try
           {
               if(theConnection == null)
               {
                    ORAConnection2 jdbc   = ORAConnection2.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
                Statement stat   = theConnection.createStatement();
 
                for(int i=0;i<VSCode.size();i++)
                {
                     Boolean Bselected = (Boolean)RowData[i][11];
                     if(Bselected.booleanValue())
                     {

                         String QString = "Update PartyMaster Set ";
                         QString = QString+" PartyName = '"+((String)RowData[i][1]).toUpperCase()+"',";
                         QString = QString+" Address1 = '"+((String)RowData[i][2]).toUpperCase()+"',";
                         QString = QString+" Address2 = '"+((String)RowData[i][3]).toUpperCase()+"',";
                         QString = QString+" Address3 = '"+((String)RowData[i][4]).toUpperCase()+"',";
                         QString = QString+" OffPhone1 = '"+(String)RowData[i][6]+"',";
                         QString = QString+" Fax = '"+(String)RowData[i][7]+"',";
                         QString = QString+" email1 = '"+(String)RowData[i][8]+"',";
                         QString = QString+" Website = '"+(String)RowData[i][9]+"',";
                         QString = QString+" ContPerson1 = '"+((String)RowData[i][10]).toUpperCase()+"'";
                         QString = QString+" Where PartyCode = '"+(String)RowData[i][0]+"'";
                         stat.execute(QString);
                    }
                }
                theConnection.close();
           }
           catch(Exception ex)
           {
                 System.out.println(ex);
           }
      }*/
      public String getQString()
      {
            String QString = "Select Supplier.Ac_Code,Supplier.Name,Supplier.Addr1,Supplier.Addr2,Supplier.Addr3,'' as Pincode,Supplier.Phone,Supplier.Fax,Supplier.email,Supplier.Website,Supplier.Contact "+
                             "From Supplier";

            if(((suppanel.TSort.getText()).trim()).length()==0)
                    QString = QString+" Order By 2";
            else
                    QString = QString+" Order By "+suppanel.TSort.getText();

            return QString;
      }
}
