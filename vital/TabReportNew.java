package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class TabReportNew extends JPanel
{
               JTable         ReportTable;
     private   Object         RowData[][];
     private   String         ColumnData[],ColumnType[];
     private   boolean        Flag;
               JPanel         thePanel;

     TabReportNew(Object RowData[][],String ColumnData[],String ColumnType[])
     {
          this.RowData     = RowData;
          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          thePanel         = new JPanel();
          verifyRowData();
          setReportTable();
     }

     public void setReportTable()
     {
          AbstractTableModel dataModel = new AbstractTableModel()
          {
               public int getColumnCount(){return ColumnData.length;}
               public int getRowCount(){return RowData.length;}
               public Object getValueAt(int row,int col){return RowData[row][col];}
               public String getColumnName(int col){ return ColumnData[col];}
               public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }

               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
               public void setValueAt(Object element,int row,int col)
               {
                    RowData[row][col]=element;
               }
          };
          ReportTable  = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
          }
           //ReportTable.setShowGrid(false);
          setLayout(new BorderLayout());
          thePanel.setLayout(new BorderLayout());
          thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
          add("Center",thePanel);
     }

     public void verifyRowData()
     {
          String str=""; 
          for(int i=0;i<RowData.length;i++)
          {
               for(int j=0;j<ColumnData.length;j++)
               {
                    try
                    {
                         Object obj = RowData[i][j];     
                         if(obj instanceof Boolean)
                              continue;
                         
                         str = (((String)RowData[i][j]).trim()).toUpperCase();
                         if(str.startsWith("NULL"))
                              RowData[i][j]="";
                    }
                    catch(Exception ex)
                    {
                         RowData[i][j]="";
                    }
               }
          }
     }

     public void setPrefferedColumnWidth(int iColWidth[])
     {
          if(iColWidth.length != ColumnData.length)
               return;
          for(int i=0;i<ColumnData.length;i++)
          {
               if(iColWidth[i]>0)
                    (ReportTable.getColumn(ColumnData[i])).setPreferredWidth(iColWidth[i]);
          }
          ReportTable.removeColumn((TableColumn)ReportTable.getColumn("Last Ordered"));
     }

     public void setPrefferedColumnWidth1(int iColWidth[])
     {
          if(iColWidth.length != ColumnData.length)
               return;
          for(int i=0;i<ColumnData.length;i++)
          {
               if(iColWidth[i]>0)
                    (ReportTable.getColumn(ColumnData[i])).setPreferredWidth(iColWidth[i]);
          }
     }

     protected void finalize() throws Throwable
     {
          ReportTable    = null;
          RowData        = null;
          ColumnData     = null;
          ColumnType     = null;
          thePanel       = null;
          super          . finalize();
         //System.out.println("finalizeinTabreport");
     }
}

