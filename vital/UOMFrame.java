package vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import java.sql.*;

import guiutil.*;
import util.*;
import jdbc.*;

public class UOMFrame extends JInternalFrame
{
     protected      JLayeredPane Layer;

     JPanel         TopPanel,MiddlePanel,BottomPanel;
     AddressField   TMeasureType;
     JTabbedPane    thePane;
     TabReport      theReport;
     JButton        BOkay,BExit;
     Vector         VUnOfMat;
     Common         common = new Common();

     Connection theConnection = null;

     int iUserCode=0;
     int iAuthCode=0;

     public UOMFrame(JLayeredPane Layer,int iUserCode,int iAuthCode)
     {
          this.Layer = Layer;
          this.iUserCode = iUserCode;
          this.iAuthCode = iAuthCode;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          thePane             = new JTabbedPane();
          TopPanel            = new JPanel();
          MiddlePanel         = new JPanel();
          BottomPanel         = new JPanel();
          TMeasureType           = new AddressField(30);
          VUnOfMat            = new Vector();
          BOkay               = new MyButton("Save");
          BExit               = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("Unit Of Measurements for Materials");
          setClosable(true);
          setMaximizable(false);
          setIconifiable(true);
          setResizable(false);
          setBounds(0,0,400,500);

          TopPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new BorderLayout());
          BottomPanel.setLayout(new FlowLayout());

          TopPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {
          setVector();
          setTabReport();

          /* TopPanel Controls     */

          TopPanel.add(new JLabel("Measurement Type"));
          TopPanel.add(TMeasureType);

          MiddlePanel.add("Center",thePane);
          thePane.addTab("Unit Of Measurement List",theReport);

          /* BottomPanel Controls*/

          BottomPanel.add(BOkay);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
          BOkay.setMnemonic('S');
          BExit.setMnemonic('E');
     }

     public void setVector()
     {
          VUnOfMat    = new Vector();

          String QS   = "Select UomName from UoM order by 1 ";

          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet result          = theStatement.executeQuery(QS);
               while(result.next())
               {
                    VUnOfMat.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }

     private void setTabReport()
     {
          try
          {
               String ColumnName[] = {"Unit of Measurement NAME"};
               String ColumnType[] = {"S"};
               int    ColumnWidth[]  = {350};

               Object RowData[][] = new Object[VUnOfMat.size()][ColumnName.length];
               for(int i=0;i<VUnOfMat.size();i++)
               {
                    RowData[i][0] = common.parseNull((String)VUnOfMat.elementAt(i));
               }
               theReport      = new TabReport(RowData,ColumnName,ColumnType);
               theReport.setPrefferedColumnWidth1(ColumnWidth);
          }
          catch(Exception ex)
          {
               System.out.println("setTabReport : "+ex);
          }
     }

          /*   Insert Data Query String Methods   */

     private String getInsertUOMQS(String SUOmCode,String SUOmName,String SUserCode,String SDateTime,int id)
     {
          String QS = "";

          QS = "Insert Into UOM(UOMCODE,UOMNAME,USERCODE,CREATIONDATE,ID) Values (";
          QS = QS+"'"+SUOmCode+"',";
          QS = QS+"'"+SUOmName+"',";
          QS = QS+"'"+SUserCode+"',";
          QS = QS+"'"+SDateTime+"',";
          QS = QS+id+")";
       
          return QS;
     }
          /*          Insert Data Methods         */

     public void insertUOM(String SUOmName,String SUserCode)
     {
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               stat.execute(getInsertUOMQS(String.valueOf(maxValueOfTable()+1),SUOmName,SUserCode,common.getServerDate(),getNextVal("Uom_Seq")));

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public int maxValueOfTable()
     {
          int iMax = 0;
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               ResultSet result = stat.executeQuery("Select max(UOMCODE) from uom");

               result.next();

               iMax = common.toInt(result.getString(1));
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return iMax;
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    BOkay.setEnabled(false);
                    saveData();
                    setVector();
                    setTabReport();
                    getContentPane().remove(MiddlePanel);
                    MiddlePanel.remove(thePane);
                    thePane = new JTabbedPane();

                    if(theReport != null)
                         thePane.addTab("BookThro List ",theReport);
                    else
                         thePane.addTab("BookThro List",new MyLabel("Not Available"));

                    MiddlePanel.add("Center",thePane);
                    getContentPane().add("Center",MiddlePanel);
                    updateUI();
                    TMeasureType.setText("");
                    TMeasureType.requestFocus();
                    BOkay.setEnabled(true);
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          persist();
     }

     private boolean isValidData()
     {
          try
          {
               String SCount = getExistName(TMeasureType.getText());

               if(common.parseNull(TMeasureType.getText()).equals(""))
               {
                    JOptionPane.showMessageDialog(null,"MeasureMent Type Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TMeasureType.requestFocus();
                    return false;
               }
               if(!SCount.equals(""))
               {
                    JOptionPane.showMessageDialog(null, "MeasureMent Type already Exists", "Error", 
                    JOptionPane.ERROR_MESSAGE);
                    TMeasureType.requestFocus();
                    return false;
               }

               if(TMeasureType.getText().trim().equals("0"))
               {
                    JOptionPane.showMessageDialog(null,"Invalid MeasureMent Type! ","Error",JOptionPane.ERROR_MESSAGE);
                    TMeasureType.requestFocus();
                    TMeasureType.setText("");
                    return false;
               }
               if(TMeasureType.getText().trim().length()==0)
               {
                    JOptionPane.showMessageDialog(null,"MeasureMent Type must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TMeasureType.requestFocus();
                    TMeasureType.setText("");
                    return false;
               } 
               return true;
          }

          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return true;
     }

     private void persist()
     {
          String SUserCode = String.valueOf(iUserCode);
          String SUOmName  = TMeasureType.getText().trim().toUpperCase();

          try
          {
               insertUOM(SUOmName,SUserCode);
          }catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public  String getExistName(String SName)
     {
          String  Name="";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(getNameQS(SName));
               while(theResult.next())
               {
                    Name =    theResult.getString(1);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return Name;
     }
     private String getNameQS(String SName)
     {
          String QS = " Select UomName from UoM Where UomName='"+SName+"' ";
          return QS;
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }

     private int getNextVal(String SSequence) throws Exception
     {
          int iValue = -1;
          String QS = "Select "+SSequence+".NextVal from dual";
          try
          {
               if(theConnection == null)
               {
                    ORAConnection jdbc   = ORAConnection.getORAConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               ResultSet result = stat.executeQuery(QS);
               if(result.next())
               {
                    iValue = result.getInt(1);
               }
               result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("getNextVal :"+SSequence+ex);
          }
          return iValue;
     }

}

      
